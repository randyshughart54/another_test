#! /usr/bin/python

"""This is a gitlab CI/CD redmine task checking script"""

import re
import sys

try:
    from redminelib import Redmine
except ImportError:
    print'One of the requiered package not found:'
    print ' * Redmine (try to: pip install python-redmine)'
    sys.exit(1)

COLOR_RED = "\033[31m"
COLOR_RST = "\033[0m"

class Commit(object):
    """Commit cheker class"""
    def __init__(self, commit_hash, commit_body, server, user, password):
        """Default constructor"""
        self.__hash = commit_hash
        self.__text = commit_body
        self.__task = ""
        self.__redmine = ""
        self.__is_valid = False

        self.__server = server
        self.__username = user
        self.__password = password

    # Public methods
    def debug(self):
        """Debug method"""
        print "\n**************************"
        print "Hash    : " + self.__hash
        print "Text    : " + self.__text
        print "Task    : " + self.__task
        print "Redmine : " + self.__redmine
        print "**************************\n"


    def is_valid(self):
        """Returns valid condition"""
        return self.__is_valid


    def check(self):
        """Checks commit message"""
        task_list = re.findall("#\d+", self.__text)
        if len(task_list) == 0:
            self.__task = "N/A"
            self.__redmine = "N/A"
            self.__is_valid = True
            return

        for entry in task_list:
            task = entry[1:]
            self.__task += str(task) + " "
            self.__check_redmine(str(task))

    # Private methods
    def __check_redmine(self, task):
        """Checks redmine project task"""
        try:
            redmine = Redmine("http://" + self.__server,
                              username=self.__username, password=self.__password)
            issue = redmine.issue.get(task)
            # Have to belong to our projects
            if str(issue.project) == "ME5000 Routers":
                self.__redmine = str(issue.project)
                self.__is_valid = True
                return
            elif str(issue.project).find("ME5K") != -1:
                self.__redmine = str(issue.project)
                self.__is_valid = True
                return
            else:
                self.__redmine = str(issue.project)
                self.__is_valid = False
                self.debug()
                print COLOR_RED, "Invalid task number!", task, COLOR_RST
                return
        except:
            print COLOR_RED, "Can't get data from Redmine!", COLOR_RST
            self.__is_valid = False
            return


def usage():
    """Usage function"""
    print "Usage: ./.gitlab-ci-commit.py <commits> <redmine-server> <username> <password>"


def main(commit_list, redmine_server, user, password):
    """Main function"""
    for entry in commit_list:
        idx = entry.find(" ")
        if idx is not -1:
            commit = Commit(entry[:idx], entry[idx + 1:], redmine_server, user, password)
            commit.check()
            if commit.is_valid() is not True:
                sys.exit(-1)
        else:
            print COLOR_RED, "Can't get commit body!", COLOR_RST


if __name__ == '__main__':
    if len(sys.argv) != 5:
        usage()
        sys.exit(2)

    main(sys.argv[1].split("\n"), str(sys.argv[2]), str(sys.argv[3]), str(sys.argv[4]))
