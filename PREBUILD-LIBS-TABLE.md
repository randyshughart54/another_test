## Prebuild libraries table

|    Name         | Predependence | Postdependence |
|-----------------|---------------|----------------|
| 1. containers   |      -        |       -        |
| 2. eventlog     |      -        |       -        | 
| 3. json         |      -        |       -        |
| 4. libmarvell   |      -        |       -        |
| 5. libpcap      |      -        |       -        |
| 6. librs422     |      -        |       -        |
| 7. libssh2      |      -        |       -        |
| 8. libxml2      |      -        |       -        |
| 9. ncurses      |      -        |       -        |
| 10.oniguruma    |      -        |       -        |
| 11.openldap     |      -        |       -        |
| 12.openssl      |      -        |       -        |
| 13.pam_ldap     |      -        |       -        |
| 14.pam_tacplus  |      -        |       -        |
| 15.pcre         |      -        |       -        |
| 16.sshpass      |      -        |       -        |
| 17.ps-info      |      -        |       -        |
| 18.boost        |      -        | yami4          |
| 19.hw-avail     |      -        | yami4          |
| 20.ivykis       |      -        | mem-monitor shlive |
| 21.ivykis-tools |      -        | mem-monitor shlive |
| 22.libffi       |      -        | glib           |
| 23.nl           |      -        | pam_radius     |
| 24.pam          |      -        | pam_radius     |
| 25.zlib         |      -        | glib           |
| 26.glib         | libffi zlib   |        -       |
| 27.libmtd       | log           |        -       |
| 28.mem-monitor  | ivykis ivykis-tools | -        |
| 29.shlive       | ivykis ivykis-tools | -        |
| 30.yami4        | boost hw-avail backtrace clockgettime alive_sig| - |
| 31.pam_radius   | nl pam log forking libsyslog ns nlapi | - |

