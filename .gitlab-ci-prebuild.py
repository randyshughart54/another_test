#! /usr/bin/python

"""This is a gitlab CI/CD prebuild lib checking script"""

import os
import sys

COLOR_RED = "\033[31m"
COLOR_RST = "\033[0m"

class PreBuildLib(object):
    """Prebuild lib class"""
    def __init__(self, lib_name, board_list, predep_list, postdep_list):
        """Default constructor"""
        self.__name = lib_name
        self.__board_list = board_list
        self.__predep_list = predep_list
        self.__postdep_list = postdep_list


    def debug(self):
        """Debug method"""
        print "\n****************************************************"
        print "Name    : " + self.__name
        print "Boards  : " + " ".join(self.__board_list)
        print "Predep  : " + " ".join(self.__predep_list)
        print "Postdep : " + " ".join(self.__postdep_list)
        print "****************************************************\n"


    def get_name_str(self):
        """Gets prebuild lib name"""
        return self.__name


    def get_predep_str(self):
        """Gets predependence string"""
        return " ".join(self.__predep_list)


    def get_postdep_str(self, prebuild_libs):
        """Gets postdependence string"""
        string = ""
        for i in self.__postdep_list:
            for lib in prebuild_libs:
                if lib.get_name_str() == i:
                    string += " ".join(lib.get_predep_list())
                    string += " "
            string += i
        return string


    def get_board_list(self):
        """Gets board to build list"""
        return self.__board_list


    def get_predep_list(self):
        """Gets predependence list"""
        return self.__predep_list


    def get_postdep_list(self):
        """Gets postdependence list"""
        return self.__postdep_list


    def generate_make_cmd_(self, device, board, prebuild_libs):
        """Generates make command"""
        predep = self.get_predep_str()
        postdep = self.get_postdep_str(prebuild_libs)
        lib = self.get_name_str()

        cmd = "make " + device + "-config-rebuild-lib && "
        cmd += "cd " + device + "/" + board + " && "
        cmd += "make " + predep + " " + lib + " " + postdep + " "
        cmd += "MAKE_ACTION=\"clean all\""
        cmd += " && cd .."

        # Possible make command for ME5100:
        # make me5100-config-rebuild-lib && \
        # cd me5100/ && make mem-monitor MAKE_ACTION='clean all && cd ..'
        return cmd


    def generate_make_cmd(self, prebuild_libs):
        """Generates shell command - make"""
        make_list = []

        for board in self.get_board_list():
            # TODO: fcm16 lc8-20 etc renaming!
            if board == "fmc" or board == "fmc16" or board == "fmc32" or \
               board == "lc" or board == "lc18" or board == "lc8-20":
                if board == "lc18":
                    board = "lc"
                make_list.append(self.generate_make_cmd_("me5000", board + "/", prebuild_libs))
            else:
                make_list.append(self.generate_make_cmd_(board, "", prebuild_libs))
        return make_list


    def generate_git_add_cmd_list(self, path):
        """Generates shell command - git add"""
        build_dir = "/build/"
        all_files = "*"
        cmd = "git add "
        cmd_list = []

        cmd_list.append(cmd + path + self.get_name_str() + build_dir + all_files)
        for post_dep_lib in self.get_postdep_list():
            cmd_list.append(cmd + path + post_dep_lib + build_dir + all_files)
        return cmd_list


    def generate_git_commit_cmd(self):
        """Generates shell command - git commit message"""
        cmd = "git commit -m \"[REBUILD-PREBUILD]"
        cmd += "[" + self.get_name_str() + "]"
        for post_lib in self.get_postdep_list():
            cmd += "[" + post_lib + "]"
        cmd += "\""
        return cmd


def init_prebuild_libs():
    """Initilizes prebuild libs properties"""
    precompiled_list = []

    # Completely independend (keep it in alphabetic order!
    precompiled_list.append(PreBuildLib(
        "containers",                        # Lib name
        ["fmc", "me5100", "me5200", "lc18"], # Boards to build
        [],                                  # Prebuild dependence
        []))                                 # Postbuild dependence

    precompiled_list.append(PreBuildLib(
        "eventlog",
        ["fmc", "me5100", "me5200", "lc18"],
        [],
        []))

    precompiled_list.append(PreBuildLib(
        "glib",
        ["fmc", "me5100", "me5200", "lc18"],
        [],
        []))

    precompiled_list.append(PreBuildLib(
        "json",
        ["fmc", "me5100", "me5200", "lc18"],
        [],
        []))

    precompiled_list.append(PreBuildLib(
        "libmarvell",
        ["fmc", "me5100", "me5200"],
        [],
        []))

    precompiled_list.append(PreBuildLib(
        "libpcap",
        ["fmc", "me5100", "me5200"],
        [],
        []))

    precompiled_list.append(PreBuildLib(
        "librs422",
        ["fmc"],
        [],
        []))

    precompiled_list.append(PreBuildLib(
        "libssh2",
        ["fmc", "me5100", "me5200"],
        [],
        []))

    precompiled_list.append(PreBuildLib(
        "libxml2",
        ["fmc", "me5100", "me5200"],
        [],
        []))

    precompiled_list.append(PreBuildLib(
        "mem-monitor",
        ["fmc", "me5100", "me5200", "lc18"],
        [],
        []))

    precompiled_list.append(PreBuildLib(
        "ncurses",
        ["fmc", "me5100", "me5200"],
        [],
        []))

    precompiled_list.append(PreBuildLib(
        "oniguruma",
        ["fmc", "me5100", "me5200"],
        [],
        []))

    precompiled_list.append(PreBuildLib(
        "openldap",
        ["fmc", "me5100", "me5200"],
        [],
        []))

    precompiled_list.append(PreBuildLib(
        "openssl",
        ["fmc", "me5100", "me5200", "lc18"],
        [],
        []))

    precompiled_list.append(PreBuildLib(
        "pam_ldap",
        ["fmc", "me5100", "me5200"],
        [],
        []))

    precompiled_list.append(PreBuildLib(
        "pam_tacplus",
        ["fmc", "me5100", "me5200"],
        [],
        []))

    precompiled_list.append(PreBuildLib(
        "pcre",
        ["fmc", "me5100", "me5200", "lc18"],
        [],
        []))

    precompiled_list.append(PreBuildLib(
        "ps-info",
        ["me5100", "me5200"],
        [],
        []))

    precompiled_list.append(PreBuildLib(
        "shlive",
        ["fmc", "me5100", "me5200"],
        [],
        []))

    precompiled_list.append(PreBuildLib(
        "sshpass",
        ["fmc", "me5100", "me5200"],
        [],
        []))

    # Postbuild dependence (keep it in alphabetic order!)
    precompiled_list.append(PreBuildLib(
        "boost",
        ["fmc", "me5100", "me5200", "lc18"],
        [],
        ["yami4"]))

    precompiled_list.append(PreBuildLib(
        "hw-avail",
        ["fmc", "me5100", "me5200", "lc18"],
        [],
        ["yami4"]))

    precompiled_list.append(PreBuildLib(
        "ivykis",
        ["fmc", "me5100", "me5200", "lc18"],
        [],
        ["mem-monitor", "shlive"]))

    precompiled_list.append(PreBuildLib(
        "ivykis-tools",
        ["fmc", "me5100", "me5200", "lc18"],
        [],
        ["mem-monitor", "shlive"]))

    precompiled_list.append(PreBuildLib(
        "libffi",
        ["fmc", "me5100", "me5200", "lc18"],
        [],
        ["glib"]))

    precompiled_list.append(PreBuildLib(
        "nl",
        ["fmc", "me5100", "me5200"],
        [],
        ["pam_radius"]))

    precompiled_list.append(PreBuildLib(
        "pam",
        ["fmc", "me5100", "me5200", "lc18"],
        [],
        ["pam_radius"]))

    precompiled_list.append(PreBuildLib(
        "zlib",
        ["fmc", "me5100", "me5200", "lc18"],
        [],
        ["glib"]))

    # Prebuild dependence (keep it in alphabetic order!)
    precompiled_list.append(PreBuildLib(
        "yami4",
        ["fmc", "me5100", "me5200", "lc18"],
        ["clockgettime", "backtrace", "alive_sig"],
        []))

    precompiled_list.append(PreBuildLib(
        "pam_radius",
        ["fmc", "me5100", "me5200"],
        ["log", "forking", "libsyslog", "ns", "nlapi"],
        []))

    precompiled_list.append(PreBuildLib(
        "libmtd",
        ["fmc", "me5100", "me5200", "lc18"],
        ["log"],
        []))

    return precompiled_list


def execute_cmd(cmd):
    """Executes command in os"""
    print cmd
    # Uncomment it when it's done!
    #if os.system(cmd) != 0:
    #   print "Can't execute: " + cmd
    #    sys.exit(2)


def build_lib(lib, prebuild_libs):
    """Builds prebuild lib via builder.sh"""
    lib.debug()
    print "Building " + lib.get_name_str() + " (" + " ".join(lib.get_postdep_list()) + ")"
    for make_cmd in lib.generate_make_cmd(prebuild_libs):
        execute_cmd(make_cmd)


def publish_build(lib, branch, username, path):
    """Publishes changes to gitlab"""
    cmd_checkout = "git checkout"
    cmd_add = lib.generate_git_add_cmd_list(path)
    cmd_status = "git status"
    cmd_commit = lib.generate_git_commit_cmd()
    cmd_push = "git push origin " + branch

    print "Pushing commit for " + lib.get_name_str()
    execute_cmd("git config --global user.email \"" + username + "@eltex.loc\"")
    execute_cmd("git config --global user.name \"" + username + "\"")
    execute_cmd(cmd_checkout)
    for cmd in cmd_add:
        execute_cmd(cmd)
    execute_cmd(cmd_status)
    execute_cmd(cmd_commit)
    execute_cmd(cmd_push)


def check_postdep(names, prebuild_libs):
    """Checks amount of postdependence"""
    postdep = 0

    for name in names:
        for lib in prebuild_libs:
            if name == lib.get_name_str():
                if len(lib.get_postdep_list()) > 0:
                    postdep += 1
    if postdep > 1:
        print COLOR_RED
        print "Don't do in one merge request multiple prebuild libs changes (with postdependence)"
        print "Separate changes to different merge request!"
        print COLOR_RST
        sys.exit(2)


def usage():
    """Usage function"""
    print "Usage: ./.gitlab-ci-prebuild.py <files> <git-branch> <username>"


def main(files, branch, username):
    """Main function"""
    prebuild_libs = init_prebuild_libs()
    project_path = "common/libs/"
    names = set()

    for entry in files:
        if entry.find(project_path) != -1:
            tmp = entry[len(project_path):]
            tmp = tmp[:tmp.find("/")]
            names.add(tmp)

    check_postdep(names, prebuild_libs)

    for name in names:
        for lib in prebuild_libs:
            if name == lib.get_name_str():
                build_lib(lib, prebuild_libs)
                publish_build(lib, branch, username, project_path)


if __name__ == '__main__':
    if len(sys.argv) != 4:
        usage()
        sys.exit(2)

    main(sys.argv[1].split("\n"), sys.argv[2], sys.argv[3])
