#ifndef _LIBSYSLOG_IGMP_H_INCLUDED_
#define _LIBSYSLOG_IGMP_H_INCLUDED_

#include "libsyslog.h"

#ifdef DISABLE_SYSLOG_SYSTEM_IGMP
	#define SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...)
#else
	#define SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...) \
		SYSLOG_## LEVEL(__system_type__, __subsystem_type__, __format__, ## __VA_ARGS__)
#endif

#define SYSLOG_IGMP_COMMON(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, IGMP, COMMON, __format__, ## __VA_ARGS__)

#define SYSLOG_IGMP_TGS(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, IGMP, TGS, __format__, ## __VA_ARGS__)

#endif //_LIBSYSLOG_IGMP_H_INCLUDED_