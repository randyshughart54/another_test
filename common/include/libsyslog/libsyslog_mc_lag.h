#ifndef _LIBSYSLOG_MC_LAG_H_INCLUDED_
#define _LIBSYSLOG_MC_LAG_H_INCLUDED_

#include "libsyslog.h"

#ifdef DISABLE_SYSLOG_SYSTEM_MC_LAG
	#define SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...)
#else
	#define SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...) \
		SYSLOG_## LEVEL(__system_type__, __subsystem_type__, __format__, ## __VA_ARGS__)
#endif

#define SYSLOG_MC_LAG_COMMON(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, MC_LAG, COMMON, __format__, ## __VA_ARGS__)

#endif //_LIBSYSLOG_MC_LAG_H_INCLUDED_
