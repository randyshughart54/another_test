#ifndef _LIBSYSLOG_ISIS_H_INCLUDED_
#define _LIBSYSLOG_ISIS_H_INCLUDED_

#include "libsyslog.h"

#ifdef DISABLE_SYSLOG_SYSTEM_ISIS
	#define SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...)
#else
	#define SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...) \
		SYSLOG_## LEVEL(__system_type__, __subsystem_type__, __format__, ## __VA_ARGS__)
#endif

#define SYSLOG_ISIS_PM(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, ISIS, PM, __format__, ## __VA_ARGS__)

#define SYSLOG_ISIS_SDC(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, ISIS, SDC, __format__, ## __VA_ARGS__)

#define SYSLOG_ISIS_ADJCHANGE(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, ISIS, ADJCHANGE, __format__, ## __VA_ARGS__)

#define SYSLOG_ISIS_OVRLDCHANGE(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, ISIS, OVRLDCHANGE, __format__, ## __VA_ARGS__)
#endif //_LIBSYSLOG_ISIS_H_INCLUDED_
