#ifndef _LIBSYSLOG_LABEL_MGR_H_INCLUDED_
#define _LIBSYSLOG_LABEL_MGR_H_INCLUDED_

#include "libsyslog.h"

#ifdef DISABLE_SYSLOG_SYSTEM_LABEL_MGR
	#define SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...)
#else
	#define SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...) \
		SYSLOG_## LEVEL(__system_type__, __subsystem_type__, __format__, ## __VA_ARGS__)
#endif

#define SYSLOG_LABEL_MGR_COMMON(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, LABEL_MGR, COMMON, __format__, ## __VA_ARGS__)

#define SYSLOG_LABEL_MGR_RLLB(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, LABEL_MGR, RLLB, __format__, ## __VA_ARGS__)

#define SYSLOG_LABEL_MGR_RLDF(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, LABEL_MGR, RLDF, __format__, ## __VA_ARGS__)

#define SYSLOG_LABEL_MGR_RSIP(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, LABEL_MGR, RSIP, __format__, ## __VA_ARGS__)

#define SYSLOG_LABEL_MGR_RSTC(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, LABEL_MGR, RSTC, __format__, ## __VA_ARGS__)

#define SYSLOG_LABEL_MGR_RSPX(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, LABEL_MGR, RSTC, __format__, ## __VA_ARGS__)

#define SYSLOG_LABEL_MGR_R6WPL(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, LABEL_MGR, RSTC, __format__, ## __VA_ARGS__)

#endif //_LIBSYSLOG_LABEL_MGR_H_INCLUDED_