#ifndef _LIBSYSLOG_NS_H_INCLUDED_
#define _LIBSYSLOG_NS_H_INCLUDED_

#include "libsyslog.h"

#ifdef DISABLE_SYSLOG_SUBSYSTEM_NS
	#define SYSLOG_NS_DBG(__format__, ...)
	#define SYSLOG_NS_ERR(__format__, ...)
#else
	#define SYSLOG_NS_DBG(__format__, ...) SYSLOG_DEBUG(NS, COMMON, __format__, ## __VA_ARGS__)
	#define SYSLOG_NS_ERR(__format__, ...) SYSLOG_ERR(NS, COMMON, __format__, ## __VA_ARGS__)
#endif

#endif
