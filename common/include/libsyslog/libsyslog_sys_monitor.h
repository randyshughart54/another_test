#ifndef _LIBSYSLOG_SYS_MONITOR_H_INCLUDED_
#define _LIBSYSLOG_SYS_MONITOR_H_INCLUDED_

#include "libsyslog.h"

#ifdef DISABLE_SYSLOG_SYSTEM_SYS_MONITOR
	#define SYS_MONITOR_SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...)
#else
	#define SYS_MONITOR_SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...) \
		SYSLOG_## LEVEL(__system_type__, __subsystem_type__, __format__, ## __VA_ARGS__)
#endif

#define SYSLOG_SYS_MONITOR_BOARD(LEVEL, __format__, ...) \
	SYS_MONITOR_SYSLOG_USE_LEVEL(LEVEL, SYS_MONITOR, BOARD, __format__, ## __VA_ARGS__)

#define SYSLOG_SYS_MONITOR_SYSTEM(LEVEL, __format__, ...) \
	SYS_MONITOR_SYSLOG_USE_LEVEL(LEVEL, SYS_MONITOR, SYSTEM, __format__, ## __VA_ARGS__)

#define SYSLOG_SYS_MONITOR_COMMON(LEVEL, __format__, ...) \
	SYS_MONITOR_SYSLOG_USE_LEVEL(LEVEL, SYS_MONITOR, COMMON, __format__, ## __VA_ARGS__)

#define SYSLOG_SYS_MONITOR_IPC(LEVEL, __format__, ...) \
	SYS_MONITOR_SYSLOG_USE_LEVEL(LEVEL, SYS_MONITOR, IPC, __format__, ## __VA_ARGS__)

#define SYSLOG_SYS_MONITOR_IPC_DTL(LEVEL, __format__, ...) \
	SYS_MONITOR_SYSLOG_USE_LEVEL(LEVEL, SYS_MONITOR, IPC_DTL, __format__, ## __VA_ARGS__)

#define SYSLOG_SYS_MONITOR_THREAD_ALIVE(LEVEL, __format__, ...) \
	SYS_MONITOR_SYSLOG_USE_LEVEL(LEVEL, SYS_MONITOR, THREAD_ALIVE, __format__, ## __VA_ARGS__)

#define SYSLOG_SYS_MONITOR_SVCMON(LEVEL, __format__, ...) \
	SYS_MONITOR_SYSLOG_USE_LEVEL(LEVEL, SYS_MONITOR, SVCMON, __format__, ## __VA_ARGS__)
#endif
