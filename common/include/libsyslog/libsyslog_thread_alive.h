#ifndef _LIBSYSLOG_THREAD_ALIVE_H_INCLUDED_
#define _LIBSYSLOG_THREAD_ALIVE_H_INCLUDED_

#include "libsyslog.h"


#ifdef DISABLE_SYSLOG_SUBSYSTEM_THREAD_ALIVE
	#define SYSLOG_THREAD_ALIVE_DBG(__format__, ...)
#else
	#define SYSLOG_THREAD_ALIVE_DBG(__format__, ...) \
		SYSLOG_DEBUG(THREAD_ALIVE, COMMON, __format__, ## __VA_ARGS__)
#endif

#ifdef DISABLE_SYSLOG_SUBSYSTEM_THREAD_ALIVE
	#define SYSLOG_THREAD_ALIVE_ERR(__format__, ...)
#else
	#define SYSLOG_THREAD_ALIVE_ERR(__format__, ...) \
		SYSLOG_ERR(THREAD_ALIVE, COMMON, __format__, ## __VA_ARGS__)
#endif

#endif
