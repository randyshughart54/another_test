#ifndef _LIBSYSLOG_OSPF_H_INCLUDED_
#define _LIBSYSLOG_OSPF_H_INCLUDED_

#include "libsyslog.h"

#ifdef DISABLE_SYSLOG_SYSTEM_OSPF_V2
	#define SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...)
#else
	#define SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...) \
		SYSLOG_## LEVEL(__system_type__, __subsystem_type__, __format__, ## __VA_ARGS__)
#endif

#define SYSLOG_OSPF_V2_PM(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, OSPF_V2, PM, __format__, ## __VA_ARGS__)

#define SYSLOG_OSPF_V2_NM(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, OSPF_V2, NM, __format__, ## __VA_ARGS__)

#define SYSLOG_OSPF_V2_ADJCHANGE(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, OSPF_V2, ADJCHANGE, __format__, ## __VA_ARGS__)

#endif //_LIBSYSLOG_OSPF_H_INCLUDED_
