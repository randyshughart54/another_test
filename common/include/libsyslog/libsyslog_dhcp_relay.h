#ifndef _LIBSYSLOG_DHCP_RELAY_H_INCLUDED_
#define _LIBSYSLOG_DHCP_RELAY_H_INCLUDED_

#include "libsyslog.h"

#ifdef DISABLE_SYSLOG_SYSTEM_DHCP_RELAY
	#define IF_SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...)
#else
	#define IF_SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...) \
		SYSLOG_## LEVEL(__system_type__, __subsystem_type__, __format__, ## __VA_ARGS__)
#endif

#define SYSLOG_DHCP_RELAY_CMD(LEVEL, __format__, ...) \
	IF_SYSLOG_USE_LEVEL(LEVEL, DHCP_RELAY, CMD, __format__, ## __VA_ARGS__)

#define SYSLOG_DHCP_RELAY_MGMT(LEVEL, __format__, ...) \
	IF_SYSLOG_USE_LEVEL(LEVEL, DHCP_RELAY, MGMT, __format__, ## __VA_ARGS__)

#define SYSLOG_DHCP_RELAY_GENERAL(LEVEL, __format__, ...) \
	IF_SYSLOG_USE_LEVEL(LEVEL, DHCP_RELAY, GENERAL, __format__, ## __VA_ARGS__)

#define SYSLOG_DHCP_RELAY_IPC(LEVEL, __format__, ...) \
	IF_SYSLOG_USE_LEVEL(LEVEL, DHCP_RELAY, IPC, __format__, ## __VA_ARGS__)

#define SYSLOG_DHCP_RELAY_IPC_DTL(LEVEL, __format__, ...) \
	IF_SYSLOG_USE_LEVEL(LEVEL, DHCP_RELAY, IPC_DTL, __format__, ## __VA_ARGS__)

#define SYSLOG_DHCP_RELAY_THREAD_ALIVE(LEVEL, __format__, ...) \
	IF_SYSLOG_USE_LEVEL(LEVEL, DHCP_RELAY, THREAD_ALIVE, __format__, ## __VA_ARGS__)

#define SYSLOG_DHCP_RELAY_SVCMON(LEVEL, __format__, ...) \
	IF_SYSLOG_USE_LEVEL(LEVEL, DHCP_RELAY, SVCMON, __format__, ## __VA_ARGS__)
#endif
