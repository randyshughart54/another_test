#ifndef _LIBSYSLOG_UDLD_H_INCLUDED_
#define _LIBSYSLOG_UDLD_H_INCLUDED_

#include "libsyslog.h"

#ifdef DISABLE_SYSLOG_SYSTEM_UDLD
	#define SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...)
#else
	#define SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...) \
		SYSLOG_## LEVEL(__system_type__, __subsystem_type__, __format__, ## __VA_ARGS__)
#endif

#define SYSLOG_UDLD_INIT(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, UDLD, INIT, __format__, ## __VA_ARGS__)

#define SYSLOG_UDLD_IPC(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, UDLD, IPC, __format__, ## __VA_ARGS__)

#define SYSLOG_UDLD_IPC_DTL(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, UDLD, IPC_DTL, __format__, ## __VA_ARGS__)

#define SYSLOG_UDLD_CMD(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, UDLD, CMD, __format__, ## __VA_ARGS__)

#define SYSLOG_UDLD_SOCKET(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, UDLD, SOCKET, __format__, ## __VA_ARGS__)

#define SYSLOG_UDLD_GENERAL(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, UDLD, GENERAL, __format__, ## __VA_ARGS__)

#define SYSLOG_UDLD_CONFIG(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, UDLD, CONFIG, __format__, ## __VA_ARGS__)

#define SYSLOG_UDLD_TIMER(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, UDLD, TIMER, __format__, ## __VA_ARGS__)

#define SYSLOG_UDLD_RX(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, UDLD, RX, __format__, ## __VA_ARGS__)

#define SYSLOG_UDLD_TX(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, UDLD, TX, __format__, ## __VA_ARGS__)

#define SYSLOG_UDLD_NBR_SM(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, UDLD, NBR_SM, __format__, ## __VA_ARGS__)

#define SYSLOG_UDLD_NBR_SM_DTL(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, UDLD, NBR_SM_DTL, __format__, ## __VA_ARGS__)

#define SYSLOG_UDLD_PORT_SM(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, UDLD, PORT_SM, __format__, ## __VA_ARGS__)

#define SYSLOG_UDLD_PORT_SM_DTL(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, UDLD, PORT_SM_DTL, __format__, ## __VA_ARGS__)

#define SYSLOG_UDLD_THREAD_ALIVE(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, UDLD, THREAD_ALIVE, __format__, ## __VA_ARGS__)

#define SYSLOG_UDLD_SVCMON(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, UDLD, SVCMON, __format__, ## __VA_ARGS__)

#endif //_LIBSYSLOG_UDLD_H_INCLUDED_
