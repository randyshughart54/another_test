#ifndef _LIBSYSLOG_BGP_H_INCLUDED_
#define _LIBSYSLOG_BGP_H_INCLUDED_

#include "libsyslog.h"

#ifdef DISABLE_SYSLOG_SYSTEM_BGP
	#define SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...)
#else
	#define SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...) \
		SYSLOG_## LEVEL(__system_type__, __subsystem_type__, __format__, ## __VA_ARGS__)
#endif

#define SYSLOG_BGP_RM(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, BGP, RM, __format__, ## __VA_ARGS__)

#define SYSLOG_BGP_NM(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, BGP, NM, __format__, ## __VA_ARGS__)

#define SYSLOG_BGP_ML(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, BGP, ML, __format__, ## __VA_ARGS__)

#define SYSLOG_BGP_RA(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, BGP, RA, __format__, ## __VA_ARGS__)

#define SYSLOG_BGP_PM(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, BGP, PM, __format__, ## __VA_ARGS__)

#define SYSLOG_BGP_UM(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, BGP, UM, __format__, ## __VA_ARGS__)

#define SYSLOG_BGP_DC(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, BGP, DC, __format__, ## __VA_ARGS__)

#define SYSLOG_BGP_COMMON(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, BGP, COMMON, __format__, ## __VA_ARGS__)

#define SYSLOG_BGP_ADJCHANGE(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, BGP, ADJCHANGE, __format__, ## __VA_ARGS__)

#define SYSLOG_BGP_MAXPFXEXCEED(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, BGP, MAXPFXEXCEED, __format__, ## __VA_ARGS__)

#endif //_LIBSYSLOG_BGP_H_INCLUDED_
