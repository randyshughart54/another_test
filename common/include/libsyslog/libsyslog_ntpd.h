#ifndef _LIBSYSLOG_NTPD_H_INCLUDED_
#define _LIBSYSLOG_NTPD_H_INCLUDED_

#include "libsyslog.h"

#ifdef DISABLE_SYSLOG_SUBSYSTEM_NTPD
	#define SYSLOG_NTPD_ERROR(__format__, ...)
#else
	#define SYSLOG_NTPD_ERROR(__format__, ...) \
		SYSLOG_ERR(NTPD, COMMON, __format__, ## __VA_ARGS__)
#endif

#ifdef DISABLE_SYSLOG_SUBSYSTEM_NTPD
	#define SYSLOG_NTPD_DEBUG(__format__, ...)
#else
	#define SYSLOG_NTPD_DEBUG(__format__, ...) \
		SYSLOG_DEBUG(NTPD, COMMON, __format__, ## __VA_ARGS__)
#endif

#ifdef DISABLE_SYSLOG_SUBSYSTEM_NTPD
	#define SYSLOG_NTPD_INFO(__format__, ...)
#else
	#define SYSLOG_NTPD_INFO(__format__, ...) \
		SYSLOG_INFO(NTPD, COMMON, __format__, ## __VA_ARGS__)
#endif

#ifdef DISABLE_SYSLOG_SUBSYSTEM_NTPD
	#define SYSLOG_NTPD_CRITICAL(__format__, ...)
#else
	#define SYSLOG_NTPD_CRITICAL(__format__, ...) \
		SYSLOG_CRIT(NTPD, COMMON, __format__, ## __VA_ARGS__)
#endif

#endif
