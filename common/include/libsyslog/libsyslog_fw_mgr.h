#ifndef _LIBSYSLOG_FW_MGR_H_INCLUDED_
#define _LIBSYSLOG_FW_MGR_H_INCLUDED_

#include "libsyslog.h"

#ifdef DISABLE_SYSLOG_SYSTEM_FW_MGR
	#define FW_MGR_SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...)
#else
	#define FW_MGR_SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...) \
		SYSLOG_## LEVEL(__system_type__, __subsystem_type__, __format__, ## __VA_ARGS__)
#endif

#define SYSLOG_FW_MGR_COMMON(LEVEL, __format__, ...) \
	FW_MGR_SYSLOG_USE_LEVEL(LEVEL, FW_MGR, COMMON, __format__, ## __VA_ARGS__)

#define SYSLOG_FW_MGR_IPC(LEVEL, __format__, ...) \
	FW_MGR_SYSLOG_USE_LEVEL(LEVEL, FW_MGR, IPC, __format__, ## __VA_ARGS__)

#define SYSLOG_FW_MGR_IPC_DTL(LEVEL, __format__, ...) \
	FW_MGR_SYSLOG_USE_LEVEL(LEVEL, FW_MGR, IPC_DTL, __format__, ## __VA_ARGS__)

#define SYSLOG_FW_MGR_THREAD_ALIVE(LEVEL, __format__, ...) \
	FW_MGR_SYSLOG_USE_LEVEL(LEVEL, FW_MGR, THREAD_ALIVE, __format__, ## __VA_ARGS__)

#define SYSLOG_FW_MGR_SVCMON(LEVEL, __format__, ...) \
	FW_MGR_SYSLOG_USE_LEVEL(LEVEL, FW_MGR, SVCMON, __format__, ## __VA_ARGS__)

#endif
