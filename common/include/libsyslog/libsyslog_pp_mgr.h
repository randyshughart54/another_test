#ifndef _LIBSYSLOG_PP_MGR_H_INCLUDED_
#define _LIBSYSLOG_PP_MGR_H_INCLUDED_

#include "libsyslog.h"

#ifdef DISABLE_SYSLOG_SYSTEM_PP_MGR
	#define PP_SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...) \
		do { } while(0)
#else
	#define PP_SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...) \
		do { \
		SYSLOG_## LEVEL(__system_type__, __subsystem_type__, __format__, ## __VA_ARGS__); \
		} while(0)
#endif

#define SYSLOG_PP_MGR_ARP(LEVEL, __format__, ...) \
	PP_SYSLOG_USE_LEVEL(LEVEL, PP_MGR, ARP, __format__, ## __VA_ARGS__)

#define SYSLOG_PP_MGR_ARP_DTL(LEVEL, __format__, ...) \
	PP_SYSLOG_USE_LEVEL(LEVEL, PP_MGR, ARP_DTL, __format__, ## __VA_ARGS__)

#define SYSLOG_PP_MGR_CMD(LEVEL, __format__, ...) \
	PP_SYSLOG_USE_LEVEL(LEVEL, PP_MGR, CMD, __format__, ## __VA_ARGS__)

#define SYSLOG_PP_MGR_GENERAL(LEVEL, __format__, ...) \
	PP_SYSLOG_USE_LEVEL(LEVEL, PP_MGR, GENERAL, __format__, ## __VA_ARGS__)

#define SYSLOG_PP_MGR_L2(LEVEL, __format__, ...) \
	PP_SYSLOG_USE_LEVEL(LEVEL, PP_MGR, L2, __format__, ## __VA_ARGS__)

#define SYSLOG_PP_MGR_L2_MAC(LEVEL, __format__, ...) \
	PP_SYSLOG_USE_LEVEL(LEVEL, PP_MGR, L2_MAC, __format__, ## __VA_ARGS__)

#define SYSLOG_PP_MGR_MPLS(LEVEL, __format__, ...) \
	PP_SYSLOG_USE_LEVEL(LEVEL, PP_MGR, MPLS, __format__, ## __VA_ARGS__)

#define SYSLOG_PP_MGR_INTERFACES(LEVEL, __format__, ...) \
	PP_SYSLOG_USE_LEVEL(LEVEL, PP_MGR, INTERFACES, __format__, ## __VA_ARGS__)

#define SYSLOG_PP_MGR_EGRESS_OBJ(LEVEL, __format__, ...) \
	PP_SYSLOG_USE_LEVEL(LEVEL, PP_MGR, EGRESS_OBJ, __format__, ## __VA_ARGS__)

#define SYSLOG_PP_MGR_ROUTES(LEVEL, __format__, ...) \
	PP_SYSLOG_USE_LEVEL(LEVEL, PP_MGR, ROUTES, __format__, ## __VA_ARGS__)

#define SYSLOG_PP_MGR_IP_ADDRS(LEVEL, __format__, ...) \
	PP_SYSLOG_USE_LEVEL(LEVEL, PP_MGR, IP_ADDRS, __format__, ## __VA_ARGS__)

#define SYSLOG_PP_MGR_SFP_MONITORING(LEVEL, __format__, ...) \
	PP_SYSLOG_USE_LEVEL(LEVEL, PP_MGR, SFP_MONITORING, __format__, ## __VA_ARGS__)

#define SYSLOG_PP_MGR_SYNC(LEVEL, __format__, ...) \
	PP_SYSLOG_USE_LEVEL(LEVEL, PP_MGR, SYNC, __format__, ## __VA_ARGS__)

#define SYSLOG_PP_MGR_VPN(LEVEL, __format__, ...) \
	PP_SYSLOG_USE_LEVEL(LEVEL, PP_MGR, VPN, __format__, ## __VA_ARGS__)

#define SYSLOG_PP_MGR_HW_API(LEVEL, __format__, ...) \
	PP_SYSLOG_USE_LEVEL(LEVEL, PP_MGR, HW_API, __format__, ## __VA_ARGS__)

#define SYSLOG_PP_MGR_IPC(LEVEL, __format__, ...) \
	PP_SYSLOG_USE_LEVEL(LEVEL, PP_MGR, IPC, __format__, ## __VA_ARGS__)

#define SYSLOG_PP_MGR_IPC_DTL(LEVEL, __format__, ...) \
	PP_SYSLOG_USE_LEVEL(LEVEL, PP_MGR, IPC_DTL, __format__, ## __VA_ARGS__)

#define SYSLOG_PP_MGR_COUNTERS(LEVEL, __format__, ...) \
	PP_SYSLOG_USE_LEVEL(LEVEL, PP_MGR, COUNTERS, __format__, ## __VA_ARGS__)

#define SYSLOG_PP_MGR_COUNTERS_DTL(LEVEL, __format__, ...) \
	PP_SYSLOG_USE_LEVEL(LEVEL, PP_MGR, COUNTERS_DTL, __format__, ## __VA_ARGS__)

#define SYSLOG_PP_MGR_BFD(LEVEL, __format__, ...) \
	PP_SYSLOG_USE_LEVEL(LEVEL, PP_MGR, BFD, __format__, ## __VA_ARGS__)

#define SYSLOG_PP_MGR_MULTICAST(LEVEL, __format__, ...) \
	PP_SYSLOG_USE_LEVEL(LEVEL, PP_MGR, MULTICAST, __format__, ## __VA_ARGS__)

#define SYSLOG_PP_MGR_MULTICAST_DTL(LEVEL, __format__, ...) \
	PP_SYSLOG_USE_LEVEL(LEVEL, PP_MGR, MULTICAST_DTL, __format__, ## __VA_ARGS__)

#define SYSLOG_PP_MGR_QOS(LEVEL, __format__, ...) \
	PP_SYSLOG_USE_LEVEL(LEVEL, PP_MGR, QOS, __format__, ## __VA_ARGS__)

#define SYSLOG_PP_MGR_THREAD_ALIVE(LEVEL, __format__, ...) \
	PP_SYSLOG_USE_LEVEL(LEVEL, PP_MGR, THREAD_ALIVE, __format__, ## __VA_ARGS__)

#define SYSLOG_PP_MGR_STAT(LEVEL, __format__, ...) \
	PP_SYSLOG_USE_LEVEL(LEVEL, PP_MGR, STAT, __format__, ## __VA_ARGS__)

#define SYSLOG_PP_MGR_ACL(LEVEL, __format__, ...) \
	PP_SYSLOG_USE_LEVEL(LEVEL, PP_MGR, ACL, __format__, ## __VA_ARGS__)

#define SYSLOG_PP_MGR_SVCMON(LEVEL, __format__, ...) \
	PP_SYSLOG_USE_LEVEL(LEVEL, PP_MGR, SVCMON, __format__, ## __VA_ARGS__)

#define SYSLOG_PP_MGR_SG(LEVEL, __format__, ...) \
	PP_SYSLOG_USE_LEVEL(LEVEL, PP_MGR, SG, __format__, ## __VA_ARGS__)

#define SYSLOG_PP_MGR_SG_DTL(LEVEL, __format__, ...) \
	PP_SYSLOG_USE_LEVEL(LEVEL, PP_MGR, SG_DTL, __format__, ## __VA_ARGS__)

#define SYSLOG_PP_MGR_BD_STORM_CTRL(LEVEL, __format__, ...) \
	PP_SYSLOG_USE_LEVEL(LEVEL, PP_MGR, BD_STORM_CTRL, __format__, ## __VA_ARGS__)

#define SYSLOG_PP_MGR_IP_TUNNEL(LEVEL, __format__, ...) \
	PP_SYSLOG_USE_LEVEL(LEVEL, PP_MGR, IP_TUNNEL, __format__, ## __VA_ARGS__)

#endif
