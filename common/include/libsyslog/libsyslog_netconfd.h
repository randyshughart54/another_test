#ifndef _LIBSYSLOG_NETCONFD_H_INCLUDED_
#define _LIBSYSLOG_NETCONFD_H_INCLUDED_

#include "libsyslog.h"

#ifdef DISABLE_SYSLOG_SYSTEM_NETCONFD
	#define SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...)
#else
	#define SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...) \
		SYSLOG_## LEVEL(__system_type__, __subsystem_type__, __format__, ## __VA_ARGS__)
#endif

#define SYSLOG_NETCONFD_COMMON(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, NETCONFD, COMMON, __format__, ## __VA_ARGS__)

#define SYSLOG_NETCONFD_CFG(LEVEL, __format__, ...) \
	if (SYSLOG_get_netconfd_cfg_flag()) SYSLOG_USE_LEVEL(LEVEL, NETCONFD, CFG, __format__, ## __VA_ARGS__)

#define SYSLOG_NETCONFD_SVCMON(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, NETCONFD, SVCMON, __format__, ## __VA_ARGS__)

#endif
