#ifndef _LIBSYSLOG_CP_H_INCLUDED_
#define _LIBSYSLOG_CP_H_INCLUDED_

#include "libsyslog.h"

#ifdef DISABLE_SYSLOG_SYSTEM_CP
	#define SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...)
#else
	#define SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...) \
		SYSLOG_## LEVEL(__system_type__, __subsystem_type__, __format__, ## __VA_ARGS__)
#endif

#define SYSLOG_CP_INTERNAL(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, CP, INTERNAL, __format__, ## __VA_ARGS__)

#define SYSLOG_CP_SM(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, CP, SM, __format__, ## __VA_ARGS__)

#define SYSLOG_CP_I3(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, CP, I3, __format__, ## __VA_ARGS__)

#define SYSLOG_CP_LA(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, CP, LA, __format__, ## __VA_ARGS__)

#define SYSLOG_CP_BD(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, CP, BD, __format__, ## __VA_ARGS__)

#define SYSLOG_CP_SCK(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, CP, SCK, __format__, ## __VA_ARGS__)

#define SYSLOG_CP_DLS(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, CP, DLS, __format__, ## __VA_ARGS__)

#define SYSLOG_CP_CSS(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, CP, CSS, __format__, ## __VA_ARGS__)

#endif //_LIBSYSLOG_CP_H_INCLUDED_