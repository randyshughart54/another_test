#ifndef _LIBSYSLOG_SVCMON_H_INCLUDED_
#define _LIBSYSLOG_SVCMON_H_INCLUDED_

#include "libsyslog.h"

#ifdef DISABLE_SYSLOG_SUBSYSTEM_SVCMON
	#define SYSLOG_SVCMON_DBG(__format__, ...)
#else
	#define SYSLOG_SVCMON_DBG(__format__, ...) \
		SYSLOG_DEBUG(SVCMON, COMMON, __format__, ## __VA_ARGS__)
#endif

#ifdef DISABLE_SYSLOG_SUBSYSTEM_SVCMON
	#define SYSLOG_SVCMON_ERR(__format__, ...)
#else
	#define SYSLOG_SVCMON_ERR(__format__, ...) \
		SYSLOG_ERR(SVCMON, COMMON, __format__, ## __VA_ARGS__)
#endif

#ifdef DISABLE_SYSLOG_SUBSYSTEM_SVCMON
	#define SYSLOG_SVCMON_INFO(__format__, ...)
#else
	#define SYSLOG_SVCMON_INFO(__format__, ...) \
		SYSLOG_INFO(SVCMON, COMMON, __format__, ## __VA_ARGS__)
#endif

#endif
