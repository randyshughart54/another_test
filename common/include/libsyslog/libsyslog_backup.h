#ifndef _LIBSYSLOG_BACKUP_H_INCLUDED_
#define _LIBSYSLOG_BACKUP_H_INCLUDED_

#include "libsyslog.h"
#include <string.h>


// =============================================================================
// === COMMON ==================================================================
// =============================================================================
/* Common error messages for services*/
#define ERR_NO_FILE_MSG "No such file or directory"
#define ERR_IS_A_DIRECTORY_MSG "Is a directory"
#define ERR_NOT_A_DIRECTORY_MSG "Not a directory"
#define ERR_NETWORK_UNREACH_MSG "Network is unreachable"
#define ERR_CONNECTION_REFUSED_MSG "Connection refused"
#define ERR_PERMISSION_DENIED_MSG "Permission denied"
#define ERR_PERMISSION_DENIED_TRY_AGAIN_MSG "Permission denied, please try again"
#define ERR_NO_ROUTE_MSG "No route to host"


typedef enum {
	VRF_ERR_UNKNOWN = 1,
	VRF_ERR_NOT_EXIST = 150,
} vrf_err_code_t;

typedef enum {
	FW_IMAGE_ERR_UNKNOWN = 1,
	FW_IMAGE_ERR_UNSUPPORTED_OR_CORRUPTED = 151,
} fw_image_err_code_t;

static inline const char * vrf_rc_stringize(vrf_err_code_t err)
{
	switch(err)
	{
		case VRF_ERR_NOT_EXIST:
			return "This VRF does not exist";
		case VRF_ERR_UNKNOWN:
		default:
			return "Unknown error";
	}
}

static inline const char * fw_image_rc_stringize(fw_image_err_code_t err)
{
	switch(err)
	{
		case FW_IMAGE_ERR_UNSUPPORTED_OR_CORRUPTED:
			return "Firmware image unsupported or corrupted";
		case FW_IMAGE_ERR_UNKNOWN:
		default:
			return "Unknown error";
	}
}

#define SYSLOG_BACKUP_COMMON(__format__, ...) \
	SYSLOG_INFO(BACKUP, COMMON, __format__, ## __VA_ARGS__);
#endif

// =============================================================================
// === FTP =====================================================================
// =============================================================================
#define FTPEXITSTATUS(x) ((x) & 0xff)

typedef enum {
	FTP_ERR_UNKNOWN = -1,
	FTP_ERR_NO = 0,
	FTP_ERR_331 = FTPEXITSTATUS(331),
	FTP_ERR_332 = FTPEXITSTATUS(332),
	FTP_ERR_350 = FTPEXITSTATUS(350),
	FTP_ERR_421 = FTPEXITSTATUS(421),
	FTP_ERR_425 = FTPEXITSTATUS(425),
	FTP_ERR_426 = FTPEXITSTATUS(426),
	FTP_ERR_430 = FTPEXITSTATUS(430),
	FTP_ERR_434 = FTPEXITSTATUS(434),
	FTP_ERR_450 = FTPEXITSTATUS(450),
	FTP_ERR_451 = FTPEXITSTATUS(451),
	FTP_ERR_452 = FTPEXITSTATUS(452),
	FTP_ERR_501 = FTPEXITSTATUS(501),
	FTP_ERR_502 = FTPEXITSTATUS(502),
	FTP_ERR_503 = FTPEXITSTATUS(503),
	FTP_ERR_504 = FTPEXITSTATUS(504),
	FTP_ERR_530 = FTPEXITSTATUS(530),
	FTP_ERR_532 = FTPEXITSTATUS(532),
	FTP_ERR_550 = FTPEXITSTATUS(550),
	FTP_ERR_551 = FTPEXITSTATUS(551),
	FTP_ERR_552 = FTPEXITSTATUS(552),
	FTP_ERR_553 = FTPEXITSTATUS(553),
} ftp_err_code_t;

static inline const char * ftp_rc_stringize(ftp_err_code_t err)
{
	switch(err)
	{
		case FTP_ERR_NO:
			return "Backup operation successfully";
		case FTP_ERR_331:
			return "User name okay, need password";
		case FTP_ERR_332:
			return "Need account for login";
		case FTP_ERR_350:
			return "Requested file action pending further information";
		case FTP_ERR_421:
			return "Service not available, closing control connection. "
			       "This may be a reply to any command if the service "
			       "knows it must shut down";
		case FTP_ERR_425:
			return "Can't open data connection";
		case FTP_ERR_426:
			return "Connection closed; transfer aborted";
		case FTP_ERR_430:
			return "Invalid username or password";
		case FTP_ERR_434:
			return "Requested host unavailable";
		case FTP_ERR_450:
			return "Requested file action not taken";
		case FTP_ERR_451:
			return "Requested action aborted. Local error in processing";
		case FTP_ERR_452:
			return "Requested action not taken. Insufficient storage "
			       "space in system.File unavailable (e.g., file busy)";
		case FTP_ERR_501:
			return "Syntax error in parameters or arguments";
		case FTP_ERR_502:
			return "Command not implemented";
		case FTP_ERR_503:
			return "Bad sequence of commands";
		case FTP_ERR_504:
			return "Command not implemented for that parameter";
		case FTP_ERR_530:
			return "Not logged in";
		case FTP_ERR_532:
			return "Need account for storing files";
		case FTP_ERR_550:
			return "Requested action not taken. File unavailable (e.g., file not found, no access)";
		case FTP_ERR_551:
			return "Requested action aborted. Page type unknown";
		case FTP_ERR_552:
			return "Requested file action aborted. "
			       "Exceeded storage allocation "
			       "(for current directory or dataset)";
		case FTP_ERR_553:
			return "Requested action not taken. File name not allowed";
		case FTP_ERR_UNKNOWN:
		default:
			return "Unknown error";
	}
}

#define SYSLOG_BACKUP_FTP(__format__, ...) \
	SYSLOG_INFO(BACKUP, FTP, __format__, ## __VA_ARGS__);

// =============================================================================
// === SCP =====================================================================
// =============================================================================
typedef enum {
	SCP_ERR_NO = 0,
	SCP_ERR_UNKNOWN,
	SCP_ERR_INVALID_ARG,
	SCP_ERR_CONFLICT_ARG,
	SCP_ERR_RUNTIME,
	SCP_ERR_UNRECOGNIZED,
	SCP_ERR_FILE,
	SCP_ERR_IS_A_DIRECTORY,
	SCP_ERR_NOT_A_DIRECTORY,
	SCP_ERR_PASS,
	SCP_ERR_KEY,
	SCP_ERR_NETWORK,
	SCP_ERR_ROUTE,
	SCP_ERR_CONNECTION,
	SCP_ERR_PERMISSION_DENIED_TRY_AGAIN,
	SCP_ERR_PERMISSION_DENIED,
} scp_err_code_t;

static inline const char * scp_rc_stringize(scp_err_code_t err)
{
	switch(err)
	{
		case SCP_ERR_NO:
			return "Backup operation successfully";
		case SCP_ERR_INVALID_ARG:
			return "Invalid command line argument";
		case SCP_ERR_CONFLICT_ARG:
			return "Conflicting arguments given";
		case SCP_ERR_RUNTIME:
			return "General runtime error";
		case SCP_ERR_UNRECOGNIZED:
			return "Unrecognized response from ssh";
		case SCP_ERR_FILE:
			return "No such file or directory";
		case SCP_ERR_IS_A_DIRECTORY:
			return "Is a directory";
		case SCP_ERR_NOT_A_DIRECTORY:
			return "Not a directory";
		case SCP_ERR_PASS:
			return "Invalid/incorrect password";
		case SCP_ERR_KEY:
			return "Host public key is unknown";
		case SCP_ERR_NETWORK:
			return "Network is unreachable";
		case SCP_ERR_ROUTE:
			return "No route to host";
		case SCP_ERR_CONNECTION:
			return "Connection refused";
		case SCP_ERR_PERMISSION_DENIED_TRY_AGAIN:
			return "Permission denied, please try again";
		case SCP_ERR_PERMISSION_DENIED:
			return "Permission denied";
		case SCP_ERR_UNKNOWN:
		default:
			return "Unknown error";
	}
}

#define SYSLOG_BACKUP_SCP(__format__, ...) \
	SYSLOG_INFO(BACKUP, SCP, __format__, ## __VA_ARGS__);

// =============================================================================
// === TFTP ====================================================================
// =============================================================================
typedef enum {
	TFTP_ERR_NO = 0,
	TFTP_ERR_CLIENT_UNKNOWN,
	TFTP_ERR_CLIENT_LONG_NAME,
	TFTP_ERR_CLIENT_NOFILE,
	TFTP_ERR_CLIENT_TIMEOUT,
	TFTP_ERR_CLIENT_BAD_OPT,
	TFTP_ERR_CLIENT_WRITE,
	TFTP_ERR_SERVER_UNKNOWN,
	TFTP_ERR_SERVER_NOFILE,
	TFTP_ERR_SERVER_ACCESS,
	TFTP_ERR_SERVER_WRITE,
	TFTP_ERR_SERVER_OP,
	TFTP_ERR_SERVER_BAD_ID,
	TFTP_ERR_SERVER_EXIST,
	TFTP_ERR_SERVER_BAD_USER,
	TFTP_ERR_SERVER_BAD_OPT,
	TFTP_ERR_SERVER_IS_A_DIRECTORY,
	TFTP_ERR_SERVER_NOT_A_DIRECTORY,
	TFTP_ERR_SERVER_PERMISSION_DENIED,
	TFTP_ERR_CLIENT_NETWORK,
	TFTP_ERR_MAX
} tftp_err_code_t;

static inline const char * tftp_rc_stringize(tftp_err_code_t err)
{
	switch(err)
	{
		case TFTP_ERR_NO:
			return "Backup operation successfully";
		case TFTP_ERR_CLIENT_UNKNOWN:
			return "Client error: unknown error";
		case TFTP_ERR_CLIENT_LONG_NAME:
			return "Client error: remote filename is too long";
		case TFTP_ERR_CLIENT_NOFILE:
			return "Client error: file not found";
		case TFTP_ERR_CLIENT_TIMEOUT:
			return "Client error: timeout";
		case TFTP_ERR_CLIENT_BAD_OPT:
			return "Client error: bad option";
		case TFTP_ERR_CLIENT_WRITE:
			return "Client error: write";
		case TFTP_ERR_CLIENT_NETWORK:
			return "Client error: network is unreachable";
		case TFTP_ERR_SERVER_UNKNOWN:
			return "Server answer: unknown error";
		case TFTP_ERR_SERVER_NOFILE:
			return "Server answer: file not found";
		case TFTP_ERR_SERVER_ACCESS:
			return "Server answer: access violation";
		case TFTP_ERR_SERVER_WRITE:
			return "Server answer: disk full";
		case TFTP_ERR_SERVER_OP:
			return "Server answer: bad operation";
		case TFTP_ERR_SERVER_BAD_ID:
			return "Server answer: unknown transfer id";
		case TFTP_ERR_SERVER_EXIST:
			return "Server answer: file already exists";
		case TFTP_ERR_SERVER_BAD_USER:
			return "Server answer: no such user";
		case TFTP_ERR_SERVER_BAD_OPT:
			return "Server answer: bad option";
		case TFTP_ERR_SERVER_IS_A_DIRECTORY:
			return "Server answer: is a directory";
		case TFTP_ERR_SERVER_NOT_A_DIRECTORY:
			return "Server answer: not a directory";
		case TFTP_ERR_SERVER_PERMISSION_DENIED:
			return "Server answer: permission denied";
		default:
			return "Unknown error";
	}
}


#define SYSLOG_BACKUP_TFTP(__format__, ...) \
	SYSLOG_INFO(BACKUP, TFTP, __format__, ## __VA_ARGS__);

// =============================================================================
// === SFTP ====================================================================
// =============================================================================
typedef enum {
	SFTP_ERR_NO = 0,
	SFTP_ERR_UNKNOWN,
	SFTP_ERR_FILE_NOT_FOUND,
	SFTP_ERR_NETWORK = 11,
	SFTP_ERR_ROUTE,
	SFTP_ERR_CONNECTION,
	SFTP_ERR_PERMISSION_DENIED_TRY_AGAIN,
	SFTP_ERR_PERMISSION_DENIED
} sftp_err_code_t;

static inline const char * sftp_rc_stringize(sftp_err_code_t err)
{
	switch(err)
	{
		case SFTP_ERR_NO:
			return "Backup operation successfully";
		case SFTP_ERR_FILE_NOT_FOUND:
			return "File not found";
		case SFTP_ERR_NETWORK:
			return "Network is unreachable";
		case SFTP_ERR_ROUTE:
			return "No route to host";
		case SFTP_ERR_CONNECTION:
			return "Connection refused";
		case SFTP_ERR_PERMISSION_DENIED_TRY_AGAIN:
			return "Permission denied, please try again";
		case SFTP_ERR_PERMISSION_DENIED:
			return "Permission denied";
		case SFTP_ERR_UNKNOWN:
		default:
			return "Unknown error";
	}
}

#define SYSLOG_BACKUP_SFTP(__format__, ...) \
	SYSLOG_INFO(BACKUP, SFTP, __format__, ## __VA_ARGS__);

static inline const char * rem_rc_stringize(char * type, int err)
{
	if (err == VRF_ERR_NOT_EXIST)
		return vrf_rc_stringize(err);

	if (err == FW_IMAGE_ERR_UNSUPPORTED_OR_CORRUPTED)
		return fw_image_rc_stringize(err);

	if (!strcasecmp(type, "SCP"))
		return scp_rc_stringize(err);
	else if (!strcasecmp(type, "TFTP"))
		return tftp_rc_stringize(err);
	else if (!strcasecmp(type, "FTP"))
		return ftp_rc_stringize(FTPEXITSTATUS(err));
	else if (!strcasecmp(type, "SFTP"))
		return sftp_rc_stringize(err);
	else if (!strcasecmp(type, "USB"))
		return strerror(err);
	else
		return "Unknown error";
}
