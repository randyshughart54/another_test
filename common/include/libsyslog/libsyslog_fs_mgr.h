#pragma once

#include "libsyslog.h"

#ifdef DISABLE_SYSLOG_SYSTEM_FS_MGR
#define SYSLOG_FS_MGR(...)
#else
#define SYSLOG_FS_MGR(subsystem, level, ...)\
    SYSLOG_ ## level(FS_MGR, subsystem, ##__VA_ARGS__)
#endif
