#ifndef _LIBSYSLOG_LDP_H_INCLUDED_
#define _LIBSYSLOG_LDP_H_INCLUDED_

#include "libsyslog.h"

#ifdef DISABLE_SYSLOG_SYSTEM_LDP
	#define SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...)
#else
	#define SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...) \
		SYSLOG_## LEVEL(__system_type__, __subsystem_type__, __format__, ## __VA_ARGS__)
#endif

#define SYSLOG_LDP_PM(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, LDP, PM, __format__, ## __VA_ARGS__)

#define SYSLOG_LDP_ADJCHANGE(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, LDP, ADJCHANGE, __format__, ## __VA_ARGS__)
#endif //_LIBSYSLOG_LDP_H_INCLUDED_
