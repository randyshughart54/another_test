#ifndef _LIBSYSLOG_FW_LIB_H_INCLUDED_
#define _LIBSYSLOG_FW_LIB_H_INCLUDED_

#include "libsyslog.h"

#ifdef DISABLE_SYSLOG_SYSTEM_FW_LIB
	#define SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...)
#else
	#define SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...) \
		SYSLOG_## LEVEL(__system_type__, __subsystem_type__, __format__, ## __VA_ARGS__)
#endif

#define SYSLOG_FW_LIB_COMMON(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, FW_LIB, COMMON, __format__, ## __VA_ARGS__)

#endif
