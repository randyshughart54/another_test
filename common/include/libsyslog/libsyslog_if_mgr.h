#ifndef _LIBSYSLOG_IF_MGR_H_INCLUDED_
#define _LIBSYSLOG_IF_MGR_H_INCLUDED_

#include "libsyslog.h"

#ifdef DISABLE_SYSLOG_SYSTEM_IF_MGR
	#define IF_SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...)
#else
	#define IF_SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...) \
		SYSLOG_## LEVEL(__system_type__, __subsystem_type__, __format__, ## __VA_ARGS__)
#endif

#define SYSLOG_IF_MGR_CMD(LEVEL, __format__, ...) \
	IF_SYSLOG_USE_LEVEL(LEVEL, IF_MGR, CMD, __format__, ## __VA_ARGS__)

#define SYSLOG_IF_MGR_GENERAL(LEVEL, __format__, ...) \
	IF_SYSLOG_USE_LEVEL(LEVEL, IF_MGR, GENERAL, __format__, ## __VA_ARGS__)

#define SYSLOG_IF_MGR_INTERFACES(LEVEL, __format__, ...) \
	IF_SYSLOG_USE_LEVEL(LEVEL, IF_MGR, INTERFACES, __format__, ## __VA_ARGS__)

#define SYSLOG_IF_MGR_VRF(LEVEL, __format__, ...) \
	IF_SYSLOG_USE_LEVEL(LEVEL, IF_MGR, VRF, __format__, ## __VA_ARGS__)

#define SYSLOG_IF_MGR_VRF_DTL(LEVEL, __format__, ...) \
	IF_SYSLOG_USE_LEVEL(LEVEL, IF_MGR, VRF_DTL, __format__, ## __VA_ARGS__)

#define SYSLOG_IF_MGR_SYNC_SERVICES(LEVEL, __format__, ...) \
	IF_SYSLOG_USE_LEVEL(LEVEL, IF_MGR, SYNC_SERVICES, __format__, ## __VA_ARGS__)

#define SYSLOG_IF_MGR_SYNC_OS(LEVEL, __format__, ...) \
	IF_SYSLOG_USE_LEVEL(LEVEL, IF_MGR, SYNC_OS, __format__, ## __VA_ARGS__)

#define SYSLOG_IF_MGR_SYNC_OS_DTL(LEVEL, __format__, ...) \
	IF_SYSLOG_USE_LEVEL(LEVEL, IF_MGR, SYNC_OS_DTL, __format__, ## __VA_ARGS__)

#define SYSLOG_IF_MGR_SYNC_PHYS(LEVEL, __format__, ...) \
	IF_SYSLOG_USE_LEVEL(LEVEL, IF_MGR, SYNC_PHYS, __format__, ## __VA_ARGS__)

#define SYSLOG_IF_MGR_SYNC_MAPPER(LEVEL, __format__, ...) \
	IF_SYSLOG_USE_LEVEL(LEVEL, IF_MGR, SYNC_MAPPER, __format__, ## __VA_ARGS__)

#define SYSLOG_IF_MGR_SYNC_NEIGHBOR(LEVEL, __format__, ...) \
	IF_SYSLOG_USE_LEVEL(LEVEL, IF_MGR, SYNC_NEIGHBOR, __format__, ## __VA_ARGS__)

#define SYSLOG_IF_MGR_SYNC_UDLD(LEVEL, __format__, ...) \
	IF_SYSLOG_USE_LEVEL(LEVEL, IF_MGR, SYNC_UDLD, __format__, ## __VA_ARGS__)

#define SYSLOG_IF_MGR_IPC(LEVEL, __format__, ...) \
	IF_SYSLOG_USE_LEVEL(LEVEL, IF_MGR, IPC, __format__, ## __VA_ARGS__)

#define SYSLOG_IF_MGR_IPC_DTL(LEVEL, __format__, ...) \
	IF_SYSLOG_USE_LEVEL(LEVEL, IF_MGR, IPC_DTL, __format__, ## __VA_ARGS__)

#define SYSLOG_IF_MGR_LINK_UP(LEVEL, __format__, ...) \
	IF_SYSLOG_USE_LEVEL(LEVEL, IF_MGR, LINK_UP, __format__, ## __VA_ARGS__)

#define SYSLOG_IF_MGR_LINK_DOWN(LEVEL, __format__, ...) \
	IF_SYSLOG_USE_LEVEL(LEVEL, IF_MGR, LINK_DOWN, __format__, ## __VA_ARGS__)

#define SYSLOG_IF_MGR_ADMIN_UP(LEVEL, __format__, ...) \
	IF_SYSLOG_USE_LEVEL(LEVEL, IF_MGR, ADMIN_UP, __format__, ## __VA_ARGS__)

#define SYSLOG_IF_MGR_ADMIN_DOWN(LEVEL, __format__, ...) \
	IF_SYSLOG_USE_LEVEL(LEVEL, IF_MGR, ADMIN_DOWN, __format__, ## __VA_ARGS__)

#define SYSLOG_IF_MGR_THREAD_ALIVE(LEVEL, __format__, ...) \
	IF_SYSLOG_USE_LEVEL(LEVEL, IF_MGR, THREAD_ALIVE, __format__, ## __VA_ARGS__)

#define SYSLOG_IF_MGR_ERRDISABLE(LEVEL, __format__, ...) \
	IF_SYSLOG_USE_LEVEL(LEVEL, IF_MGR, ERRDISABLE, __format__, ## __VA_ARGS__)

#define SYSLOG_IF_MGR_SVCMON(LEVEL, __format__, ...) \
	IF_SYSLOG_USE_LEVEL(LEVEL, IF_MGR, SVCMON, __format__, ## __VA_ARGS__)

#endif
