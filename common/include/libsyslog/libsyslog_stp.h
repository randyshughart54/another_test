#ifndef _LIBSYSLOG_STP_H_INCLUDED_
#define _LIBSYSLOG_STP_H_INCLUDED_

#include "libsyslog.h"

#ifdef DISABLE_SYSLOG_SYSTEM_STP
	#define SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...)
#else
	#define SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...) \
		SYSLOG_## LEVEL(__system_type__, __subsystem_type__, __format__, ## __VA_ARGS__)
#endif

#define SYSLOG_STP_COMMON(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, STP, COMMON, __format__, ## __VA_ARGS__)

#define SYSLOG_STP_PM(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, STP, PM, __format__, ## __VA_ARGS__)

#define SYSLOG_STP_NM(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, STP, NM, __format__, ## __VA_ARGS__)

#endif //_LIBSYSLOG_STP_H_INCLUDED_