#ifndef _LIBSYSLOG_ROUTE_MGR_H_INCLUDED_
#define _LIBSYSLOG_ROUTE_MGR_H_INCLUDED_

#include "libsyslog.h"

#ifdef DISABLE_SYSLOG_SYSTEM_ROUTE_MGR
	#define IF_SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...)
#else
	#define IF_SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...) \
		SYSLOG_## LEVEL(__system_type__, __subsystem_type__, __format__, ## __VA_ARGS__)
#endif

#define SYSLOG_ROUTE_MGR_CMD(LEVEL, __format__, ...) \
	IF_SYSLOG_USE_LEVEL(LEVEL, ROUTE_MGR, CMD, __format__, ## __VA_ARGS__)

#define SYSLOG_ROUTE_MGR_GENERAL(LEVEL, __format__, ...) \
	IF_SYSLOG_USE_LEVEL(LEVEL, ROUTE_MGR, GENERAL, __format__, ## __VA_ARGS__)

#define SYSLOG_ROUTE_MGR_IPC(LEVEL, __format__, ...) \
	IF_SYSLOG_USE_LEVEL(LEVEL, ROUTE_MGR, IPC, __format__, ## __VA_ARGS__)

#define SYSLOG_ROUTE_MGR_IPC_DTL(LEVEL, __format__, ...) \
	IF_SYSLOG_USE_LEVEL(LEVEL, ROUTE_MGR, IPC_DTL, __format__, ## __VA_ARGS__)

#define SYSLOG_ROUTE_MGR_SVCMON(LEVEL, __format__, ...) \
	IF_SYSLOG_USE_LEVEL(LEVEL, ROUTE_MGR, SVCMON, __format__, ## __VA_ARGS__)


#endif
