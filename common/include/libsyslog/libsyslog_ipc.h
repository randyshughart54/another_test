#ifndef _LIBSYSLOG_IPC_H_INCLUDED_
#define _LIBSYSLOG_IPC_H_INCLUDED_

#include "libsyslog.h"


#ifdef DISABLE_SYSLOG_SUBSYSTEM_IPC
	#define SYSLOG_IPC_DBG(__format__, ...)
#else
	#define SYSLOG_IPC_DBG(__format__, ...) \
		SYSLOG_DEBUG(IPC, COMMON, __format__, ## __VA_ARGS__)
#endif

#ifdef DISABLE_SYSLOG_SUBSYSTEM_IPC
	#define SYSLOG_IPC_ERR(__format__, ...)
#else
	#define SYSLOG_IPC_ERR(__format__, ...) \
		SYSLOG_ERR(IPC, COMMON, __format__, ## __VA_ARGS__)
#endif

#ifdef DISABLE_SYSLOG_SUBSYSTEM_IPC
	#define SYSLOG_IPC_INFO(__format__, ...)
#else
	#define SYSLOG_IPC_INFO(__format__, ...) \
		SYSLOG_INFO(IPC, COMMON, __format__, ## __VA_ARGS__)
#endif

#endif
