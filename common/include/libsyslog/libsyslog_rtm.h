#ifndef _LIBSYSLOG_RTM_H_INCLUDED_
#define _LIBSYSLOG_RTM_H_INCLUDED_

#include "libsyslog.h"

#ifdef DISABLE_SYSLOG_SYSTEM_RTM
	#define SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...)
#else
	#define SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...) \
		SYSLOG_## LEVEL(__system_type__, __subsystem_type__, __format__, ## __VA_ARGS__)
#endif

#define SYSLOG_RTM_COMMON(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, RTM, COMMON, __format__, ## __VA_ARGS__)

#define SYSLOG_RTM_QCFT(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, RTM, QCFT, __format__, ## __VA_ARGS__)

#define SYSLOG_RTM_QCCS(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, RTM, QCCS, __format__, ## __VA_ARGS__)

#define SYSLOG_RTM_QCTE(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, RTM, QCTE, __format__, ## __VA_ARGS__)

#define SYSLOG_RTM_QAQL(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, RTM, QAQL, __format__, ## __VA_ARGS__)

#define SYSLOG_RTM_QRML(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, RTM, QRML, __format__, ## __VA_ARGS__)

#endif //_LIBSYSLOG_RTM_H_INCLUDED_