#ifndef _LIBSYSLOG_VRRP_MGR_H_INCLUDED_
#define _LIBSYSLOG_VRRP_MGR_H_INCLUDED_

#include "libsyslog.h"

#ifdef DISABLE_SYSLOG_SYSTEM_VRRP_MGR
	#define IF_SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...)
#else
	#define IF_SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...) \
		SYSLOG_## LEVEL(__system_type__, __subsystem_type__, __format__, ## __VA_ARGS__)
#endif

#define SYSLOG_VRRP_MGR_CMD(LEVEL, __format__, ...) \
	IF_SYSLOG_USE_LEVEL(LEVEL, VRRP_MGR, CMD, __format__, ## __VA_ARGS__)

#define SYSLOG_VRRP_MGR_GENERAL(LEVEL, __format__, ...) \
	IF_SYSLOG_USE_LEVEL(LEVEL, VRRP_MGR, GENERAL, __format__, ## __VA_ARGS__)

#define SYSLOG_VRRP_MGR_IPC(LEVEL, __format__, ...) \
	IF_SYSLOG_USE_LEVEL(LEVEL, VRRP_MGR, IPC, __format__, ## __VA_ARGS__)

#define SYSLOG_VRRP_MGR_IPC_DTL(LEVEL, __format__, ...) \
	IF_SYSLOG_USE_LEVEL(LEVEL, VRRP_MGR, IPC_DTL, __format__, ## __VA_ARGS__)

#define SYSLOG_VRRP_MGR_DB(LEVEL, __format__, ...) \
	IF_SYSLOG_USE_LEVEL(LEVEL, VRRP_MGR, DB, __format__, ## __VA_ARGS__)

#define SYSLOG_VRRP_MGR_SYNC_GROUP(LEVEL, __format__, ...) \
	IF_SYSLOG_USE_LEVEL(LEVEL, VRRP_MGR, SYNC, __format__, ## __VA_ARGS__)

#define SYSLOG_VRRP_MGR_SOCKET(LEVEL, __format__, ...) \
	IF_SYSLOG_USE_LEVEL(LEVEL, VRRP_MGR, SOCKET, __format__, ## __VA_ARGS__)

#define SYSLOG_VRRP_MGR_INTERFACE(LEVEL, __format__, ...) \
	IF_SYSLOG_USE_LEVEL(LEVEL, VRRP_MGR, INTERFACE, __format__, ## __VA_ARGS__)

#define SYSLOG_VRRP_MGR_SCHEDULER(LEVEL, __format__, ...) \
	IF_SYSLOG_USE_LEVEL(LEVEL, VRRP_MGR, SCHEDULER, __format__, ## __VA_ARGS__)

#define SYSLOG_VRRP_MGR_VRRP(LEVEL, __format__, ...) \
	IF_SYSLOG_USE_LEVEL(LEVEL, VRRP_MGR, VRRP, __format__, ## __VA_ARGS__)

#define SYSLOG_VRRP_MGR_UTILS(LEVEL, __format__, ...) \
	IF_SYSLOG_USE_LEVEL(LEVEL, VRRP_MGR, UTILS, __format__, ## __VA_ARGS__)

#define SYSLOG_VRRP_MGR_SVCMON(LEVEL, __format__, ...) \
	IF_SYSLOG_USE_LEVEL(LEVEL, VRRP_MGR, SVCMON, __format__, ## __VA_ARGS__)

#endif /* _LIBSYSLOG_VRRP_MGR_H_INCLUDED_ */
