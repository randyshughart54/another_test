#ifndef _LIBSYSLOG_BFD_H_INCLUDED_
#define _LIBSYSLOG_BFD_H_INCLUDED_

#include "libsyslog.h"

#ifdef DISABLE_SYSLOG_SYSTEM_BFD
	#define SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...)
#else
	#define SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...) \
		SYSLOG_## LEVEL(__system_type__, __subsystem_type__, __format__, ## __VA_ARGS__)
#endif

#define SYSLOG_BFD_COMMON(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, BFD, COMMON, __format__, ## __VA_ARGS__)
#define SYSLOG_BFD_ADJCHANGE(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, BFD, ADJCHANGE, __format__, ## __VA_ARGS__)
#define SYSLOG_BFD_ADJTRACK(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, BFD, ADJTRACK, __format__, ## __VA_ARGS__)

#endif //_LIBSYSLOG_BFD_H_INCLUDED_
