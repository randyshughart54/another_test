#ifndef _LIBSYSLOG_MSDP_H_INCLUDED_
#define _LIBSYSLOG_MSDP_H_INCLUDED_

#include "libsyslog.h"

#ifdef DISABLE_SYSLOG_SYSTEM_MSDP
	#define SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...)
#else
	#define SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...) \
		SYSLOG_## LEVEL(__system_type__, __subsystem_type__, __format__, ## __VA_ARGS__)
#endif

#define SYSLOG_MSDP_COMMON(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, MSDP, COMMON, __format__, ## __VA_ARGS__)

#define SYSLOG_MSDP_PF(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, MSDP, PF, __format__, ## __VA_ARGS__)

#define SYSLOG_MSDP_AI3(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, MSDP, AI3, __format__, ## __VA_ARGS__)

#endif //_LIBSYSLOG_MSDP_H_INCLUDED_
