#ifndef _LIBSYSLOG_ALARM_H_INCLUDED_
#define _LIBSYSLOG_ALARM_H_INCLUDED_

#include "libsyslog.h"

#ifdef DISABLE_SYSLOG_SYSTEM_ALARM_MGR
	#define ALARM_SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...)
#else
	#define ALARM_SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...) \
		SYSLOG_## LEVEL(__system_type__, __subsystem_type__, __format__, ## __VA_ARGS__)
#endif

#define SYSLOG_ALARM_MGR_COMMON(LEVEL, __format__, ...) \
	ALARM_SYSLOG_USE_LEVEL(LEVEL, ALARM_MGR, COMMON, __format__, ## __VA_ARGS__)

#define SYSLOG_ALARM_MGR_IPC(LEVEL, __format__, ...) \
	ALARM_SYSLOG_USE_LEVEL(LEVEL, ALARM_MGR, IPC, __format__, ## __VA_ARGS__)

#define SYSLOG_ALARM_MGR_IPC_DTL(LEVEL, __format__, ...) \
	ALARM_SYSLOG_USE_LEVEL(LEVEL, ALARM_MGR, IPC_DTL, __format__, ## __VA_ARGS__)

#define SYSLOG_ALARM_MGR_THREAD_ALIVE(LEVEL, __format__, ...) \
	ALARM_SYSLOG_USE_LEVEL(LEVEL, ALARM_MGR, THREAD_ALIVE, __format__, ## __VA_ARGS__)

#define SYSLOG_ALARM_MGR_SVCMON(LEVEL, __format__, ...) \
	ALARM_SYSLOG_USE_LEVEL(LEVEL, ALARM_MGR, SVCMON, __format__, ## __VA_ARGS__)

#endif //_LIBSYSLOG_ALARM_H_INCLUDED_
