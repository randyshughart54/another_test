#ifndef _LIBSYSLOG_KLISH_H_INCLUDED_
#define _LIBSYSLOG_KLISH_H_INCLUDED_

#include "libsyslog.h"

#ifdef DISABLE_SYSLOG_SYSTEM_KLISH
	#define SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...)
#else
	#define SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...) \
		SYSLOG_## LEVEL(__system_type__, __subsystem_type__, __format__, ## __VA_ARGS__)
#endif

#define SYSLOG_KLISH_COMMON(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, KLISH, COMMON, __format__, ## __VA_ARGS__)

#define SYSLOG_KLISH_COMMAND(LEVEL, __format__, ...) \
	if (SYSLOG_get_cli_command_flag()) SYSLOG_USE_LEVEL(LEVEL, KLISH, COMMAND, __format__, ## __VA_ARGS__)

#endif
