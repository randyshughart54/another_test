#ifndef _LIBSYSLOG_TOP_H_INCLUDED_
#define _LIBSYSLOG_TOP_H_INCLUDED_

#include "libsyslog.h"

#ifdef DISABLE_SYSLOG_SYSTEM_TOP
	#define TOP_SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...) \
		do { } while(0)
#else
	#define TOP_SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...) \
		do { \
		SYSLOG_## LEVEL(__system_type__, __subsystem_type__, __format__, ## __VA_ARGS__); \
		} while(0)
#endif

#ifdef DISABLE_SYSLOG_SYSTEM_TOP
	#define SYSLOG_TOP_EXEC(LEVEL, __format__, ...)

	//#define SYSLOG_TOP_SYS(LEVEL, __format__, ...)

	#define SYSLOG_TOP_MON(LEVEL, __format__, ...)

	#define SYSLOG_TOP_MON_DTL(LEVEL, __format__, ...)

	#define SYSLOG_TOP_REBOOT(LEVEL, __format__, ...)

	#define SYSLOG_TOP_IPCPING(LEVEL, __format__, ...)
#else
	#define SYSLOG_TOP_EXEC(LEVEL, __format__, ...) \
		TOP_SYSLOG_USE_LEVEL(LEVEL, TOP_MGR, EXEC, __format__, ## __VA_ARGS__)

	/*#define SYSLOG_TOP_SYS(LEVEL, __format__, ...) \
		TOP_SYSLOG_USE_LEVEL(LEVEL, TOP_MGR, SYS, __format__, ## __VA_ARGS__)*/

	#define SYSLOG_TOP_MON(LEVEL, __format__, ...) \
		TOP_SYSLOG_USE_LEVEL(LEVEL, TOP_MGR, MON, __format__, ## __VA_ARGS__)

	#define SYSLOG_TOP_MON_DTL(LEVEL, __format__, ...) \
		TOP_SYSLOG_USE_LEVEL(LEVEL, TOP_MGR, MON_DTL, __format__, ## __VA_ARGS__)

	#define SYSLOG_TOP_REBOOT(LEVEL, __format__, ...) \
		TOP_SYSLOG_USE_LEVEL(LEVEL, TOP_MGR, REBOOT, __format__, ## __VA_ARGS__)

	#define SYSLOG_TOP_IPCPING(LEVEL, __format__, ...) \
		TOP_SYSLOG_USE_LEVEL(LEVEL, TOP_MGR, IPCPING, __format__, ## __VA_ARGS__)

	#define SYSLOG_TOP_IPC(LEVEL, __format__, ...) \
		TOP_SYSLOG_USE_LEVEL(LEVEL, TOP_MGR, IPC, __format__, ## __VA_ARGS__)

	#define SYSLOG_TOP_IPC_DTL(LEVEL, __format__, ...) \
		TOP_SYSLOG_USE_LEVEL(LEVEL, TOP_MGR, IPC_DTL, __format__, ## __VA_ARGS__)

	#define SYSLOG_TOP_THREAD_ALIVE(LEVEL, __format__, ...) \
		TOP_SYSLOG_USE_LEVEL(LEVEL, TOP_MGR, THREAD_ALIVE, __format__, ## __VA_ARGS__)

	#define SYSLOG_TOP_SVCMON(LEVEL, __format__, ...) \
		TOP_SYSLOG_USE_LEVEL(LEVEL, TOP_MGR, SVCMON, __format__, ## __VA_ARGS__)
#endif

#endif /* _LIBSYSLOG_TOP_H_INCLUDED_ */
