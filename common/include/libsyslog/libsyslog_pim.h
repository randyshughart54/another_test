#ifndef _LIBSYSLOG_PIM_H_INCLUDED_
#define _LIBSYSLOG_PIM_H_INCLUDED_

#include "libsyslog.h"

#ifdef DISABLE_SYSLOG_SYSTEM_PIM
	#define SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...)
#else
	#define SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...) \
		SYSLOG_## LEVEL(__system_type__, __subsystem_type__, __format__, ## __VA_ARGS__)
#endif

#define SYSLOG_PIM_NM(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, PIM, NM, __format__, ## __VA_ARGS__)

#define SYSLOG_PIM_TM(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, PIM, TM, __format__, ## __VA_ARGS__)

#define SYSLOG_PIM_ML(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, PIM, ML, __format__, ## __VA_ARGS__)

#define SYSLOG_PIM_GM(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, PIM, GM, __format__, ## __VA_ARGS__)

#endif //_LIBSYSLOG_PIM_H_INCLUDED_