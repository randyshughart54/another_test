#ifndef _LIBSYSLOG_EXCH_H_INCLUDED_
#define _LIBSYSLOG_EXCH_H_INCLUDED_

#include "libsyslog.h"

#ifdef DISABLE_SYSLOG_SYSTEM_EXCH
	#define SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...)
#else
	#define SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...) \
		SYSLOG_## LEVEL(__system_type__, __subsystem_type__, __format__, ## __VA_ARGS__)
#endif

#define SYSLOG_EXCH_COMMON(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, EXCH, COMMON, __format__, ## __VA_ARGS__)

#define SYSLOG_EXCH_SVCMON(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, EXCH, SVCMON, __format__, ## __VA_ARGS__)

#endif
