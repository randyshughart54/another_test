#ifndef _LIBSYSLOG_LACP_H_INCLUDED_
#define _LIBSYSLOG_LACP_H_INCLUDED_

#include "libsyslog.h"

#ifdef DISABLE_SYSLOG_SYSTEM_LACP
	#define SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...)
#else
	#define SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...) \
		SYSLOG_## LEVEL(__system_type__, __subsystem_type__, __format__, ## __VA_ARGS__)
#endif

#define SYSLOG_LACP_NM(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, LACP, NM, __format__, ## __VA_ARGS__)

#define SYSLOG_LACP_LM(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, LACP, LM, __format__, ## __VA_ARGS__)

#define SYSLOG_LACP_COMMON(LEVEL, __format__, ...) \
	SYSLOG_USE_LEVEL(LEVEL, LACP, COMMON, __format__, ## __VA_ARGS__)

#endif //_LIBSYSLOG_LACP_H_INCLUDED_