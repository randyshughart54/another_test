#ifndef _LIBSYSLOG_SERVICE_MGR_H_INCLUDED_
#define _LIBSYSLOG_SERVICE_MGR_H_INCLUDED_

#include "libsyslog.h"

#ifdef DISABLE_SYSLOG_SYSTEM_SERVICE_MGR
	#define SERVICE_MGR_SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...) \
		do { } while(0)
#else
	#define SERVICE_MGR_SYSLOG_USE_LEVEL(LEVEL, __system_type__, __subsystem_type__, __format__, ...) \
		do { \
		SYSLOG_## LEVEL(__system_type__, __subsystem_type__, __format__, ## __VA_ARGS__); \
		} while(0)
#endif


#define SYSLOG_SERVICE_MGR_COMMON(LEVEL, __format__, ...) \
	SERVICE_MGR_SYSLOG_USE_LEVEL(LEVEL, SERVICE_MGR, COMMON, __format__, ## __VA_ARGS__)

#define SYSLOG_SERVICE_MGR_NETWORK(LEVEL, __format__, ...) \
	SERVICE_MGR_SYSLOG_USE_LEVEL(LEVEL, SERVICE_MGR, NETWORK, __format__, ## __VA_ARGS__)

#define SYSLOG_SERVICE_MGR_SIGNALS(LEVEL, __format__, ...) \
	SERVICE_MGR_SYSLOG_USE_LEVEL(LEVEL, SERVICE_MGR, SIGNALS, __format__, ## __VA_ARGS__)

#define SYSLOG_SERVICE_MGR_REGPROCESS(LEVEL, __format__, ...) \
	SERVICE_MGR_SYSLOG_USE_LEVEL(LEVEL, SERVICE_MGR, REGPROCESS, __format__, ## __VA_ARGS__)

#define SYSLOG_SERVICE_MGR_UNREGPROCESS(LEVEL, __format__, ...) \
	SERVICE_MGR_SYSLOG_USE_LEVEL(LEVEL, SERVICE_MGR, UNREGPROCESS, __format__, ## __VA_ARGS__)

#define SYSLOG_SERVICE_MGR_UPDCFG(LEVEL, __format__, ...) \
	SERVICE_MGR_SYSLOG_USE_LEVEL(LEVEL, SERVICE_MGR, UPDCFG, __format__, ## __VA_ARGS__)

#define SYSLOG_SERVICE_MGR_GETINFO(LEVEL, __format__, ...) \
	SERVICE_MGR_SYSLOG_USE_LEVEL(LEVEL, SERVICE_MGR, GETINFO, __format__, ## __VA_ARGS__)

#define SYSLOG_SERVICE_MGR_IPC(LEVEL, __format__, ...) \
	SERVICE_MGR_SYSLOG_USE_LEVEL(LEVEL, SERVICE_MGR, IPC, __format__, ## __VA_ARGS__)

#define SYSLOG_SERVICE_MGR_IPC_DTL(LEVEL, __format__, ...) \
	SERVICE_MGR_SYSLOG_USE_LEVEL(LEVEL, SERVICE_MGR, IPC_DTL, __format__, ## __VA_ARGS__)

#define SYSLOG_SERVICE_MGR_THREAD_ALIVE(LEVEL, __format__, ...) \
	SERVICE_MGR_SYSLOG_USE_LEVEL(LEVEL, SERVICE_MGR, THREAD_ALIVE, __format__, ## __VA_ARGS__)

#define SYSLOG_SERVICE_MGR_SVCMON(LEVEL, __format__, ...) \
	SERVICE_MGR_SYSLOG_USE_LEVEL(LEVEL, SERVICE_MGR, SVCMON, __format__, ## __VA_ARGS__)

#endif
