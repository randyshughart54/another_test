#ifndef _SYSTEM_SM_TYPES_H_INCLUDED_
#define _SYSTEM_SM_TYPES_H_INCLUDED_

typedef struct cpu_utilization
{
	int cpu_id;
	int five_sec_index;
	double five_sec_smpls[FIVE_SEC_OBJ_CNT]; // Cpu utilization in % during 5 sec observation time
	double five_sec_max[FIVE_SEC_OBJ_CNT];   // Cpu max utilization in % during 5 sec observation time
	int one_min_index;
	double one_min_smpls[ONE_MIN_OBJ_CNT];   // Cpu utilization in % during 1 min observation time
	double one_min_max[ONE_MIN_OBJ_CNT];     // Cpu max utilization in % during 1 min observation time
	double five_min_smpl;                    // Cpu utilization in % during 5 min observation time
	int one_hour_index;
	double one_hour_smpls[ONE_HOUR_OBJ_CNT]; // Cpu utilization in % during 1 hour observation time
	double one_hour_max[ONE_HOUR_OBJ_CNT];   // Cpu max utilization in % during 1 hour observation time
} cpu_utilization_t;

typedef struct cpus_utilization
{
	cpu_utilization_t cpus[CPUS_COUNT];
} cpus_utilization_t;

#endif
