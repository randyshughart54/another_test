#ifndef __RUNTIME_ENV_CFG_TYPES_H_INCLUDED__
#define __RUNTIME_ENV_CFG_TYPES_H_INCLUDED__

#include <sys/types.h>

typedef struct
{
	char port_name[48];
} port_name_t;

enum
{
	NOTIF_TYPE_COMMON_STRING = 0,
	NOTIF_TYPE_FIRMWARE_INSTALLATION_STEP,
	NOTIF_TYPE_FIRMWARE_INSTALLATION_RESULT,
	NOTIF_TYPE_COMMON_WARN_STRING,
};

typedef struct
{
	char string[1024];
	int notif_type;
	uint8_t is_multicast_notif;
	uint8_t is_need_enable_notif;
} notification_t;

#endif
