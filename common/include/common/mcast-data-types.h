#ifndef _MCAST_DATA_TYPES_H_
#define _MCAST_DATA_TYPES_H_

#include <stdint.h>
#include <libnetwork/network_defs_ip.h>

#define MAX_INET_ADDR_LEN 32

typedef struct mpf_sg_pair
{
  network_ip_prefix_t source_addr;
  network_ip_prefix_t group_addr;
  uint8_t             is_ipv4;
} mpf_sg_pair_t;

typedef enum mpf_rsp_type
{
  MPF_RC_UNSUCCESSFUL = 0,
  MPF_RC_OK,
  MPF_RC_NOT_FOUND,
} mpf_rsp_type_e;

typedef struct mpf_record_ent
{
  mpf_sg_pair_t sg_pair;
  uint8_t source_is_local;
  uint8_t data_liveness_timer;
  uint8_t msg_from_stub;
  uint8_t join_spt_request_allowed;
  uint8_t oif_is_tunnel;
  uint8_t use_mc_group_directly;
  mpf_rsp_type_e return_code;       //is filled for response
  int32_t mc_group;                 //will be filled on FMC
  int32_t incoming_if;
  int32_t notification_if;
  uint32_t vrf_idx;
  uint32_t oifs_count;
  int32_t outgoing_if[0];
} mpf_record_ent_t;

typedef struct mpf_record_ent_batch
{
  int32_t entries_count;
  mpf_record_ent_t entry[0];
} mpf_record_ent_batch_t;

typedef struct mpf_delete_one_ent
{
  mpf_sg_pair_t sg_pair;
  mpf_rsp_type_e return_code;
} mpf_delete_one_ent_t;

typedef struct mpf_delete_ent
{
  int32_t pair_count;
  mpf_delete_one_ent_t sg_pair[0];
} mpf_delete_ent_t;

typedef struct mpf_register_ent
{
  uint32_t if_index;
} mpf_register_ent_t;

typedef struct _mpf_data_start
{
  mpf_sg_pair_t sg_pair;
  int32_t incoming_if;
  uint8_t ttl;
  uint8_t is_src_connected;
} mpf_data_start_t;

typedef struct _mpf_act_src_rpl
{
  int pair_count;
  mpf_data_start_t act_src[0];
} mpf_act_src_rpl_s;

typedef struct _mpf_data_stop
{
  mpf_sg_pair_t sg_pair;
} mpf_data_stop_t;

typedef struct _mpf_wrong_if_alert
{
  mpf_sg_pair_t sg_pair;
  int32_t incoming_if;
} mpf_wrong_if_alert_t;

typedef struct _mpf_join_spt
{
  mpf_sg_pair_t sg_pair;
} mpf_join_spt_t;

typedef struct _mpf_reset_notif
{
  mpf_sg_pair_t sg_pair;
} mpf_reset_notif_t;

typedef struct _mpf_config_t
{
  uint8_t  is_ipv4;
  uint32_t vrf_idx;
  uint32_t qmpf_addr;
} mpf_config_t;

#endif /* _MCAST_DATA_TYPES_H_ */
