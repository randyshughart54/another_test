#ifndef __FW_TYPES_H__
#define __FW_TYPES_H__

#include <common/shelf-types.h>
#include <common/vrf.h>

#include <stdint.h>

typedef enum
{
	FW_STATUS_INVALID = 1,
	FW_STATUS_TIMEOUT, //TODO not used
	FW_STATUS_CURRENT,
	FW_STATUS_DOWNLOADED,
	FW_STATUS_TESTING,
} libfw_status_t; //TODO rename to fw_status_t
//TODO state as separate type, see sector_state in fw_image.h

#define FW_MAX_VERSION_LEN 128

typedef struct
{
	uint8_t is_valid;
	uint32_t timestamp;
	char version_string[FW_MAX_VERSION_LEN];
} libfw_version_t; //TODO rename to fw_version_t

// see also MAX_VERSIONS:
#define FW_MAX_PARTITIONS 2

#define CURRENT_PARTITION 0
#define ALTERNATE_PARTITION 1

typedef struct
{
	libfw_status_t fw_status;
	/* FW_STATUS_INVALID, FW_STATUS_TIMEOUT: all the fields below
	   are undefined.
	   (FIXME: better pass STATE_CURRENT/STATE_DOWNLOADED/STATE_TESTING
	   as is, in a separate field! */

	uint32_t active_partition;
	/* The version that will be booted the next time
	   (0..FW_MAX_PARTITIONS); any value outside this range
	   should be treated as "not active". */

	uint32_t fallback_partition;
	/* If fw_status is one of FW_STATUS_DOWNLOADED or FW_STATUS_TESTING:
	   the version that will be selected as active
	   if there is no confirmation from the operator. */

	libfw_version_t versions[FW_MAX_PARTITIONS];

	uint32_t running_partition;
	/* The partition that the firmware is currently running from. */

	uint32_t number_of_units;
} fw_unit_status_t; //TODO rename to fw_unit_bootinfo_t

typedef struct
{
	fw_unit_status_t unit_status[APP_MAX_UNITS_PER_STACK];
} fw_stack_status_t; //TODO rename to fw_stack_bootinfo_t

typedef struct
{
	uint8_t need_confirm;
	uint32_t confirm_full_timeout;
	uint32_t confirm_remain_timeout;
} fw_confirm_status_t;

#define FW_FLD_UNIT		9
#define FW_FLD_IMAGE	8
#define FW_FLD_RUNNING	8
#define FW_FLD_BOOT		12
#define FW_FLD_VERSION	20
#define FW_FLD_DATE		22
#define FW_FLD_BOOT_VERSION	60

typedef struct
{
	char unit[FW_FLD_UNIT];
	char image[FW_FLD_IMAGE];
	char running[FW_FLD_RUNNING];
	char boot[FW_FLD_BOOT];
	char version[FW_FLD_VERSION];
	char date[FW_FLD_DATE];
} fw_show_firmware_row_t;

typedef struct
{
	fw_show_firmware_row_t row[APP_MAX_UNITS_PER_STACK][FW_MAX_PARTITIONS];
} fw_show_firmware_t;

typedef struct
{
	char version[FW_FLD_VERSION];
	char date[FW_FLD_DATE];
} fw_show_version_t;

typedef struct
{
	char u_boot_version[FW_FLD_BOOT_VERSION];
	char x_loader_version[FW_FLD_BOOT_VERSION];
	char fpga_version[FW_FLD_BOOT_VERSION];
} fw_show_boot_version_t;

#define PASSWORD_LEN 132

typedef struct
{
	char vrf[VRF_NAME_LEN];
	char password[PASSWORD_LEN];
	size_t url_len;
	char src_url[0];
} fw_install_t;

typedef struct
{
	char hostname[64];
	char rsync_module[64];
	char firmware_name[64];
} fw_install_slave_t;

#define FW_UBOOT 	"u-boot"
#define FW_XLOADER	"x-loader"

typedef struct
{
	char bootloader_name[16];
	char vrf[VRF_NAME_LEN];
	char password[PASSWORD_LEN];
	uint8_t bootloader_for_linecard;
	size_t url_len;
	char src_url[0];
} fw_bootloader_update_t;

typedef struct
{
	char hostname[64];
	char rsync_module[64];
	char bootloader_name[16];
} fw_bootloader_update_neighbours_t;

typedef struct
{
	char vrf[VRF_NAME_LEN];
	char password[PASSWORD_LEN];
	uint8_t fpga_for_linecard;
	size_t url_len;
	char src_url[0];
} fw_fpga_update_t;

typedef struct
{
	char hostname[64];
	char rsync_module[64];
} fw_fpga_update_neighbours_t;

/*
   For all target units, the following is checked:
   - firmware on the unit must be in CURRENT state;
   - firmware located in target partition must be valid;
   - the version of firmware in target partition must be the same on all target units.
   If any of the checks fails, the command fails.
   If any operation fails on any of the target units,
   the boot sector status is reverted on all target units, and the command fails.

   If FW_SELECT_FLAG_ALTERNATE flag is set,
   target partition parameter is ignored,
   and currently inactive partition is selected.

   If FW_SELECT_FLAG_NEEDCONFIRM flag is set,
   the boot sector status is set to DOWNLOADED.
   If FW_SELECT_FLAG_NEEDCONFIRM is not set,
   the boot sector status is set to CURRENT.

   If FW_SELECT_FLAG_OVERDOWN flag is set,
   the requirement that the boot sector must be in CURRENT state
   is relaxed (DOWNLOADED state is allowed, too).
   This is useful for canceling a software upgrade.

   If we're trying to select a partition that is already active,
   operation fails, unless FW_SELECT_FLAG_FORCEUPDATE flag is set.
   This is useful for switching from "need-confirm" to "no-confirm" mode
   or vice versa.
*/

enum {
	FW_SELECT_FLAG_ALTERNATE   = 0x1,
	FW_SELECT_FLAG_NEEDCONFIRM = 0x2,
	//FW_SELECT_FLAG_NSSU        = 0x4,
	FW_SELECT_FLAG_CURRENT     = 0x8,
	FW_SELECT_FLAG_OVERDOWN    = 0x10,
	FW_SELECT_FLAG_FORCEUPDATE = 0x20,
};

typedef struct
{
	uint8_t unit_id;
	uint8_t partition_num;
	uint32_t flags;
	uint32_t timeout;
} fw_select_t;

/*typedef struct
{
	uint8_t unit_id;
	//BOOL all_units;
} fw_confirm_t;*/

enum
{
	FW_RETCODE_ERROR_UNKNOWN = -1,
	FW_RETCODE_SUCCESS = 0,
	//
	// (0..255]: exit statuses of TFTP task process
	// [256..258]: exit statuses urlcopy
	//
	FW_RETCODE_ERROR_COPY_BAD_HWTYPE = 259,
	FW_RETCODE_ERROR_IMAGE_FILE_CHECK_HEADER = 260,
	FW_RETCODE_ERROR_FIRMWARE_UPDATE_SLAVE = 261,
	FW_RETCODE_ERROR_FIRMWARE_INVALID_PRODUCT_HDR_FILE = 262,
	FW_RETCODE_ERROR_INVALID_SECTOR_STATE = 263,
	FW_RETCODE_ERROR_ALL_UNITS_OFFLINE = 264,
	FW_RETCODE_ERROR_LCS_NOT_PRESENT = 265,
	FW_RETCODE_ERROR_SELECT_FROM_WRONG_STATE = 266
};

typedef struct
{
	int rcode;
} fwmgr_result_t;

#endif /* __FW_TYPES_H__ */
