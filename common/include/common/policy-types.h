#ifndef __COMMON_POLICY_TYPES_H_INCLUDED__
#define __COMMON_POLICY_TYPES_H_INCLUDED__

/*
 * This file defines types related to traffic classification and QoS.
 */

#include <stdint.h>
#include <sys/types.h>

#define CMAP_NAME_LEN           64
#define IF_SVC_PLC_NAME_LEN     64
#define IF_ACCESS_GRP_NAME_LEN  64

#define CMAP_NAME_CLASS_DEFAULT "class-default"

typedef enum
{
	QOS_BW_UNITS_INVALID = 0,
	QOS_BW_UNITS_PERCENT,
	QOS_BW_UNITS_KBPS,
} qos_bw_units_t;

static inline const char *qos_bw_units_stringize(qos_bw_units_t units)
{
	switch (units)
	{
		case QOS_BW_UNITS_PERCENT:     return "percent";
		case QOS_BW_UNITS_KBPS:        return "kbps";
		default:                       return "???";
	}
}

typedef enum
{
	CMAP_MATCH_MODE_INVALID = 0,
	CMAP_MATCH_MODE_ALL = 1,
	CMAP_MATCH_MODE_ANY = 2,
} cmap_match_mode_t;

static inline const char *cmap_match_mode_stringize(cmap_match_mode_t match_mode)
{
	switch (match_mode)
	{
		case CMAP_MATCH_MODE_ALL: return "match-all";
		case CMAP_MATCH_MODE_ANY: return "match-any";
		default:                  return "match-???";
	}
}

typedef enum
{
	CMAP_MATCH_TYPE_INVALID = 0,
	CMAP_MATCH_TYPE_TC = 1,
} cmap_match_type_t;

static inline const char *cmap_match_type_stringize(cmap_match_type_t match_type)
{
	switch (match_type)
	{
		case CMAP_MATCH_TYPE_TC:  return "tc";
		default:                  return "???";
	}
}

typedef struct
{
	uint8_t match_type;
	uint8_t tc;
} cmap_match_t;

typedef struct
{
	char name[CMAP_NAME_LEN];
	uint8_t match_mode;

	uint32_t num_matches;
	cmap_match_t matches[0];
} cmap_t;

static inline size_t cmap_sizeof_matches(const cmap_t *cmap)
{
	return sizeof(cmap_match_t) * cmap->num_matches;
}

typedef enum
{
	PP_QOS_SHAPE_TYPE_INVALID,
	PP_QOS_SHAPE_TYPE_DIRECT,
	PP_QOS_SHAPE_TYPE_PROFILE,
} pp_qos_shape_type;

typedef enum
{
	PP_QOS_RATE_LIMIT_TYPE_INVALID,
	PP_QOS_RATE_LIMIT_TYPE_DIRECT,
	PP_QOS_RATE_LIMIT_TYPE_PROFILE,
} pp_qos_rate_limit_type;

#define SHAPE_PROFILE_NAME_LEN          64
#define RATE_LIMIT_PROFILE_NAME_LEN     64

typedef struct _pp_shape_config
{
	pp_qos_shape_type type;
	union
	{
		struct
		{
			uint32_t shaper_kbps; /* If 0, shaper is disabled */
			uint32_t burst_size;
		};

		char shape_profile[SHAPE_PROFILE_NAME_LEN];
	};
} pp_shape_config_t;

typedef struct _pp_rate_limit_config
{
	pp_qos_rate_limit_type type;
	union
	{
		uint32_t rate;
		char rate_limit_profile[RATE_LIMIT_PROFILE_NAME_LEN];
	};
} pp_rate_limit_config_t;

typedef struct
{
	char cmap_name[CMAP_NAME_LEN];

	uint8_t  set_min_bw;
	uint8_t  min_bw_units;
	uint32_t min_bw;
	pp_shape_config_t shape_config;
	uint8_t  is_prioritized;
	uint32_t queue_size;
	int pcp;
	int cfi;
} pmap_class_t;

typedef struct
{
	char name[IF_SVC_PLC_NAME_LEN];
	uint32_t num_classes;
	pmap_class_t classes[0];
} pmap_t;

static inline size_t pmap_sizeof_classes(const pmap_t *pmap)
{
	return sizeof(pmap_class_t) * pmap->num_classes;
}

#endif /*  __COMMON_POLICY_TYPES_H_INCLUDED__ */
