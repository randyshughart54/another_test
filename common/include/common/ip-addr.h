#ifndef __COMMON_IP_ADDR_H_INCLUDED__
#define __COMMON_IP_ADDR_H_INCLUDED__

#include <libnetwork/network_defs_ip.h>

#define DOMAIN_NAME_LEN 256 /* rfc 1035: 255 + '\0' */

#if DOMAIN_NAME_LEN < INET6_ADDRSTRLEN
    #error "DOMAIN_NAME_LEN must be >= INET6_ADDRSTRLEN"
#endif

#define ND_RA_REACH_TIME_MIN 0        // milliseconds
#define ND_RA_REACH_TIME_MAX 3600000  // milliseconds

#define IP_ADDR_FLAG_OWNER_IS_LO 0x1
#define IP_ADDR_OWNER_IS_LO(addr) ((addr)->flags & IP_ADDR_FLAG_OWNER_IS_LO)
#define IP_ADDR_FLAG_IS_LL 0x2
#define IP_ADDR_OWNER_IS_LL(addr) ((addr)->flags & IP_ADDR_FLAG_IS_LL)
#define IP_ADDR_FLAG_IS_CFG_LL 0x4
#define IP_ADDR_OWNER_IS_CFG_LL(addr) ((addr)->flags & IP_ADDR_FLAG_IS_CFG_LL)
#define IP_ADDR_FLAG_OWNER_IS_IP_TUNNEL 0x80
#define IP_ADDR_OWNER_IS_IP_TUNNEL(addr) ((addr)->flags & IP_ADDR_FLAG_OWNER_IS_IP_TUNNEL)

// Because first two bits taken (see above)
#define IP_ADDR_FLAG_VRRP_BIT                 3 // This ip-address is vrrp
#define IP_ADDR_VRRP_BIT_SET(addr)            ((addr)->flags |= (1 << IP_ADDR_FLAG_VRRP_BIT))
#define IS_IP_ADDR_VRRP_BIT_SET(addr)         ((addr)->flags &  (1 << IP_ADDR_FLAG_VRRP_BIT))
#define IS_IP_ADDR_FLAGS_VRRP_BIT_SET(flags)  (flags         &  (1 << IP_ADDR_FLAG_VRRP_BIT))

#define IP_ADDR_FLAG_VRRP_MASTER_BIT          4 // This ip-address is vrrp-master
#define IP_ADDR_VRRP_MASTER_BIT_SET(addr)     ((addr)->flags |= (1 << IP_ADDR_FLAG_VRRP_MASTER_BIT))
#define IS_IP_ADDR_VRRP_MASTER_BIT_SET(addr)  ((addr)->flags &  (1 << IP_ADDR_FLAG_VRRP_MASTER_BIT))

#define IP_ADDR_FLAG_VRRP_ACCEPT_BIT          5 // This ip-address in vrrp accept mode (accoring rfc)
#define IP_ADDR_VRRP_ACCEPT_BIT_SET(addr)     ((addr)->flags |= (1 << IP_ADDR_FLAG_VRRP_ACCEPT_BIT))
#define IS_IP_ADDR_VRRP_ACCEPT_BIT_SET(addr)  ((addr)->flags &  (1 << IP_ADDR_FLAG_VRRP_ACCEPT_BIT))

#define IP_ADDR_FLAG_COMMON_USAGE_BIT         6 // This ip-address has common usage with his parent or virtual
#define IP_ADDR_COMMON_USAGE_BIT_SET(addr)    ((addr)->flags |= (1 << IP_ADDR_FLAG_COMMON_USAGE_BIT))
#define IP_ADDR_COMMON_USAGE_BIT_CLEAR(addr)  ((addr)->flags &= ~(1 << IP_ADDR_FLAG_COMMON_USAGE_BIT))
#define IS_IP_ADDR_COMMON_USAGE_BIT_SET(addr) ((addr)->flags &  (1 << IP_ADDR_FLAG_COMMON_USAGE_BIT))


typedef enum _rpf_check_type {
	RPF_CHECK_TYPE_LOOSE,
	RPF_CHECK_TYPE_STRICT
} rpf_check_type_e;

typedef struct _addr_rpf {
	rpf_check_type_e check_type;
	uint8_t allow_default;
} addr_rpf_t;

typedef struct _addr_ra {
	uint8_t supress_en;
	uint32_t reach_time;
} ip6_addr_nd_ra_t;

typedef struct _addr {
	network_ip_prefix_t pfx;
	uint8_t icmp_unreach_en;
	addr_rpf_t rpf;
	uint32_t mtu;
	uint32_t owner_iface_idx;
	uint32_t owner_vrf_idx;
	uint8_t flags;

	/*
	 * Only valid for IPv6 addrs
	 */
	uint32_t ip6_flow_info;
	uint32_t ip6_scope_id;
	ip6_addr_nd_ra_t ip6_nd_ra;
} addr_t;

#endif
