#ifndef _SYSTEM_PP_TYPES_H_INCLUDED_
#define _SYSTEM_PP_TYPES_H_INCLUDED_

#include <stdint.h>
#include <linux/if_ether.h>
#include <common/ip-addr.h>
#include <common/if-types.h>
#include <common/shelf-types.h>
#include <libnetwork/network_defs_mac.h>
#include <libnetwork/network_defs_ip.h>

#define LDP_ID_FEC_MAX_AII 255
#define LDP_ID_FEC_MAX_AGI 255
#define BGP_ID_VPN_ROUTE_DIST_LEN 8
#define LDP_ID_LENGTH 6

/*
 * Identifies a BOARD_BPE_PIZZABOX or a BOARD_BPE_LINECARD device location.
 *
 * Cannot identify a BOARD_BPE_CTRLCARD (e.g., FMC16 slot in ME5000 shelf),
 * because FMC slots are numbered separately from LC slots.
 */

#define MAX_PORTS_PER_BOARD 40
#define MPLS_MAX_ACTIVE_TUNNELS 8

typedef struct
{
	int unit;
	int slot;
} pp_location_t;

typedef enum _bd_type {
	BD_TYPE_NONE,
	BD_TYPE_VLAN,
	BD_TYPE_VPWS,
	BD_TYPE_VPLS
} bd_type_e;

typedef enum _bd_mcast_flood_type {
	BD_MCAST_FLOOD_TYPE_ALL = 1,
	BD_MCAST_FLOOD_TYPE_UNKNOWN,
	BD_MCAST_FLOOD_TYPE_NONE
} bd_mcast_flood_type_e;

typedef enum _bd_member_type {
	BD_MEMBER_TYPE_STATIC = 1,
	BD_MEMBER_TYPE_DYNAMIC,
	BD_MEMBER_TYPE_OTHER
} bd_member_type_e;

typedef enum _bd_stlp_type {
	BD_STLP_TYPE_NULL_TYPE,
	BD_STLP_TYPE_VLAN_INTERFACE,
	BD_STLP_TYPE_VLAN_SET,
} bd_stlp_type_e;

typedef enum _bd_stp_port_state_type {
	BD_STP_PORT_STATE_TYPE_INIT,
	BD_STP_PORT_STATE_TYPE_DISABLED,
	BD_STP_PORT_STATE_TYPE_LEARNING,
	BD_STP_PORT_STATE_TYPE_FORWADING,
	BD_STP_PORT_STATE_TYPE_BLOCKED,
	BD_STP_PORT_STATE_TYPE_LISTENING,
} bd_stp_port_state_type_e;

typedef enum _ft_path_type {
	FT_PATH_TYPE_NONE,
	FT_PATH_TYPE_OTHER,
	FT_PATH_TYPE_CONNECTED,
	FT_PATH_TYPE_I3_LOCAL,
	FT_PATH_TYPE_I3_CONNECTED,
	FT_PATH_TYPE_STATIC,
	FT_PATH_TYPE_OSPF_INTRA_AREA,
	FT_PATH_TYPE_OSPF_INTER_AREA,
	FT_PATH_TYPE_OSPF_TYPE1_EXT,
	FT_PATH_TYPE_OSPF_TYPE2_EXT,
	FT_PATH_TYPE_TYPE1_NSSA,
	FT_PATH_TYPE_TYPE2_NSSA,
	FT_PATH_TYPE_BGP_INT,
	FT_PATH_TYPE_BGP_EXT,
	FT_PATH_TYPE_BGP_VPN,
	FT_PATH_TYPE_ISIS_LVL1_INT,
	FT_PATH_TYPE_ISIS_LVL2_INT,
	FT_PATH_TYPE_ISIS_LVL1_EXT,
	FT_PATH_TYPE_ISIS_LVL2_EXT,
	FT_PATH_TYPE_IGRP,
	FT_PATH_TYPE_RIP,
	FT_PATH_TYPE_HELLO,
	FT_PATH_TYPE_EGP,
	FT_PATH_TYPE_GGP,
	FT_PATH_TYPE_EIGRP_SUMM,
	FT_PATH_TYPE_EIGRP_INT,
	FT_PATH_TYPE_EIGRP_EXT,
	FT_PATH_TYPE_ESIS,
	FT_PATH_TYPE_BBNSPFIGP,
	FT_PATH_TYPE_IDPR,
	FT_PATH_TYPE_DVMRP,
	FT_PATH_TYPE_ICMP,
} ft_path_type_e;

typedef enum _ft_route_action_type {
	FT_ROUTE_ACTION_TYPE_INVALID,
	FT_ROUTE_ACTION_TYPE_LOCAL,
	FT_ROUTE_ACTION_TYPE_FORWARD,
	FT_ROUTE_ACTION_TYPE_REJECT,
	FT_ROUTE_ACTION_TYPE_DISCARD,
	FT_ROUTE_ACTION_TYPE_TUNNEL,
} ft_route_action_type_e;

typedef enum _ft_route_type {
	FT_ROUTE_TYPE_STATIC = 1,
	FT_ROUTE_TYPE_DYNAMIC,
} ft_route_type_e;

typedef enum _sldf_xc_type {
	SLDF_XC_TYPE_SIMPLE,
	SLDF_XC_TYPE_EGRESS,
	SLDF_XC_TYPE_INGRESS,
	SLDF_XC_TYPE_PW_EGRESS,
	SLDF_XC_TYPE_PW_INGRESS,
	SLDF_XC_TYPE_IMPLICIT,
	SLDF_XC_TYPE_RSVP_EGRESS,
	SLDF_XC_TYPE_RSVP_INGRESS,
	SLDF_XC_TYPE_RSVP_PART,
} sldf_xc_type_e;

typedef enum _sldf_seg_type
{
	SLDF_SEG_TYPE_UNKNOWN,
	SLDF_SEG_TYPE_IN,
	SLDF_SEG_TYPE_OUT,
	SLDF_SEG_TYPE_RESERVE_INFO,
} sldf_seg_type;

typedef enum _sldf_vpn_type {
	SLDF_VPN_TYPE_NONE,
	SLDF_VPN_TYPE_VPWS,
	SLDF_VPN_TYPE_VPLS
} sldf_vpn_type_e;

typedef enum _sldf_fec_type {
	SLDF_FEC_TYPE_NONE      = 0x0,
	SLDF_FEC_TYPE_HOST      = 0x1,
	SLDF_FEC_TYPE_PREFIX    = 0x2,
	SLDF_FEC_TYPE_VC        = 0x3,
	SLDF_FEC_TYPE_VC_GEN_ID = 0x4,
	SLDF_FEC_TYPE_VC_STATIC = 0x5,
	SLDF_FEC_TYPE_VC_ST_GEN = 0x6,
	SLDF_FEC_TYPE_VC_MS_PW  = 0x7,
	SLDF_FEC_TYPE_VC_BGP    = 0x8
} sldf_fec_type_e;

typedef enum _pw_type {
	PW_TYPE_NONE   = 0x0,
	PW_TYPE_STATIC = 0x1,
	PW_TYPE_LDP    = 0x2,
	PW_TYPE_BGP    = 0x3,
	PW_TYPE_MS     = 0x4
} pw_type_e;

typedef enum _sldf_pw_fec_type {
	SLDF_PW_FEC_TYPE_FR         = 0x0001,
	SLDF_PW_FEC_TYPE_AAL5       = 0x0002,
	SLDF_PW_FEC_TYPE_ATM        = 0x0003,
	SLDF_PW_FEC_TYPE_ETH_VLAN   = 0x0004,
	SLDF_PW_FEC_TYPE_ETHERNET   = 0x0005,
	SLDF_PW_FEC_TYPE_HDLC       = 0x0006,
	SLDF_PW_FEC_TYPE_PPP        = 0x0007,
	SLDF_PW_FEC_TYPE_CEM        = 0x0008,
	SLDF_PW_FEC_TYPE_ATM_VCC    = 0x0009,
	SLDF_PW_FEC_TYPE_ATM_VPC    = 0x000A,
	SLDF_PW_FEC_TYPE_IP_L2      = 0x000B,
	SLDF_PW_FEC_TYPE_ATM_VCC_11 = 0x000C,
	SLDF_PW_FEC_TYPE_ATM_VPC_11 = 0x000D,
	SLDF_PW_FEC_TYPE_AAL5_PDU   = 0x000E,
	SLDF_PW_FEC_TYPE_FR_PORT    = 0x000F,
	SLDF_PW_FEC_TYPE_CEP        = 0x0010,
	SLDF_PW_FEC_TYPE_SATOP_E1   = 0x0011,
	SLDF_PW_FEC_TYPE_SATOP_T1   = 0x0012,
	SLDF_PW_FEC_TYPE_SATOP_E3   = 0x0013,
	SLDF_PW_FEC_TYPE_SATOP_T3   = 0x0014,
	SLDF_PW_FEC_TYPE_CES        = 0x0015,
	SLDF_PW_FEC_TYPE_TDM        = 0x0016,
	SLDF_PW_FEC_TYPE_CES_CAS    = 0x0017,
	SLDF_PW_FEC_TYPE_TDM_CAS    = 0x0018,
	SLDF_PW_FEC_TYPE_FR_DLCI    = 0x0019
} sldf_pw_fec_type_e;

typedef enum _mpls_update_id_type {
	MPLS_UPDATE_ID_TYPE_NONE,
	MPLS_UPDATE_ID_TYPE_EGRESS_TUNNEL,
	MPLS_UPDATE_ID_TYPE_INGRESS_TUNNEL,
	MPLS_UPDATE_ID_TYPE_PW,
} mpls_update_id_type_e;

typedef enum _address_type {
	ADDRESS_TYPE_IPV4,
	ADDRESS_TYPE_IPV6
} address_type_e;

typedef enum _pp_link_state
{
	PP_LINK_STATE_UNKNOWN,
	PP_LINK_STATE_UP,
	PP_LINK_STATE_DOWN,
	PP_LINK_STATE_DOWN_ADMIN,
	PP_LINK_STATE_DOWN_UDLD,
	PP_LINK_STATE_LOWLAYERDWN
} pp_link_state_e;

typedef enum _pp_flow_control {
	PP_FLOW_CONTROL_UNKNOWN,
	PP_FLOW_CONTROL_ON,
	PP_FLOW_CONTROL_OFF,
	PP_FLOW_CONTROL_RX,
	PP_FLOW_CONTROL_TX
} pp_flow_control_e;

typedef enum _pp_autonegotiation {
	PP_AUTONEGOTIATION_UNKNOWN,
	PP_AUTONEGOTIATION_OFF,
	PP_AUTONEGOTIATION_ON
} pp_autonegotiation_e;

typedef enum _pp_transceiver
{
	PP_TRANSCEIVER_UNKNOWN,
	PP_TRANSCEIVER_COPPER,
	PP_TRANSCEIVER_FIBER,
	PP_TRANSCEIVER_AC,
} pp_transceiver_e;

typedef struct _bd_id {
	bd_type_e type;
	uint32_t id;
	uint32_t sub_id;
} bd_id_t;

typedef struct _bd_props {
	uint8_t mac_learning_enabled;
	uint8_t discard_unknown_dest;
	bd_mcast_flood_type_e multicast_flood_mode;
	uint32_t mac_aging_time;
	uint32_t fdb_max_size;
} bd_props_t;

typedef struct _bd {
	uint32_t vsi_id;
	bd_id_t id;
	bd_props_t props;
	uint32_t mcg_id;
} bd_t;

typedef struct _bd_binding_props {
	uint32_t max_mac_addresses;
	uint8_t mac_learning_enabled;
	uint8_t set_port_state;
	bd_stp_port_state_type_e port_state;
} bd_binding_props_t;

typedef struct _bd_binding_iface {
	uint32_t if_index;
	bd_member_type_e member_type;
	uint8_t split_horizon_group;
	bd_binding_props_t mutable_bind_properties;
} bd_binding_iface_t;

typedef struct _bd_binding_request {
	bd_id_t bd;
	size_t size;
	size_t count;
} bd_binding_request_t;

typedef struct _bd_ifs_remove {
	uint32_t if_index;
	uint32_t l3_vsi;
} bd_ifs_remove_t;

typedef struct _bd_binding_response {
	uint32_t if_index;
	uint8_t rc;
} bd_binding_response_t;

typedef struct _bd_vlan_range {
	uint16_t start;
	uint16_t end;
} bd_vlan_range_t;

typedef struct _bd_stlp {
	bd_stlp_type_e type;
	uint32_t if_index;
	uint32_t group_id;
	size_t vlan_ids_count;
	size_t vlan_ranges_count;
} bd_stlp_t;

typedef struct _bd_stlp_request {
	size_t size;
	bd_stp_port_state_type_e state;
	bd_stlp_t stlp;
} bd_stlp_request_t;

typedef struct _ft_next_hop {
	addr_t hop_addr;
	int32_t output_if_index;
	uint32_t path_cost;
	ft_route_type_e route_type;
	ft_path_type_e path_type;
	uint8_t connected;
	uint8_t loose_next_hop;
	uint8_t secondary;
	uint8_t rsvp_nh;
	uint32_t egr_obj_id;
	uint32_t egr_obj_encap_id;
	uint32_t parent_egr_obj_id;
	uint8_t mac[ETH_ALEN];
	uint32_t label;
} ft_next_hop;

typedef struct _ft_route_entry {
	uint8_t invalid:1,
	        stale:1,
	        ecmp_full:1,
	        is_virtual:1,
	        use_static_arp:1;
	uint32_t id;
	addr_t dst_addr;
	ft_route_action_type_e action_type;
	uint32_t num_hops;
	addr_t match_addr;
	uint32_t route_user_data_len;
	uint8_t user_data[8];
	uint32_t admin_distance;
	uint32_t vrf_id;
	int32_t local_dest_if_index;
	int ecmp_grp_id;
	uint32_t failover_id;
	ft_next_hop nexthops[0];
} ft_route_entry_t;

typedef struct _ft_route_request {
	size_t count;
	size_t size;
} ft_route_request_t;

typedef struct _ft_route_updare_response {
	uint32_t id;
	int8_t rc;
} ft_route_update_response_t;

typedef struct _ft_audit {
	uint32_t vrf_id;
	int is_ipv6;
} ft_audit_t;

typedef struct _sldf_xc_id {
	uint32_t xc_index;
	uint32_t in_seg_index;
	uint32_t in_label;
	uint32_t out_seg_index;
	uint32_t in_seg_label_source;
} sldf_xc_id;

typedef struct _sldf_nh {
	addr_t hop_addr;
	int egr_obj_id;
	int egr_obj_encap_id;
	uint8_t mac[ETH_ALEN];
} sldf_nh_t;

typedef struct _sldf_label_block_info {
	uint8_t local_route_distinguisher[8];
	uint16_t local_ve_id;
	uint16_t remote_ve_id_block_offset;

	uint16_t label_block_size;
	uint32_t base_label;
} sldf_label_block_info_t;

#if !defined(BOARD_BPE_PIZZABOX)
typedef struct _service_tunnel_id_set
{
	uint32_t new_tunnel_id;
	network_ip_address_t peer_ip;
	uint32_t out_label;
} service_tunnel_id_set_t;
#endif

typedef struct _sldf_update_block_t
{
	union
	{
		struct
		{
			uint32_t is_in_label_valid:1,
			         is_term_tunnel:1;
			uint32_t in_label;
			uint32_t in_if_index;
			uint32_t in_seg_index;           //For recovery only
		} in_seg;

		struct
		{
			uint32_t  push_label:1,
			          is_out_label_valid:1,
			          is_next_hop_mac_valid:1, //For sync only
			          php:1,
			          inactive:1;
			uint32_t  out_label;
			uint32_t  out_if_index;
			network_ip_address_t next_hop;
			uint32_t  out_seg_index;         //For recovery only
			uint32_t  vif_index;
			uint32_t  vpn_tunnel_vif_index;  //For recovery only
			uint32_t  tunnel_id;             //For sync only
			uint32_t  arp_encap_id;      //For sync only
			uint8_t   next_hop_mac[ETH_ALEN];//For sync only
		} out_seg;

		struct
		{
			uint32_t lsp_xc_index;
			uint32_t out_if_index;
			float bandwidth; //kbps
			uint8_t release_rsrc;
		} reserve_info;
	};
	sldf_seg_type type;
} sldf_update_block_t;

typedef struct _sldf_fec_data {
	uint8_t            fec_type;
	uint8_t            vc_fec_type;
	uint16_t           vc_fec_vc_type;
	uint8_t            vc_fec_use_control_word;
	uint64_t           vc_fec_group_id;
	uint64_t           vc_fec_vc_id;
#if 0
	uint8_t            vc_fec_control_word_config;
	uint64_t           vc_remote_group_id;
#endif

	pw_type_e     pw_type;
	union
	{
		struct
		{
			uint8_t  vc_fec_ldp_peer[LDP_ID_LENGTH];
		} stat;
		struct
		{
			uint8_t  pw_saii_len;
			uint8_t  pw_saii[LDP_ID_FEC_MAX_AII];
			uint8_t  pw_taii_len;
			uint8_t  pw_taii[LDP_ID_FEC_MAX_AII];
			uint8_t  pw_agi_len;
			uint8_t  pw_agi[LDP_ID_FEC_MAX_AGI];
		} ldp;
		struct
		{
			uint16_t local_ve_id;
			uint16_t remote_ve_id;
			uint8_t  local_rd[BGP_ID_VPN_ROUTE_DIST_LEN];
			uint8_t  remote_rd[BGP_ID_VPN_ROUTE_DIST_LEN];
		} bgp;
	} id;
} sldf_fec_data_t;

#define SLDF_UPDATE_VRF_PAYLOAD_SIZE (int)(2 * sizeof(uint32_t))

typedef struct _sldf_update_entry {
	uint32_t xc_id;              //For recovery only
	sldf_xc_id egress_id;
	sldf_xc_id ingress_id;

	uint32_t owner_type;         //For recovery only

	network_ip_prefix_t fec;
	uint32_t vrf_index;
	sldf_fec_type_e fec_type;    //For recovery only
	sldf_fec_data_t fec_data;    //For recovery only

	uint32_t use_control_word:1,
	         is_active:1,        //For sync only
	         is_cp_activated:1,  //For sync only
	         perf_active:1,
	         is_vrf_tunnel:1,
	         owned_by_rsvp:1,
	         in_pending_list;    //For sync only

	sldf_xc_type_e type;

	uint32_t pw_vif_index;
	uint32_t bundle_vif_index;
	uint32_t ac_if_index;
	uint32_t pw_index;
	uint32_t pw_encap_id;     //For sync only
	uint32_t pw_port_id;      //For sync only
	sldf_vpn_type_e vpn_type;
	uint32_t vpn_id;
	uint32_t redn_group_id;

	uint32_t in_vrf_index;
#if defined(BOARD_BPE_PIZZABOX)
	uint32_t vrf_rif_vsi;
#else
	uint32_t vrf_rif_vsis[MAX_LC_BOARDS];
#endif
	sldf_label_block_info_t label_block_info;

	uint32_t perf_correlator;
	struct timeval send_start;

	uint32_t updates_count;
	uint32_t out_seg_count;
	uint32_t in_seg_count;
	uint32_t reserve_info_count;
	sldf_update_block_t updates[0];
} sldf_update_entry_t;

typedef struct  _sldf_mpls_db_last_recover_entry_key
{
	union
	{
		struct
		{
			uint32_t in_label;
		}sw;
		struct
		{
			uint32_t out_label;
			uint32_t in_label;
			network_ip_prefix_t fec;
			network_ip_address_t next_hop;
			uint8_t l3_vrf;
		}tun;
		struct
		{
			sldf_fec_data_t fec_data;
		}pw;
		struct
		{
			uint8_t local_route_distinguisher[8];
			uint16_t local_ve_id;
			uint16_t remote_ve_id_block_offset;
		}label_block;
		struct
		{
			network_ip_address_t fec;
			uint32_t label;
		}fec_node;
		struct
		{
			uint32_t label;
			network_ip_address_t next_hop;
			uint16_t lsp_tunnel_id;
			uint16_t lsp_tunnel_instance;
		}rsvp;
	};
	uint8_t key_valid;
} sldf_mpls_db_last_recover_entry_key_t;

typedef struct _sldf_mpls_db_get_entry {
	sldf_mpls_db_last_recover_entry_key_t key;
	uint8_t key_valid;
	uint32_t owner_type;
} sldf_mpls_db_get_entry_t;

typedef struct _sldf_pw_swithover {
	sldf_fec_data_t fec_data_old_active_pw;
	sldf_fec_data_t fec_data_new_active_pw;
	uint8_t  new_active_info_valid:1,
	         old_active_info_valid:1;
} sldf_pw_switchover_t;

typedef struct _sldf_reroute {
	uint32_t old_label;
	network_ip_address_t old_next_hop;
	uint32_t new_label;
	network_ip_address_t new_next_hop;
} sldf_reroute_t;

typedef struct _la_microbfd_trap_set_request {
	uint32_t if_index;
	uint32_t src_ip;
	uint32_t dst_ip;
	uint16_t udp_dst_port;
	uint8_t  mc_mac_addr[ETH_ALEN];
} la_microbfd_trap_set_request_t;

typedef struct _la_microbfd_trap_unset_request {
	uint32_t if_index;
} la_microbfd_trap_unset_request_t;

#if !defined(BOARD_BPE_LINECARD)
typedef struct _mpf_act_src_query_request {
	uint8_t  is_ipv4;
	uint32_t vrf_index;
	uint32_t entity_index;
} mpf_act_src_query_request_t;
#endif /* !(BOARD_BPE_LINECARD) */

typedef struct {
	uint32_t vrf_id;
	uint16_t local_discriminator;
} bfd_sess_id_t;

typedef enum {
	bfd_event_type_timeout,
	bfd_event_type_timein,
	bfd_event_type_state_change,
	bfd_event_type_error,
	bfd_event_type_try_again,
} bfd_event_type_t;

typedef struct _bfd_session_set_request {
	struct timeval send_time;

	uint32_t if_index;
	uint32_t local_port;
	uint8_t per_hop_behavior;

	network_ip4_address_t local_addr;
	network_ip4_address_t next_hop_addr;
	network_ip4_address_t remote_addr;
	uint32_t              local_discriminator; // used as session identifier
	uint32_t              remote_discriminator;

	uint8_t  is_remote_discriminator_specified : 1;
	uint8_t  multihop        : 1;
	uint8_t  have_local_addr : 1;
	uint8_t  create          : 1;
	uint16_t udp_src_port;
	uint8_t  ip_ttl;

	uint32_t local_min_tx, remote_min_tx;
	uint32_t local_min_rx, remote_min_rx;

	uint8_t local_diag,        remote_diag;
	uint8_t local_state,       remote_state;
	uint8_t local_flags,       remote_flags;
	uint8_t local_detect_mult, remote_detect_mult;

	uint32_t bfd_period;
	uint32_t bfd_detection_time;

	uint32_t vrf_id;
} bfd_session_set_request_t;

typedef struct {
	uint32_t local_discriminator;
	int rc;
} bfd_sesstion_unset_reply_t;

typedef struct {
	uint32_t vrf_id;
	uint16_t local_discriminator:13;
	bfd_event_type_t type:3;
} bfd_event_t;

typedef struct count_stat_msg_port_struct {
	uint32_t if_index; //interface index
	uint32_t load_interval;// load interval for rate culc
	uint64_t inbtsrate; //Input Rate(bytes/s) in last 300sec (average)
	uint64_t inpcktsrate; // Input Rate(packets/s) in last 300sec (average)
	uint64_t outbtsrate; //Output Rate(bytes/s) in last 300sec (average)
	uint64_t outpcktsrate; // Output Rate(packets/s) in last 300sec (average)
	uint64_t inUpcktsrate;// input Rate(Upackets/s) in last 300sec (average)
	uint64_t inMLTpcktsrate;// Input Rate(Mpackets/s) in last 300sec (average)
	uint64_t inBRDpcktsrate;// input Rate(Bpackets/s) in last 300sec (average)
	uint64_t outUpcktsrate;// Output Rate(Upackets/s) in last 300sec (average)
	uint64_t outMLTpcktsrate;// Output Rate(Mpackets/s) in last 300sec (average)
	uint64_t outBRDpcktsrate;// Output Rate(Bpackets/s) in last 300sec (average)
	uint64_t inbts; //Input bytes
	uint64_t inUpckts; //Input U-packets
	uint64_t inNUpckts; //Input NU-packets
	uint64_t inError; //Input Error
	uint64_t outbts; //Output bytes
	uint64_t outUpckts; // Output U-packets
	uint64_t outNUpckts; // Output NU-packets
	uint64_t outError; // Output Error
	uint64_t HC_inbts;//HC input ocktes
	uint64_t HC_inUpckts;//HC input Unicast packets
	uint64_t HC_inMLTpckts;// HC input Multicast packets
	uint64_t HC_inBRDpckts;// HC input Broadcast packets
	uint64_t HC_outbts;//HC output ocktes
	uint64_t HC_outUpckts;//HC output Unicast packets
	uint64_t HC_outMLTpckts;// HC output Multicast packets
	uint64_t HC_outBRDpckts;// HC output Broadcast packets
	uint64_t ttlbts; //Total bytes
	uint64_t ttlpckts; //Total packets
	uint64_t in64pckts;//Received pkts 64 octets
	uint64_t in65to127pckts;//Received pkts 65 to 127 Octets
	uint64_t in128to255pckts;//Received pkts 128 to 255 Octets
	uint64_t in256to511pckts;//Received pkts 256 to 511 Octets
	uint64_t in512to1023pckts;//Received pkts 512 to 1023 Octets
	uint64_t in1024to1518pckts;//Received pkts 1024 to 1518 Octets
	uint64_t in1519to2047pckts;//Received pkts 1519 to 2047 Octets
	uint64_t in2048to4095pckts;//Received pkts 2048 to 4095 Octets
	uint64_t in4096to9216pckts;//Received pkts 4096 to 9216 Octets
	uint64_t out64pckts;//Transmitted pkts 64 octets
	uint64_t out65to127pckts;//Transmitted pkts 64 to 127 Octets
	uint64_t out128to255pckts;//Transmitted pkts 128 to 255 Octets
	uint64_t out256to511pckts;//Transmitted pkts 255 to 511 Octets
	uint64_t out512to1023pckts;//Transmitted pkts 512 to 1023 Octets
	uint64_t out1024to1518pckts;//Transmitted pkts 1024 to 1518 Octets
	uint64_t out1519to2047pckts;//Transmitted pkts 1519 to 2047 Octets
	uint64_t out2048to4095pckts;//Transmitted pkts 2048 to 4095 Octets
	uint64_t out4096to9016pckts;//Transmitted pkts 4096 to 9216 Octets
	uint64_t oversize;// Oversize packets
	uint64_t jabber;// Jabber packets
	uint64_t collision;// Collisions
	uint64_t crc_error;// CRC error
	uint64_t fcs_error;// FCS error
	uint64_t single_collision;//Single collisions
	uint64_t multi_cillision;//Multi collision
	uint64_t sqet_error;//SQET test errors
	uint64_t deffered;//Deferred transmissions
	uint64_t late_collision;//Late collisions
	uint64_t excessive_collision;//Excessive collisions
	uint64_t out_mac_error;//Internal MAC transmit errors
	uint64_t sense_error;//Carrier sense errors
	uint64_t too_long;//Frame too longs
	uint64_t in_mac_error;//Internal MAC receive errors
	uint64_t symb_error;//Symbol errors
} count_stat_msg_port_struct_t;

typedef struct count_stat_msg_struct {
	int phys_port_val; //number of physical ports
	count_stat_msg_port_struct_t port[0];
} count_stat_msg_struct_t;

typedef struct count_stat_summary_msg_port_struct {
	uint32_t if_index; //interface index
	uint64_t HC_inbts;//HC input ocktes
	uint64_t HC_inUpckts;//HC input Unicast packets
	uint64_t HC_inMLTpckts;// HC input Multicast packets
	uint64_t HC_inBRDpckts;// HC input Broadcast packets
	uint64_t HC_outbts;//HC output ocktes
	uint64_t HC_outUpckts;//HC output Unicast packets
	uint64_t HC_outMLTpckts;// HC output Multicast packets
	uint64_t HC_outBRDpckts;// HC output Broadcast packets
} count_stat_summary_msg_port_struct_t;

typedef struct stat_summary_msg_struct {
	int phys_port_val; //number of physical ports
	count_stat_summary_msg_port_struct_t port[0];
} count_stat_summary_msg_struct_t;

typedef struct count_stat_sub_if_msg_port_struct {
	uint32_t if_index;
	uint32_t load_interval;
	uint64_t inbts;           //input ocktes
	uint64_t inpckts;         //input packets
	uint64_t outbts;          //output ocktes
	uint64_t outpckts;        //output packets
	uint64_t indroppckts;     //input dropped packets(not used yet)
	uint64_t outdroppckts;    //output dropped ocktes(not used yet)
	uint64_t indropbts;       //input dropped bts(not used yet)
	uint64_t outdropbts;      //output dropped bts(not used yet)
	uint64_t inbtsrate;       //input Rate(bits/s) in last 300sec (average)
	uint64_t inpcktsrate;     //input Rate(packets/s) in last 300sec (average)
	uint64_t outbtsrate;      //output Rate(bitss/s) in last 300sec (average)
	uint64_t outpcktsrate;    //output Rate(packets/s) in last 300sec (average)
} count_stat_sub_if_msg_port_struct_t;

typedef struct
{
	size_t memsize; // size of memory really allocated for this structure
	int phys_port_cnt;
	int bundle_cnt;
	int sub_cnt;
	uint8_t data[0]; // port - count_stat_msg_port_struct_t, bundle - count_stat_msg_port_struct_t, sub - count_stat_sub_if_msg_port_struct_t
} count_stat_all_t;

#define DEFAULT_LOAD_INTERVAL 300 //seconds
#define MIN_LOAD_INTERVAL     20  //seconds

typedef struct stat_sub_if_msg_struct {
	int sub_port_count; //number of sub if
	count_stat_sub_if_msg_port_struct_t port[0];
} count_stat_sub_if_msg_struct_t;

typedef struct count_stat_slot_port_port_struct {
	uint32_t port;                // port number
	uint32_t slot;                // slot number
	uint32_t load_interval;       // load interval for rate culc
	uint8_t  is_oper_up;          // oper state
	uint64_t inbtsrate;           // Input Rate(bytes/s) in last 300sec (average)
	uint64_t inpcktsrate;         // Input Rate(packets/s) in last 300sec (average)
	uint64_t outbtsrate;          // Output Rate(bytes/s) in last 300sec (average)
	uint64_t outpcktsrate;        // Output Rate(packets/s) in last 300sec (average)
	uint64_t inUpcktsrate;        // input Rate(Upackets/s) in last 300sec (average)
	uint64_t inMLTpcktsrate;      // Input Rate(Mpackets/s) in last 300sec (average)
	uint64_t inBRDpcktsrate;      // input Rate(Bpackets/s) in last 300sec (average)
	uint64_t outUpcktsrate;       // Output Rate(Upackets/s) in last 300sec (average)
	uint64_t outMLTpcktsrate;     // Output Rate(Mpackets/s) in last 300sec (average)
	uint64_t outBRDpcktsrate;     // Output Rate(Bpackets/s) in last 300sec (average)
	uint64_t inError;             // Input Error
	uint64_t outError;            // Output Error
	uint64_t oversize;            // Oversize packets
	uint64_t collision;           // Collisions
	uint64_t crc_error;           // CRC error
	uint64_t fcs_error;           // FCS error
	uint64_t single_collision;    // Single collisions
	uint64_t multi_cillision;     // Multi collision
	uint64_t sqet_error;          // SQET test errors
	uint64_t deffered;            // Deferred transmissions
	uint64_t late_collision;      // Late collisions
	uint64_t excessive_collision; // Excessive collisions
	uint64_t out_mac_error;       // Internal MAC transmit errors
	uint64_t sense_error;         // Carrier sense errors
	uint64_t too_long;            // Frame too longs
	uint64_t in_mac_error;        // Internal MAC receive errors
	uint64_t symb_error;          // Symbol errors
} count_stat_slot_port_port_struct_t;

typedef enum _sub_count_stat_mod_type {
	SUB_COUNT_STAT_RATE_DISABLED,
	SUB_COUNT_STAT_RATE_ENABLED
} sub_count_stat_mod_e;

typedef enum _msg_host_action {
	MSG_HOST_ACTION_ADD,
	MSG_HOST_ACTION_DEL
} msg_host_action_e;

typedef struct _msg_host {
	msg_host_action_e action;
	network_ip_address_t addr;
	uint32_t vrf_id;
	uint8_t mac[ETH_ALEN];
	uint32_t if_index;
	int encap_id;
	int fec_id;
} msg_host_t;

typedef enum _fdb_mac_entry_type
{
	FDB_MAC_DYNAMIC     = 0x1,
	FDB_MAC_STATIC      = 0x2,
	FDB_MAC_REG_ENTRY   = 0x3
} fdb_mac_entry_type_t;

typedef enum _fdb_flush_type
{
	FDB_FLUSH_ALL        = 0x01,
	FDB_FLUSH_ADDR_LIST  = 0x02,
	FDB_FLUSH_INTERFACE  = 0x03
} fdb_flush_type_t;

#define MPLS_LABEL_MAX_LEN 16

typedef struct _labelled_interface
{
	uint32_t if_index;
	struct
	{
		uint16_t flags;
		uint8_t pad[2];
		uint32_t len;
		uint8_t label[MPLS_LABEL_MAX_LEN];
	} label;
} labelled_interface_t;

typedef struct _fdb_mac
{
	uint8_t mac_addr[6];
} fdb_mac_t;

typedef struct _fdb_mac_entry
{
	fdb_mac_t mac;
	union
	{
		struct
		{
			fdb_mac_entry_type_t mac_type;
			uint32_t num_interfaces;
			uint32_t num_labelled_interfaces;
			uint32_t interfaces[0];
		} upd;
		struct
		{
			fdb_mac_entry_type_t mac_type;
		} del;
		struct
		{
			uint32_t num_interfaces;
			uint32_t interfaces[0];
		} query;
	};
} fdb_mac_entry_t;

typedef struct _fdb_flush
{
	fdb_flush_type_t flush_type;
	uint32_t if_index;
	uint32_t num_macs;
	fdb_mac_t macs[0];
} fdb_flush_t;

typedef struct _fdb_action_msg
{
	bd_id_t bd_id;
	union
	{
		fdb_flush_t fdb_flush;
		fdb_mac_entry_t mac_entry;
	};
} fdb_action_msg_t;

typedef struct _trunk_hash_opt {
	uint8_t tid;
	int type;
	int arg;
} trunk_hash_opt_t;


#define MAC_LEN 6

typedef struct
{
	unsigned char addr[MAC_LEN];
} l2_mac_type;

typedef struct
{
	l2_mac_type mac;                  /* 802.3 MAC address. */
	int bid;                          /* Bridge domain id, fill in pp manager */
	int unit;
	int static_flag;                  /* 0 - mac is dynamic, 1 - is static */
	uint32_t if_index;
	uint32_t lc_mask;

} l2_mac_t;


typedef struct
{
	uint32_t bid;
	int total_mac_learned;
	int lc_mac_learned[MAX_LC_BOARDS];
} mac_learned_on_bridge_t;

typedef struct
{
	int total_mac_learned;
	int lc_mac_learned[MAX_LC_BOARDS];
	int bridge_count;
	mac_learned_on_bridge_t mac_on_bridges[0];

} ipc_mac_counters_t;

typedef struct _interface_status {
	uint32_t if_index;
	uint32_t port;
	struct timeval time_since_state_changed;
	uint8_t mac[HW_ADDR_LEN];
	pp_link_state_e link_state;
	pp_autonegotiation_e autonegotiation;
	pp_flow_control_e flow_control;
	iface_dplx_e duplex;
	iface_spd_e speed;
	iface_spd_e ability;
	pp_transceiver_e transceiver;
	uint16_t mtu;
	uint16_t ip_mtu;
} interface_status_t;

typedef struct interface_status_msg {
	int phys_port_val; //number of physical ports
	interface_status_t port[0];
} interface_status_msg_t;

typedef struct sub_interface_status_msg {
	uint32_t if_index;
	uint8_t mac[HW_ADDR_LEN];
	uint16_t mtu;
	uint16_t ip_mtu;
	struct timeval time_since_state_changed;
} sub_interface_status_msg_t;

typedef struct
{
	int unit;
	int slot;
	int limit_flag; /* 1 -is active, 0 - disabled  */
} mac_limit_t;

typedef struct
{
	uint32_t num_mac_limits;
	mac_limit_t mac_limit[0];
} ipc_mac_limit_t;

#define LB_HASH_SRC_MAC           0x00000001
#define LB_HASH_DST_MAC           0x00000002
#define LB_HASH_SRC_IP            0x00000004
#define LB_HASH_DST_IP            0x00000008
#define LB_HASH_SRC_PORT          0x00000010
#define LB_HASH_DST_PORT          0x00000020
#define LB_HASH_MPLS_LABEL        0x00000040
#define LB_HASH_PASSENGER_MPLS    0x00000080
#define LB_HASH_PASSENGER_MPLS_CW 0x00000100


typedef struct
{
	uint32_t lc_number;
	size_t ifaces_cnt;
	iface_t ifaces[0];
} msg_lc_ports_cfg_t;

typedef struct
{
	size_t size;
	int ports_cnt[MAX_LC_BOARDS];
	iface_t ports[0];
} pp_ports_state_db_t;

#if !defined(BOARD_BPE_PIZZABOX)
#define SFI_PORTS_MAX_FMC 128
#define SFI_PORTS_MAX_LC 36
#if !defined(MAX_FMC_BOARDS)
#define MAX_FMC_BOARDS 2
#endif

#define SFI_PORT_PRBS_OK     0
#define SFI_PORT_PRBS_ERROR -1

typedef struct
{
	int rc;
	struct
	{
		int status;
		int port;
#if defined(BOARD_BPE_CTRLCARD)
	} info[SFI_PORTS_MAX_FMC];
#elif defined(BOARD_BPE_LINECARD)
	} info[SFI_PORTS_MAX_LC];
#else
	} info[0];
#endif
} pp_prbs_dev_stat_gather_s;

typedef struct
{
	pp_prbs_dev_stat_gather_s lc[MAX_LC_BOARDS];
	pp_prbs_dev_stat_gather_s fmc[MAX_FMC_BOARDS];
} pp_prbs_unit_stat_gather_s;

typedef struct
{
	int lc_rc[MAX_LC_BOARDS];
	int fmc_rc[MAX_FMC_BOARDS];
} pp_prbs_unit_test_gather_s;
#endif /* !BOARD_BPE_PIZZABOX */

typedef struct
{
	uint32_t fdb_max_size;
	uint32_t hw_max_size;
	int slot_id;
} pp_mac_limit_info;

typedef struct
{
	int slot;
	char monitor_name[64];
} pp_stat_board_not_found_t;

typedef struct
{
	uint32_t bridge_domain;
	int slot_id;
} pp_vfi_limit_info_t;

typedef struct
{
	/*0 - if limit reached, 1 if ethertype was applied from waiters queue */
	uint8_t  is_apply;
	uint32_t if_index;
	uint32_t outer_tpid;
	uint32_t inner_tpid;
} pp_tpid_limit_info_t;

typedef enum _pp_tpid_egress_rewrite_limit_info_e
{
	PP_REWRITE_UNDEFINED = -1,
	PP_REWRITE_ACTION_LIMIT_REACHED,
	PP_REWRITE_PROFILE_LIMIT_REACHED,
	PP_REWRITE_NO_PHYS_INTERFACES_WITH_TPID,
	PP_REWRITE_ADD_TO_WAITERS,
	PP_REWRITE_APPLIED
} pp_tpid_egress_rewrite_limit_info_e;

typedef struct
{
	uint32_t if_index;
	pp_tpid_egress_rewrite_limit_info_e info;
	uint32_t outer_tpid;
	uint32_t inner_tpid;
	uint8_t is_egress;
} pp_tpid_egress_rewrite_limit_info_t;

typedef struct
{
	uint32_t if_index;
	int encap_id;
} pp_lfa_switch_params_t;

typedef enum _pp_mpls_info_request_type_e
{
	PP_MPLS_INFO_TYPE_NONE = 0,
	PP_MPLS_INFO_TYPE_SWITCH,
	PP_MPLS_INFO_TYPE_TUNNEL_EGRESS,
	PP_MPLS_INFO_TYPE_TUNNEL_INGRESS,
	PP_MPLS_INFO_TYPE_L3VPN_TUNNEL_EGRESS,
	PP_MPLS_INFO_TYPE_L3VPN_TUNNEL_INGRESS,
	PP_MPLS_INFO_TYPE_PW,
	PP_MPLS_INFO_TYPE_FEC_NODE,
	PP_MPLS_INFO_TYPE_RSVP_TUNNEL_EGRESS,
	PP_MPLS_INFO_TYPE_RSVP_TUNNEL_INGRESS,
} pp_mpls_info_request_type_e;

typedef enum _pp_mpls_info_switch_action_e
{
	PP_MPLS_INFO_SWITCH_ACTION_INVALID = 0,
	PP_MPLS_INFO_SWITCH_ACTION_SWAP,
	PP_MPLS_INFO_SWITCH_ACTION_PHP,
	PP_MPLS_INFO_SWITCH_ACTION_POP,
	PP_MPLS_INFO_SWITCH_ACTION_NOP,
	PP_MPLS_INFO_SWITCH_ACTION_PUSH,
} pp_mpls_info_switch_action_e;

typedef enum _pp_mpls_egress_info_type_e
{
	PP_MPLS_EGRESS_OBJ_TYPE_INVALID,
	PP_MPLS_EGRESS_EGR_OBJ_TYPE_GENERAL,
	PP_MPLS_EGRESS_EGR_OBJ_TYPE_ECMP_GRP
} pp_mpls_egress_info_type_e;

typedef struct _pp_mpls_egress_info_t
{
	pp_mpls_egress_info_type_e type;
	uint32_t ref_cnt;
	int32_t ecmp_grp_id;
	int32_t failover_id;
	union
	{
		struct
		{
			int32_t id;
			int32_t encap_id;
			uint32_t if_index;
			uint8_t mac[ETH_ALEN];
			int32_t failover_egress_id;
			int32_t failover_encap_id;
			uint32_t failover_if_index;
			uint8_t failover_mac[ETH_ALEN];
			uint8_t failover_status;
		} general;
		struct
		{
			int32_t base_member_id;
			uint32_t members_cnt;
		} ecmp;
	};
} pp_mpls_egress_info_t;

typedef struct _pp_mpls_switch_info_t
{
	sldf_xc_id id;
	uint32_t in_label;
	uint32_t in_iface;
	network_ip_prefix_t fec;
	uint8_t in_label_valid:1,
	        is_stale:1,
	        is_active:1;
	pp_mpls_info_switch_action_e action;
	uint32_t in_if_vsi;
	uint32_t out_iface;
	uint32_t out_if_vsi;
	uint32_t owner_type;
	//TODO fec node
} pp_mpls_switch_info_t;

typedef struct _pp_mpls_info_request_switch_t
{
	uint32_t have_in_label:1,
	         have_in_if:1,
	         have_fec:1,
	         get_main_info:1,
	         get_fec_node_info:1;
	pp_mpls_switch_info_t info;
} pp_mpls_switch_info_request_t;

typedef struct _pp_mpls_info_response_switch_t
{
	uint32_t count;
	pp_mpls_switch_info_t info[0];
} pp_mpls_info_response_switch_t;

typedef enum _pp_mpls_info_tunnel_direction_e
{
	PP_MPLS_TUNNEL_DIRECTION_INVALID = 0,
	PP_MPLS_TUNNEL_DIRECTION_INGRESS,
	PP_MPLS_TUNNEL_DIRECTION_EGRESS
} pp_mpls_info_tunnel_direction_e;

typedef enum _pp_mpls_parent_tunnel_type_e
{
	PP_MPLS_PARENT_TUNNEL_TYPE_NONE,
	PP_MPLS_PARENT_TUNNEL_TYPE_PHP,
	PP_MPLS_PARENT_TUNNEL_TYPE_NORMAL,
} pp_mpls_parent_tunnel_type_e;

typedef struct _pp_mpls_tunnel_info_t
{
	network_ip_address_t next_hop_addr;
	uint8_t next_hop_mac[ETH_ALEN];
	uint32_t label;
	uint32_t if_index;
	uint32_t vif_index;
	uint32_t owner_type;
	sldf_xc_id xc_id;
	sldf_fec_type_e fec_type;
	pp_mpls_info_tunnel_direction_e type;
	network_ip_prefix_t fec;
	uint32_t ingress_vrf_index;

	uint16_t is_stale:1,
	         in_active_tunnels_set:1,
	         is_arp_acquired:1,
	         is_active:1,
	         push_label:1,
	         is_parent_local:1,
	         is_vrf_tunnel:1,
	         is_pend_for_act:1;
	uint32_t tunnel_id;
	uint32_t new_tunnel_id;
	uint32_t out_rif;
	uint32_t out_gport;
	uint32_t parent_l3_if_vsi;
	uint32_t assigned_l3_vrf_if_index;
	uint32_t mpls_action;
	uint32_t parent_encap_id;
	pp_mpls_parent_tunnel_type_e parent_tunnel_type;
	uint32_t vpn_tunnel_vif_index;
} pp_mpls_tunnel_info_t;

typedef struct _pp_mpls_tunnel_info_request_t
{
	uint32_t have_fec:1,
	         have_label:1,
	         have_next_hop:1,
	         have_vif_index:1,
	         have_if:1,
	         have_type:1,
	         have_is_l3vpn:1,
	         have_tunnel_id:1,
	         have_is_acive:1,
	         //TODO other flags
	         get_main_info:1;

	pp_mpls_tunnel_info_t info;
} pp_mpls_tunnel_info_request_t;

typedef struct _pp_mpls_info_response_tunnel_t
{
	uint32_t count;
	pp_mpls_tunnel_info_t info[0];
} pp_mpls_info_response_tunnel_t;

typedef struct _pp_mpls_pw_info_t
{
	uint32_t use_cw:1,
	         is_vpls_vpn:1,
	         is_active:1,
	         is_active_by_cp:1,
	         is_added_to_label_and_egress_obj_tree:1,
	         is_stale:1,
	         redn_group_linked:1,
	         assoc_with_trunk:1;
	uint32_t index;
	uint32_t port_id;
	uint32_t encap_id;
	uint32_t vif_index;
	uint32_t bundle_vif_index;
	uint32_t redn_group_id;
	uint32_t vpws_vpn_id;
	uint32_t vpls_vpn_id;
	uint32_t vpls_bd;
	uint32_t vpls_bd_vsi;
	uint32_t ac_if_index;
	network_ip_prefix_t fec_addr;
	uint32_t owner_type;
	uint32_t out_egress_obj_id;
	uint32_t out_gport;
	sldf_fec_data_t fec_data;

	uint32_t in_label;
	uint32_t out_label;
} pp_mpls_pw_info_t;

typedef struct _pp_mpls_pw_info_request_t
{
	uint32_t have_in_label:1,
	         have_out_label:1,
	         have_fec:1,
	         have_pw_id:1,
	         have_vif_index:1,
	         //TODO other flags
	         get_main_info:1,
	         get_fec_node_info:1;

	pp_mpls_pw_info_t info;
} pp_mpls_pw_info_request_t;

typedef struct _pp_mpls_info_response_pw_t
{
	uint32_t count;
	pp_mpls_pw_info_t info[0];
} pp_mpls_info_response_pw_t;

typedef enum _pp_mpls_pending_info_type
{
	PP_PENDING_INFO_TYPE_NONE,
	PP_PENDING_INFO_TYPE_SW,
	PP_PENDING_INFO_TYPE_TUNNELS,
	PP_PENDING_INFO_TYPE_PWS,
	PP_PENDING_INFO_TYPE_L3VPN_TUNNELS,
	PP_PENDING_INFO_TYPE_ALL,
} pp_mpls_pending_info_type;

typedef struct _pp_pending_info_t
{
	union
	{
		struct
		{
			uint32_t in_label;
			uint32_t in_iface;
			uint32_t out_label;
			uint32_t out_iface;
			network_ip_address_t next_hop;
			network_ip_prefix_t tunnel_fec;
		} switch_entry;

		struct
		{
			uint32_t iface;
			uint32_t label;
			network_ip_address_t next_hop;
			network_ip_prefix_t tunnel_fec;
		} tunnel;

		struct
		{
			uint32_t count;
			uint32_t label;
			network_ip_address_t tunnel_fec;
		} l3vpn_fec_node;

		struct
		{
			sldf_fec_data_t fec_data;
		} pw;
	};
} pp_pending_info_t;

typedef struct _pp_pending_info_request_t
{
	pp_mpls_pending_info_type type;
} pp_pending_info_request_t;

typedef struct _pp_pending_info_response_t
{
	uint32_t swith_entries_count;
	uint32_t egress_tunnels_count;
	uint32_t l3vpn_egress_tunnels_count;
	uint32_t l3vpn_egress_tunnels_php_count;
	uint32_t ingress_tunnels_count;
	uint32_t pws_count;

	pp_pending_info_t info[0];
} pp_pending_info_response_t;

typedef enum _pp_mpls_command_type_e
{
	PP_MPLS_COMMAND_NONE = 0,
	PP_MPLS_COMMAND_IDS_SET,
	PP_MPLS_COMMAND_ACTIVATE,
	PP_MPLS_COMMAND_DEACTIVATE,
	PP_MPLS_COMMAND_DELETE,
} pp_mpls_command_type_e;

typedef enum _pp_mpls_object_type_e
{
	PP_MPLS_OBJECT_TYPE_NONE = 0,
	PP_MPLS_OBJECT_TYPE_SW_ENTRY,
	PP_MPLS_OBJECT_TYPE_TRANSPORT_TUNNEL,
	PP_MPLS_OBJECT_TYPE_L3VPN_TUNNEL,
	PP_MPLS_OBJECT_TYPE_PW,
	PP_MPLS_OBJECT_TYPE_RSVP_TUNNEL,
	PP_MPLS_OBJECT_TYPE_FEC_NODE,
	PP_MPLS_OBJECT_TYPE_INGRESS_TUNNEL,
	PP_MPLS_OBJECT_TYPE_L3VPN_INGRESS_TUNNEL,
	PP_MPLS_OBJECT_TYPE_RSVP_INGRESS_TUNNEL,
} pp_mpls_object_type_e;

typedef enum _pp_mpls_direction_e
{
	PP_MPLS_DIRECTION_NONE = 0,
	PP_MPLS_DIRECTION_INGRESS,
	PP_MPLS_DIRECTION_EGRESS,
	PP_MPLS_DIRECTION_BOTH,
} pp_mpls_direction_e;

typedef enum _pp_mpls_fec_node_op_e
{
	PP_MPLS_FEC_NODE_OP_NONE = 0,
	PP_MPLS_FEC_NODE_OP_ADD,
	PP_MPLS_FEC_NODE_OP_DEL,
	PP_MPLS_FEC_NODE_OP_INIT,
	PP_MPLS_FEC_NODE_OP_FREE,
	PP_MPLS_FEC_NODE_OP_RECOVER,
} pp_mpls_fec_node_op_e;

typedef struct _mpls_pw_id
{
	pw_type_e type;
	union
	{
		struct
		{
			/* Key parameters */
			network_ip_prefix_t fec;
			network_ip_address_t hexthop;
			uint32_t out_label;

			/* Replace parameters */
			uint32_t tunnel_id;
			uint32_t arp_encap_id;
			union
			{
				uint32_t egr_id;       // L3VPN egress tunnels
				uint8_t mac[ETH_ALEN]; // Transport/RSVP egress tunnels
			};
		} tunnel_egress;

		struct
		{
			uint8_t peer_id[LDP_ID_LENGTH];
		} ldp;

		struct
		{
			uint16_t  local_ve_id;
			uint16_t  remote_ve_id;
			uint8_t   local_rd[BGP_ID_VPN_ROUTE_DIST_LEN];
			uint8_t   remote_rd[BGP_ID_VPN_ROUTE_DIST_LEN];
		} bgp;
	};
	uint64_t vc_id;
} mpls_pw_id_t;

typedef union _mpls_command_key_u
{
	struct
	{
		uint32_t in_label;
	}sw_entry;

	struct
	{
		network_ip_prefix_t fec;
		uint32_t out_label;
		network_ip_address_t next_hop;
	}transport_egress_tunnel;

	struct
	{
		network_ip_prefix_t fec;
		uint32_t in_label;
	}transport_ingress_tunnel;

	struct
	{
		network_ip_prefix_t fec;
		uint32_t out_label;
		network_ip_address_t next_hop;
	}l3vpn_egress_tunnel;

	struct
	{
		uint32_t in_label;
		uint32_t vrf_index;
	}l3vpn_ingress_tunnel;

	struct
	{
		mpls_pw_id_t id;
	}pw;

	struct
	{
		uint32_t out_label;
		network_ip_address_t next_hop;
	}rsvp_egress_tunnel;

	struct
	{
		uint32_t in_label;
	}rsvp_ingress_tunnel;

	struct
	{
		network_ip_address_t fec;
		uint32_t label;
	}fec_node;
}mpls_command_key_u;

typedef struct _pp_mpls_replace_tunnel
{
	uint8_t is_l3vpn:1;
	network_ip_address_t tunnel_fec;
	uint32_t label;
	network_ip_address_t nexthop;
	uint32_t transport_label;
	int tunnel_id;
	int egress_id;
	uint32_t vrf_id;
} pp_mpls_replace_tunnel_t;

typedef union _mpls_command_ids_u
{
	struct
	{
	}sw_entry;

	struct
	{
		uint32_t tunnel_id;
		uint8_t mac[ETH_ALEN];
		uint32_t arp_encap_id;
	}transport_egress_tunnel;

	struct
	{
	}transport_ingress_tunnel;

	//L3VPN egress tunnels mean DB record and have not oper state ids
	struct
	{
	}l3vpn_egress_tunnel;

	struct
	{
	}l3vpn_ingress_tunnel;

	struct
	{
		uint32_t pw_id;
		uint32_t pw_encap_id;
	}pw;

	struct
	{
		uint32_t tunnel_id;
		uint8_t mac[ETH_ALEN];
		uint32_t arp_encap_id;
		int fec_id;
	}rsvp_egress_tunnel;

	struct
	{
	}rsvp_ingress_tunnel;

	struct
	{
		pp_mpls_fec_node_op_e op;
		pp_mpls_egress_info_type_e type;
		int ecmp_grp_id;
		pp_mpls_replace_tunnel_t replace_tunnels[MPLS_MAX_ACTIVE_TUNNELS];
		size_t replace_tunnels_cnt;
	}fec_node;
}mpls_command_ids_u;

typedef struct _pp_mpls_command_t
{
	pp_mpls_command_type_e command;
	int new_item;

	//Key info
	pp_mpls_object_type_e object;
	pp_mpls_direction_e direction;

	mpls_command_key_u key;

	//Data for set ids command
	mpls_command_ids_u ids;
} pp_mpls_command_t;

typedef struct _pp_mpls_command_request_t
{
	uint32_t count;
	pp_mpls_command_t commands[0];
}pp_mpls_command_request_t;

typedef struct _pp_arp_clear_req
{
	network_ip_address_t addr;
	uint32_t if_index;
	uint32_t vrf_id;
} pp_arp_clear_req_t;

typedef enum
{
	PP_PUNT_TYPE_TOTAL, /* Not really a punt type: this shapes/limits the total rate of all punt types */
	PP_PUNT_TYPE_OTHER,
	PP_PUNT_TYPE_IP_LOCAL,
	PP_PUNT_TYPE_IP_CONNECTED,
	PP_PUNT_TYPE_IP_MCAST,
	PP_PUNT_TYPE_IP_MCAST_PROTO,
	PP_PUNT_TYPE_IP_FRAG,
	PP_PUNT_TYPE_IP_TTL,
	PP_PUNT_TYPE_IP_OPTIONS,
	PP_PUNT_TYPE_NETFLOW,
	PP_PUNT_TYPE_MAC_LEARNING,

	PP_PUNT_TYPE_COUNT /* This item must be last */
} pp_punt_type_t;

static inline const char *pp_punt_type_stringize(pp_punt_type_t punt_type)
{
	switch (punt_type)
	{
		case PP_PUNT_TYPE_TOTAL:             return "PUNT_TYPE_TOTAL";
		case PP_PUNT_TYPE_OTHER:             return "PUNT_TYPE_OTHER";
		case PP_PUNT_TYPE_IP_LOCAL:          return "PUNT_TYPE_IP_LOCAL";
		case PP_PUNT_TYPE_IP_CONNECTED:      return "PUNT_TYPE_IP_CONNECTED";
		case PP_PUNT_TYPE_IP_MCAST:          return "PUNT_TYPE_IP_MCAST";
		case PP_PUNT_TYPE_IP_MCAST_PROTO:    return "PUNT_TYPE_IP_MCAST_PROTO";
		case PP_PUNT_TYPE_IP_FRAG:           return "PUNT_TYPE_IP_FRAG";
		case PP_PUNT_TYPE_IP_TTL:            return "PUNT_TYPE_IP_TTL";
		case PP_PUNT_TYPE_IP_OPTIONS:        return "PUNT_TYPE_IP_OPTIONS";
		case PP_PUNT_TYPE_NETFLOW:           return "PUNT_TYPE_NETFLOW";
		default:                             return "PUNT_TYPE_???";
	}
}

typedef struct
{
	uint32_t punt_shaper_kbps;
} pp_punt_shaper_params_t;

typedef struct
{
	pp_punt_shaper_params_t shaper_params; /* valid only if @use_default_params=0 */
	uint8_t use_default_params;
} pp_punt_shaper_t;

typedef struct
{
	pp_punt_shaper_t punt_type_to_shaper[PP_PUNT_TYPE_COUNT];
} pp_punt_shapers_t;

typedef struct
{
	uint32_t punt_type; /* pp_punt_type_t */
	uint8_t is_location_valid; /* if 0, the shaper is configured globally */
	pp_location_t location;
} pp_punt_shaper_id_t;

typedef struct
{
	pp_punt_shaper_id_t shaper_id;
	pp_punt_shaper_t shaper;
} pp_punt_shaper_set_one_t;

typedef struct
{
	uint32_t num_shapers;
	pp_punt_shaper_set_one_t shapers[0];
} pp_punt_shapers_set_request_t;

typedef struct
{
	uint8_t is_location_valid; /* if 0, get all locations */
	pp_location_t location;
} pp_punt_shapers_get_request_t;

typedef struct
{
	pp_punt_shaper_params_t default_params;
	pp_punt_shaper_params_t configured_params;
	pp_punt_shaper_params_t hw_params;
	uint64_t current_load_kbps;
	uint64_t current_load_pps;
} punt_type_to_shaper_param_t;

typedef struct
{
	pp_location_t location;

	/*
	 * Possible values of @rc:
	 * RCODE_OK:        @punt_type_to_shaper_params is valid.
	 * RCODE_NOT_FOUND: requested location not found.
	 * anything else:   error.
	 */
	int rc;

	/*
	 * The below members are valid only if @rc == RCODE_OK.
	 */
 	punt_type_to_shaper_param_t punt_type_to_shaper_params[PP_PUNT_TYPE_COUNT];
 	
} pp_punt_shapers_get_reply_location_t;

typedef struct
{
	uint32_t num_locations;
	pp_punt_shapers_get_reply_location_t locations[0];
} pp_punt_shapers_get_reply_t;

/*
 * We allocate 8 VOQs at once (one VOQ for every traffic class).
 */
#define FLOW_ID_BLOCK 8

/*
 * Valid TCs are from 0 to (PP_MAX_TC - 1).
 */
#define PP_MAX_TC 8
#define PP_QOS_QUEUE_INVALID 0xff
#define PP_QOS_CL_HANDLE_INVALID 0xffffffff

typedef struct
{
	uint32_t flow_id;
	uint32_t output_shaper_kbps; /* If 0, shaper is disabled */
	uint32_t output_burst_size;
	uint8_t classes_without_bw;
	uint8_t queues_allocated;
	pmap_class_t queue_pc[FLOW_ID_BLOCK];
	uint8_t tc_to_queue[PP_MAX_TC];
	uint8_t prio_queue;
	int8_t tc_to_pcp_cfi[PP_MAX_TC];
} pp_output_sch_config_t;

typedef enum
{
	PP_QOS_SCH_ACTION_NONE,
	PP_QOS_SCH_ACTION_CREATE,
	PP_QOS_SCH_ACTION_UPDATE,
	PP_QOS_SCH_ACTION_DELETE,
	PP_QOS_SCH_ACTION_RECREATE
} pp_sch_action_e;

static inline const char *pp_sch_action_stringize(pp_sch_action_e action)
{
	switch (action)
	{
		case PP_QOS_SCH_ACTION_NONE:        return "SCH_ACTION_NONE";
		case PP_QOS_SCH_ACTION_CREATE:      return "SCH_ACTION_CREATE";
		case PP_QOS_SCH_ACTION_RECREATE:    return "SCH_ACTION_RECREATE";
		case PP_QOS_SCH_ACTION_UPDATE:      return "SCH_ACTION_UPDATE";
		case PP_QOS_SCH_ACTION_DELETE:      return "SCH_ACTION_DELETE";
		default:                            return "SCH_ACTION_???";
	}
}

typedef struct
{
	uint32_t flow_id;
	uint32_t if_index;
	uint8_t  is_phys;
	uint32_t orig_egress_gport;
	uint8_t  egress_mod;
	uint32_t egress_port;
	pp_output_sch_config_t config;
	pp_sch_action_e action;

	/*
	 * Not valid in IPC_MSG_TYPE_PPMGR_OUTPUT_SCH_EGRESS_CREATE.
	 *
	 * Valid in a pp_output_sch_t.req IFF at least one of the following is true:
	 * - pol.ingress is valid,
	 * - pol.egress is valid,
	 * - pol.ctrl is valid and pol.ctrl.state != PP_OUTPUT_SCH_CTRL_STATE_EGRESS_CREATE_SENT.
	 */
#if defined(BOARD_BPE_PIZZABOX)
	uint32_t voq_connector;
#else
	uint32_t active_egress_mods[MAX_LC_BOARDS];
	uint32_t voq_connectors[MAX_LC_BOARDS][MAX_LC_BOARDS];
#endif
} pp_output_sch_request_t;

typedef struct
{
	int next_step;
	int cnt;
	pp_output_sch_request_t reqs[0];
} pp_output_sch_apply_req_t;

typedef struct
{
	int rc;
	pp_output_sch_request_t req;
} pp_output_sch_reply_t;

typedef struct
{
	int step;
	int cnt;
	pp_output_sch_reply_t rpls[0];
} pp_output_sch_apply_rpl_t;

typedef struct
{
	uint32_t profile_index;

	uint8_t dscp4_to_tc[256];
	uint8_t dscp6_to_tc[256];
	uint8_t exp_to_tc[8];
	uint8_t inner_vlan_cfi0_pcp_to_tc[8];
	uint8_t inner_vlan_cfi1_pcp_to_tc[8];
	uint8_t outer_vlan_cfi0_pcp_to_tc[8];
	uint8_t outer_vlan_cfi1_pcp_to_tc[8];
	int8_t  remark_dscp4[256];
	int8_t  remark_dscp6[256];
	int8_t  remark_exp[8];
	int8_t  remark_exp_to_dscp[8]; //stored as quarter of dscp
	int8_t  remark_outer_vlan_pcp_to_dscp4[2][8];    // remark_outer_vlan_pcp_to_dscp4[cfi][pcp]
	int8_t  remark_outer_vlan_pcp_to_dscp6[2][8];    // remark_outer_vlan_pcp_to_dscp6[cfi][pcp]
} cos_profile_t;

static inline void cos_profile_init(cos_profile_t *cos_profile)
{
	if (!cos_profile) return;

	memset(cos_profile, 0, sizeof(cos_profile_t));

	int dscp;
	for (dscp = 0; dscp < 256; ++dscp)
	{
		cos_profile->remark_dscp4[dscp] = -1;
		cos_profile->remark_dscp6[dscp] = -1;
	}

	int pcp;
	for (pcp = 0; pcp < 8; ++pcp)
	{
		int cfi;
		for (cfi = 0; cfi < 2; ++cfi)
		{
			cos_profile->remark_outer_vlan_pcp_to_dscp4[cfi][pcp] = -1;
			cos_profile->remark_outer_vlan_pcp_to_dscp6[cfi][pcp] = -1;
		}
	}

	int exp;
	for (exp = 0; exp < 8; ++exp)
	{
		cos_profile->remark_exp[exp] = -1;
		cos_profile->remark_exp_to_dscp[exp] = -1;
	}
}

#define QOS_REMARK_DSCP_DSCP4_6 0x1
#define QOS_REMARK_DSCP_EXP     0x2
#define QOS_REMARK_EXP_EXP      0x4
#define QOS_REMARK_PCP_EXP      0x8
#define QOS_REMARK_EXP_DSCP     0x10

#define QOS_REMARK_FLAG_SET(flags,flag) (flags |= flag)
#define QOS_REMARK_FLAG_UNSET(flags,flag) (flags &= ~flag)
#define QOS_REMARK_FLAG_IS_SET(flags,flag) (flags & flag)

typedef struct
{
	uint32_t profile_index;
	uint16_t flags; //QOS_REMARK_XXX

	uint8_t dscp_to_dscp4[256];
	uint8_t dscp_to_dscp6[256];
	uint8_t dscp4_to_exp[256];
	uint8_t dscp6_to_exp[256];
	uint8_t pcp_to_exp[8];
	uint8_t exp_to_exp[8];

	uint8_t exp_to_dscp[8];
} qos_remark_profile_t;

typedef struct
{
	uint8_t is_port;
	union
	{
		struct
		{
			uint8_t        dev;
			uint8_t        unit;
			uint32_t       number;
			uint32_t       sub_id;
		} port;

		struct
		{
			uint32_t       id;
			uint32_t       sub_id;
		} lag;
	};
} pp_qos_iface_desc_t;

#define PP_SHAPE_OUTPUT_BURST_SIZE_DEFAULT 131072

typedef struct _pp_shape_config pp_shape_config_t;
typedef struct _pp_rate_limit_config pp_rate_limit_config_t;

typedef struct
{
	pp_qos_iface_desc_t iface;

	pp_shape_config_t output_shape;
	pp_shape_config_t input_shape;

	pp_rate_limit_config_t rate_limit;

	char svc_plc_in[IF_SVC_PLC_NAME_LEN];
	char svc_plc_out[IF_SVC_PLC_NAME_LEN];
} pp_qos_svc_plc_iface_cfg_t;

typedef struct
{
	uint32_t diffs_cnt;
	pp_qos_svc_plc_iface_cfg_t diffs[0];
} pp_qos_svc_plc_cfg_t;

typedef struct
{
	uint32_t if_index;
	int rate;
} pp_qos_rate_limit_request_t;

typedef struct
{
	int count;
	pp_qos_rate_limit_request_t reqs[0];
} pp_qos_rate_limit_apply_req_t;

#define PP_QOS_OPER_IP_ADDR_ADD      0x1
#define PP_QOS_OPER_IP_ADDR_DEL      0x2

typedef struct
{
	uint8_t operation;
	pp_qos_iface_desc_t iface;
} pp_qos_ip_iface_cfg_t;

typedef struct
{
	uint32_t diffs_cnt;
	pp_qos_ip_iface_cfg_t diffs[0];
} pp_qos_ip_cfg_t;

typedef struct
{
	uint8_t is_add;
	pp_qos_iface_desc_t iface;
} pp_qos_bridge_membr_cfg_t;

typedef struct
{
	uint32_t diffs_cnt;
	pp_qos_bridge_membr_cfg_t diffs[0];
} pp_qos_bridges_cfg_t;

typedef struct
{
	uint8_t is_add;
	uint32_t lag_id;
	struct
	{
		uint8_t        dev;
		uint8_t        unit;
		uint32_t       number;
	} port;
} pp_qos_lag_membr_cfg_t;

typedef struct
{
	uint32_t diffs_cnt;
	pp_qos_lag_membr_cfg_t diffs[0];
} pp_qos_lag_cfg_t;

#if 0
typedef enum 
{
	PP_QOS_ID_STATE_ALLOCATED,
	PP_QOS_ID_STATE_NEED_ALLOC,
	PP_QOS_ID_STATE_NEED_DEALLOC,
	PP_QOS_ID_STATE_NOT_ALLOCATED
} pp_qos_id_state_e;

typedef struct 
{
	pp_qos_id_state_e state;
	uint32_t id;
} pp_qos_id_t;

typedef struct 
{
	uint8_t is_del;
	uint8_t id_type;
	pp_qos_iface_desc_t iface_desc;
	union 
	{
		struct 
		{
			pp_qos_id_t flow_id;
			uint32_t output_shaper_kbps;
			char svc_plc_out[IF_SVC_PLC_NAME_LEN];
		} flow;

		struct 
		{
			pp_qos_id_t voq_cnctr[MAX_LC_BOARDS][MAX_LC_BOARDS];
		} voq;		
	};
} pp_qos_ids_to_slave_t;
#endif

typedef struct
{
	char name[SHAPE_PROFILE_NAME_LEN];
	uint32_t rate;
	uint32_t burst;
} pp_qos_shape_profile_t;

typedef struct
{
	char name[RATE_LIMIT_PROFILE_NAME_LEN];
	uint32_t rate;
} pp_qos_rate_limit_profile_t;

//------------------------------------------
#define BD_STORM_CTRL_PROFILE_NAME_LEN 64
typedef enum
{
	BD_STORM_CTRL_INVALID,
	BD_STORM_CTRL_PROFILE,
	BD_STORM_CTRL_DIRECT,
} pp_bd_storm_ctrl_cfg_type_t;

typedef enum
{
	BD_STORM_CTRL_LIMIT_INVALID,
	BD_STORM_CTRL_LIMIT_KBPS,
} pp_bd_storm_ctrl_limit_type_t;

typedef enum
{
	BD_STORM_CTRL_REQ_BD,
	BD_STORM_CTRL_REQ_IF,
	BD_STORM_CTRL_REQ_BD_NLS,
} pp_bd_storm_ctrl_req_type_t;

typedef struct
{
	pp_bd_storm_ctrl_limit_type_t type;
	uint32_t limit;
	uint32_t burst;
} pp_bd_storm_ctrl_limit_t;

typedef struct
{
	pp_bd_storm_ctrl_cfg_type_t type;
	union
	{
		char profile[BD_STORM_CTRL_PROFILE_NAME_LEN];
		struct
		{
			pp_bd_storm_ctrl_limit_t bcast;
			pp_bd_storm_ctrl_limit_t mcast;
			pp_bd_storm_ctrl_limit_t uucast;
		};
	};
} pp_bd_storm_ctrl_cfg_t;

typedef struct
{
	uint32_t vport;
	uint32_t vsi;
} pp_bd_storm_ctrl_req_vals_t;

typedef struct __attribute__((packed))
{
	pp_bd_storm_ctrl_req_type_t type;
	uint32_t bd;
	pp_qos_iface_desc_t iface;

	pp_bd_storm_ctrl_cfg_t cfg;
	pp_bd_storm_ctrl_req_vals_t vals;
} pp_bd_storm_ctrl_req_t;

typedef struct __attribute__((packed))
{
	char name[BD_STORM_CTRL_PROFILE_NAME_LEN];
	pp_bd_storm_ctrl_limit_t bcast;
	pp_bd_storm_ctrl_limit_t mcast;
	pp_bd_storm_ctrl_limit_t uucast;
} pp_bd_storm_ctrl_profile_t;
//------------------------------------------
#if defined(BOARD_BPE_CTRLCARD)
typedef struct _pp_lc_stage_state
{
	int lc_num;
	int stage;
	int state;
} pp_lc_stage_state_t;
#endif

typedef struct
{
	uint8_t  install_allowed;
	uint64_t installed_routes;
	uint64_t pending_routes;
	uint64_t dropped_routes;
	uint64_t max_hw_routes;
} pp_hw_routes_counter_t;

typedef struct
{
	int      slot;
	uint8_t  is_present;
	pp_hw_routes_counter_t ipv4;
	pp_hw_routes_counter_t ipv6;
} pp_hw_routes_counter_panel_t;

typedef struct
{
	uint32_t num_panels;
	pp_hw_routes_counter_panel_t panel[0];
} pp_hw_routes_counters_t;

/********************************************************************/
/* BFD-Stub send request to pp-manager with NEXTHOP-ADDRESS and VRF */
/* pp-manager send reply with LOCAL-ADDRESS and VRF                 */
/********************************************************************/
typedef struct
{
	network_ip_address_t address;
	uint32_t vrf_id;
} pp_address_t;

#define PP_MC_ROUTE_AVERAGE_MEMBERS_NUM 32

typedef struct
{
	uint32_t vrf_id;
	int is_ip6;
} pp_ft_routes_clear_params_t;

typedef enum
{
	PP_GR_CONFIGURED_LDP,
	PP_GR_CONFIGURED_BGP,
	PP_GR_CONFIGURED_ISIS,
	PP_GR_CONFIGURED_OSPF,
	PP_GR_CONFIGURED_RSVP,
} pp_gr_configured_e;

typedef struct
{
	pp_gr_configured_e type;
	uint8_t enabled;
} pp_gr_configured_t;

typedef struct
{
	uint8_t enabled;
} pp_l3vpn_ecmp_configured_t;

typedef enum
{
	HW_MODULES_MAC_LIMIT_REQ,
	HW_MODULES_ALL_REQ,
} ipc_hw_modules_req_t;

typedef struct
{
	uint8_t unit;
	uint8_t slot;
	uint8_t all_location;
	ipc_hw_modules_req_t req_type;
} ipc_hw_modules_location_req_t;

typedef struct
{
	uint8_t unit;
	uint8_t slot;
	uint8_t mac_limit_flag;
	uint8_t is_present;
} ipc_hw_modules_answ_t;

typedef struct
{
	uint32_t num_locations;
	ipc_hw_modules_answ_t location[0];
} ipc_hw_modules_location_answ_t;

typedef enum
{
	HW_PARAM_MAX_IPV4_FLOWS,
	HW_PARAM_MAX_IPV4_ACL,
	HW_PARAM_MAX_IPV6_FLOWS,
	HW_PARAM_MAX_IPV6_ACL,
	HW_PARAM_MAX_LSP_TUNNELS,
	HW_PARAM_MAX_PW_TUNNELS,
	HW_PARAM_MAX_IPV4_ROUTES,
	HW_PARAM_MAX_IPV6_ROUTES,
	HW_PARAM_MAX_ARP,
	HW_PARAM_MAX_ACL_PROTOS,

	//Must be last
	HW_PARAM_COUNT,
} ipc_hw_max_parameters_t;

typedef struct
{
	ipc_hw_max_parameters_t id;
	uint32_t current;
	uint32_t hw_configured;
	uint32_t hw_capacity;
} ipc_hw_max_param_info_t;

typedef struct
{
	ipc_hw_max_param_info_t param[HW_PARAM_COUNT];
} ipc_hw_max_info_t;

typedef struct
{
	uint8_t in_port;
	uint8_t polynomial;
	uint8_t shift;
	uint32_t seed;
} ipc_load_balancing_lag_ecmp_cfg_t;

typedef struct
{
	ipc_load_balancing_lag_ecmp_cfg_t lag;
	ipc_load_balancing_lag_ecmp_cfg_t ecmp;
} ipc_load_balancing_lag_ecmp_t;

typedef enum _pp_arp_record_type {
	PP_ARP_RECORD_INVALID,
	PP_ARP_RECORD_STATIC,
	PP_ARP_RECORD_DYNAMIC,
	PP_ARP_RECORD_IFACE      // Shows local address(no edging, no arp requests sending)
} pp_arp_record_type_e;

typedef enum _pp_ndp_record_state_e {
	PP_NDP_STATE_INCOMPLETE,
	PP_NDP_STATE_REACHABLE,
	PP_NDP_STATE_STALE,
	PP_NDP_STATE_DELAY,
	PP_NDP_STATE_PROBE,
	PP_NDP_STATE_INTERFACE  // For local addresses
} pp_ndp_record_state_e;

typedef struct
{
	network_ip_address_t addr;
	network_mac_address_t mac;
	uint32_t vrf_index;
	uint8_t delete_flag;
} pp_static_arp_entry_t;

typedef struct
{
	uint32_t entry_count;
	pp_static_arp_entry_t entries[0];
} pp_static_arp_request_t;

typedef struct
{
	network_ip_prefix_t dst_pfx;
	uint32_t vrf_id;
} pp_route_identifier_t;

typedef struct
{
	uint32_t entry_num;
	pp_route_identifier_t entries[0];
} pp_invalidate_routes_request_t;

#define MAX_ARP_AGING_TIME 240  // minutes

#if !defined(BOARD_BPE_LINECARD)
typedef struct
{
	int8_t rc;
	uint32_t otype;
	uint32_t itype;
	uint8_t  egress_rewrited;
	uint8_t  ingress_rewrited;
} pp_if_encap_state_t;
#endif

#endif /* _SYSTEM_PP_TYPES_H_INCLUDED_ */
