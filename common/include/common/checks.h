#ifndef _CHECKS_H_INCLUDED_
#define _CHECKS_H_INCLUDED_

#include <unistd.h>
#include <log/log.h>

#ifndef IS_NE
#define IS_NE(x, value) \
	(((x) != (value)) ? \
	    log_err(#x " is not equal " #value " (%d != %d)", (int)(x), (int)(value)),1 : 0)
#endif

/*
 * Formatted output
 */
#ifndef IS_NE_fmt
#define IS_NE_fmt(x, value, fmt) \
	(((x) != (value) ) ? \
	    log_err(#x " is not equal " #value " (" #fmt " != " #fmt ")", (x), (value)),1 : 0)
#endif

/*
 * Long int
 */
#ifndef IS_NE_l
#define IS_NE_l(x, value) \
	IS_NE_fmt(x, value, %ld)
#endif

#ifndef IS_EQ
#define IS_EQ(x, value) \
	(((x) == (value)) ? \
	    log_err(#x " is equal " #value " (%d == %d)", (int)(x), (int)(value)),1 : 0)
#endif

#ifndef IS_MT
#define IS_MT(x, value) \
	(((x) > (value)) ? \
	    log_err(#x " is more than " #value " (%d > %d)", (int)(x), (int)(value)),1 : 0)
#endif

#ifndef IS_ME
#define IS_ME(x, value) \
	(((x) >= (value)) ? \
	    log_err(#x " is more than or equal " #value " (%d >= %d)", (int)(x), (int)(value)),1 : 0)
#endif

#ifndef IS_LT
#define IS_LT(x, value) \
	(((x) < (value)) ? \
	    log_err(#x " is less than " #value " (%d < %d)", (int)(x), (int)(value)),1 : 0)
#endif

#ifndef IS_LE
#define IS_LE(x, value) \
	(((x) <= (value)) ? \
	    log_err(#x " is less than or equal " #value " (%d <= %d)", (int)(x), (int)(value)),1 : 0)
#endif

#ifndef OUT_OF_RANGE
#define OUT_OF_RANGE(x, low_border, high_border) \
	(((x) < (low_border) || (x) > (high_border)) ? \
	    log_err(#x "(%d) is out of range [" #low_border "(%d):" #high_border "(%d)]", (int)(x), (int)(low_border), (int)(high_border)),1 : 0)
#endif

#ifndef NDEBUG
# define ASSERT(x) \
	do { \
		if (!(x)) { \
			log_err("ASSERT in function '%s', line %d, '%s'", __func__, __LINE__, #x); \
			_exit(0); \
		} \
	} while (0)
#else
# define ASSERT(x) ((void)0)
#endif

#ifndef PTR_IS_NULL
#define PTR_IS_NULL(ptr) \
	(((ptr) == NULL) ? \
	    log_err("Pointer " #ptr " is NULL"),1 : 0)
#endif

#ifndef PTR_NOT_NULL
#define PTR_NOT_NULL(ptr) \
	(((ptr) != NULL) ? \
	    log_err("Pointer " #ptr " is not NULL"),1 : 0)
#endif

#ifndef PTR_IS_EQ
#define PTR_IS_EQ(ptr, value) \
	(((ptr) == (value)) ? \
	    log_err("Pointer " #ptr " is equal " #value " (%p == %p)", (ptr), (value)),1 : 0)
#endif

#ifndef PTR_IS_NE
#define PTR_IS_NE(ptr, value) \
	(((ptr) != (value)) ? \
	    log_err("Pointer " #ptr " is not equal " #value " (%p != %p)", (ptr), (value)),1 : 0)
#endif

#ifndef CHK_INTRC
#define CHK_INTRC(func, ok_value) \
( { \
	int rc = (func); \
	((ok_value) != rc) ? \
		log_dbg(#func " returned not " #ok_value " (%d != %d)", (int)rc, (int)(ok_value)),1 : 0; \
} )
#endif

#endif
