#ifndef __VRF_H__
#define __VRF_H__

#include <stdint.h>

#define VRF_NAME_LEN     32

#define MGMT_VRF_NAME "mgmt-intf"
#define DEFAULT_VRF "default-ns"

/* VRF default value must be equal to 0! */
#define VRF_DEFAULT 0
#define VRF_INVALID 0xFFFFFFFF

typedef enum _vrf_serv_type {
	SERVICE_TYPE,
	NBASE_TYPE,
	INTERFACE_TYPE,
	RM_TYPE,
	EMPTY_TYPE,
} vrf_serv_type_e;

typedef struct
{
	char vrf_name[VRF_NAME_LEN];
	vrf_serv_type_e type;
	uint32_t ref_count; //used whith if_mgr_free_vrf and if_mgr_use_vrf commands
} ipc_msg_get_vrf_id_req_t;

typedef struct
{
	uint32_t vrf_index;
	vrf_serv_type_e type;
	uint32_t ref_count; //used whith if_mgr_free_vrf and if_mgr_use_vrf commands
} ipc_msg_free_vrf_by_id_req_t;

typedef struct
{
	int rc;
	uint32_t vrf_index;
	vrf_serv_type_e type;
} ipc_msg_get_vrf_id_resp_t;

typedef struct
{
	uint32_t vrf_index;
	vrf_serv_type_e type;
} ipc_msg_get_vrf_name_req_t;

typedef struct
{
	char vrf_name[VRF_NAME_LEN];
	vrf_serv_type_e type;
	uint32_t vrf_index;
} ipc_msg_get_vrf_name_resp_t;

typedef enum
{
	VRF_CREATE = 0,
	VRF_DELETE = 1
} vrf_pair_action_t;

typedef struct
{
	char vrf_name[64];
	uint32_t vrf_id;
	vrf_pair_action_t action;
} vrf_pair_t;

typedef struct
{
	char vrf_name[VRF_NAME_LEN];
	uint32_t vrf_id;
} sh_vrf_db_t;

#endif /* __VRF_H__ */
