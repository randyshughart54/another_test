#ifndef __COMMON_IFACE_NAMES_H_INCLUDED__
#define __COMMON_IFACE_NAMES_H_INCLUDED__

#include <stdio.h>
#include <stdint.h>

#include <common/errors.h>
#include <common/checks.h>
#include <if-manager/interface.h>
#include "if-types.h"

#define IFACE_NAME_TMPL      "i%u"
#define IFACE_OOB_NAME_TMPL  "eth%u"
#define TMP_IFACE_NAME_PFX   "tmp-i"
#define TMP_IFACE_NAME_TMPL  TMP_IFACE_NAME_PFX"%u.%u"
#define IFACE_NAMESPACE_TMPL "%u"
#define MAX_IFACE_NAME_LEN      32

inline static int iface_desc_stringize(iface_desc_t *desc, char *str, size_t size)
{
	if (PTR_IS_NULL(desc)) return -1;
	if (PTR_IS_NULL(str))  return -1;

	switch (desc->iface_type)
	{
		case IFACE_TYPE_PORT:
		{
			switch (desc->port.log_port.type)
			{
				case PORT_TYPE_GE:
				{
					snprintf(str, size, "gi %d/%d/%d",
					         desc->port.unit,
					         desc->port.dev,
					         desc->port.log_port.port);
					break;
				}
				case PORT_TYPE_TE:
				{
					snprintf(str, size, "te %d/%d/%d",
					         desc->port.unit,
					         desc->port.dev,
					         desc->port.log_port.port);
					break;
				}
				case PORT_TYPE_FE:
				{
					snprintf(str, size, "fo %d/%d/%d",
					         desc->port.unit,
					         desc->port.dev,
					         desc->port.log_port.port);
					break;
				}
				case PORT_TYPE_HE:
				{
					snprintf(str, size, "hu %d/%d/%d",
					         desc->port.unit,
					         desc->port.dev,
					         desc->port.log_port.port);
					break;
				}
				case PORT_TYPE_UNKNOWN:
				default:
				{
					printf("<%s> unsupported port type: %d\n", __FUNCTION__, desc->port.log_port.type);
					return -1;
				}
			}
			break;
		}
		case IFACE_TYPE_OOB:
		{
			snprintf(str, size, "mgmt %d/fmc%d/%d",
			         desc->oob_port.unit,
			         desc->oob_port.dev,
			         desc->oob_port.number);
			break;
		}
		case IFACE_TYPE_LAG:
		{
			snprintf(str, size, "bu %d", desc->lag.id);
			break;
		}
		case IFACE_TYPE_SUB:
		{
			switch (desc->sub.parent_type)
			{
				case IFACE_TYPE_PORT:
				{
					switch (desc->sub.parent.port.log_port.type)
					{
						case PORT_TYPE_GE:
						{
							snprintf(str, size, "gi %d/%d/%d.%d",
							         desc->sub.parent.port.unit,
							         desc->sub.parent.port.dev,
							         desc->sub.parent.port.log_port.port,
							         desc->sub.number);
							break;
						}
						case PORT_TYPE_TE:
						{
							snprintf(str, size, "te %d/%d/%d.%d",
							         desc->sub.parent.port.unit,
							         desc->sub.parent.port.dev,
							         desc->sub.parent.port.log_port.port,
							         desc->sub.number);
							break;
						}
						case PORT_TYPE_FE:
						{
							snprintf(str, size, "fo %d/%d/%d.%d",
							         desc->sub.parent.port.unit,
							         desc->sub.parent.port.dev,
							         desc->sub.parent.port.log_port.port,
							         desc->sub.number);
							break;
						}
						case PORT_TYPE_HE:
						{
							snprintf(str, size, "hu %d/%d/%d.%d",
							         desc->sub.parent.port.unit,
							         desc->sub.parent.port.dev,
							         desc->sub.parent.port.log_port.port,
							         desc->sub.number);
							break;
						}
						case PORT_TYPE_UNKNOWN:
						default:
						{
							printf("<%s> unsupported sub parent port type: %d\n",
							       __FUNCTION__, desc->sub.parent.port.log_port.type);
							return -1;
						}
					}
					break;
				}
				case IFACE_TYPE_LAG:
				{
					snprintf(str, size, "bu %d.%d", desc->sub.parent.lag.id, desc->sub.number);
					break;
				}
				default:
				{
					printf("<%s> unsupported parent type: %d\n", __FUNCTION__, desc->sub.parent_type);
					return -1;
				}
			}
			break;
		}
		case IFACE_TYPE_TUNNEL:
		{
			switch (desc->tunnel.type)
			{
				case TUNNEL_TYPE_MPLS:
				{
					snprintf(str, size, "tun-mpls");
					break;
				}
				case TUNNEL_TYPE_MPLS_OS:
				{
					snprintf(str, size, "tun-mpls-os");
					break;
				}
				case TUNNEL_TYPE_PW:
				{
					snprintf(str, size, "tun-pw %d", desc->tunnel.pw.pw_vc_index);
					break;
				}
				case TUNNEL_TYPE_IP:
				{
					snprintf(str, size, "tun-ip");
					break;
				}
				case TUNNEL_TYPE_PW_GROUP:
				{
					snprintf(str, size, "tun-pw-grp");
					break;
				}
				case TUNNEL_TYPE_PIM_ENCAPS:
				{
					snprintf(str, size, "tun-pim-encaps");
					break;
				}
				case TUNNEL_TYPE_PIM_DECAPS:
				{
					snprintf(str, size, "tun-pim-decaps");
					break;
				}
				case TUNNEL_TYPE_L3VPN:
				{
					snprintf(str, size, "tun-l3vpn");
					break;
				}
				case TUNNEL_TYPE_RSVP:
				{
					snprintf(str, size, "tun-rsvp");
					break;
				}
				case TUNNEL_TYPE_UNKNOWN:
				default:
				{
					printf("<%s> unsupported tunnel type: %d\n", __FUNCTION__, desc->tunnel.type);
					return -1;
				}
			}
			break;
		}
		case IFACE_TYPE_LOOPBACK:
		{
			snprintf(str, size, "lo %d", desc->lo.number);
			break;
		}
		case IFACE_TYPE_VIRTUAL:
		{
			snprintf(str, size, "vif %d(%d)", desc->iface_idx, desc->virt.parent_if_index);
			break;
		}
		case IFACE_TYPE_UNKNOWN:
		default:
		{
			printf("<%s> unsupported iface type: %d\n", __FUNCTION__, desc->iface_type);
			return -1;
		}
	}

	return 0;
}

inline static
void iface_name_stringize(uint32_t if_index, char *buf, size_t buf_size)
{
	if (PTR_IS_NULL(buf)) return;

	snprintf(buf, buf_size, IFACE_NAME_TMPL, (unsigned int)if_index);
}

inline static
int iface_oob_name_stringize(uint32_t port, char *buf, size_t buf_size)
{
	if (PTR_IS_NULL(buf)) return -1;

#if defined(BOARD_BPE_PIZZABOX)
	if(port != 1)
		return -1;

	snprintf(buf, buf_size, IFACE_OOB_NAME_TMPL, (unsigned int)4);
	return 0;
#endif
#if defined(BOARD_BPE_CTRLCARD)
	if(port < 1 || port > 2)
		return -1;

	snprintf(buf, buf_size, IFACE_OOB_NAME_TMPL, (unsigned int)port);
#endif
	return 0;
}

inline static
int iface_name_parse(const char *ifname, uint32_t *if_index)
{
	if (PTR_IS_NULL(ifname)) return ERR_BAD_PTR;
	if (PTR_IS_NULL(if_index)) return ERR_BAD_PTR;

	unsigned int if_id;
	int parsed_items;

	parsed_items = sscanf(ifname, IFACE_NAME_TMPL, &if_id);
	if (!parsed_items)
		return ERR_FAIL;

	*if_index = if_id;

	return ERR_NONE;
}

inline static
void iface_name_tmp_stringize(
		uint32_t parent_if_index,
		uint32_t outer_vid,
		char *buf,
		size_t buf_size)
{
	if (PTR_IS_NULL(buf)) return;

	snprintf(buf, buf_size, TMP_IFACE_NAME_TMPL,
	         (unsigned int)parent_if_index, (unsigned int)outer_vid);
}

inline static
int iface_name_tmp_parse(
		const char *tmp_ifname,
		uint32_t *parent_if_index,
		uint32_t *outer_vid)
{
	if (PTR_IS_NULL(tmp_ifname)) return ERR_BAD_PTR;
	if (PTR_IS_NULL(parent_if_index)) return ERR_BAD_PTR;
	if (PTR_IS_NULL(outer_vid)) return ERR_BAD_PTR;

	unsigned int vid;
	unsigned int if_id;
	int parsed_items;

	parsed_items = sscanf(tmp_ifname, TMP_IFACE_NAME_TMPL, &if_id, &vid);
	if (IS_NE(parsed_items, 2))
		return ERR_FAIL;

	*parent_if_index = if_id;
	*outer_vid = vid;

	return ERR_NONE;
}

inline static
int iface_name_is_tmp(const char *ifname)
{
	if (PTR_IS_NULL(ifname)) return 0;

	if (strstr(ifname, TMP_IFACE_NAME_PFX) != NULL)
		return 1;

	return 0;
}

inline static
void iface_namespace_stringize(uint32_t vrf, char *buf, size_t buf_size)
{
	if (PTR_IS_NULL(buf)) return;

	memset(buf, 0, buf_size);

	if (vrf != VRF_DEFAULT)
		snprintf(buf, buf_size, IFACE_NAMESPACE_TMPL, (unsigned int)vrf);
}

inline static
int iface_namespace_parse(const char *ns, uint32_t *vrf)
{
	if (PTR_IS_NULL(ns)) return ERR_BAD_PTR;
	if (PTR_IS_NULL(vrf)) return ERR_BAD_PTR;

	if (strlen(ns) == 0)
	{
		*vrf = VRF_DEFAULT;
		return ERR_NONE;
	}

	unsigned int vrf_id;
	int parsed_items;

	parsed_items = sscanf(ns, IFACE_NAMESPACE_TMPL, &vrf_id);
	if (IS_NE(parsed_items, 1))
		return ERR_FAIL;

	*vrf = vrf_id;

	return ERR_NONE;
}

inline static const char * iface_port_type_to_string(port_type_e type)
{
	switch (type)
	{
		case PORT_TYPE_UNKNOWN:
		default:
			return "unknown";
		case PORT_TYPE_GE:
			return "ge";
		case PORT_TYPE_TE:
			return "te";
		case PORT_TYPE_FE:
			return "fo";
		case PORT_TYPE_HE:
			return "hu";
	}
	return "unknown";
}

#endif
