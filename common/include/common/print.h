#ifndef __PRINT_H__
#define __PRINT_H__

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <syslog.h>

#define GENERATE_STR(TC, __file__, ...) \
	do { \
		fprintf(__file__, "%*s", (TC) * 4, ""); \
		fprintf(__file__, __VA_ARGS__); \
		fprintf(__file__, "\n"); \
		if (0 != ferror(__file__)) { \
			syslog(LOG_ERR, "<%s> !!!! TRUNCATED output, errno = %d(%s) !!!!", \
			       __FUNCTION__, errno, strerror(errno)); \
			syslog(LOG_ERR, __VA_ARGS__); \
		} \
	} while (0)

#define CREATE_STR(TC, __file__, ...) \
	do { \
		fprintf(__file__, "%*s", (TC) * 4, ""); \
		fprintf(__file__, __VA_ARGS__); \
		if (0 != ferror(__file__)) { \
			syslog(LOG_ERR, "<%s> !!!! TRUNCATED output, errno = %d(%s) !!!!", \
			       __FUNCTION__, errno, strerror(errno)); \
			syslog(LOG_ERR, __VA_ARGS__); \
		} \
	} while (0)

#define ADD_TO_STR(__file__, ...) \
	do { \
		fprintf(__file__, __VA_ARGS__); \
		if (0 != ferror(__file__)) { \
			syslog(LOG_ERR, "<%s> !!!! TRUNCATED output, errno = %d(%s) !!!!", \
			       __FUNCTION__, errno, strerror(errno)); \
			syslog(LOG_ERR, __VA_ARGS__); \
		} \
	} while (0)

#endif