#ifndef __SHELF_TYPES_H__
#define __SHELF_TYPES_H__

#include <stdint.h>
#include <unistd.h>
#include <sys/time.h>

typedef struct
{
	size_t master_idx;
} unit_role_info_t;

#if !defined(BOARD_BPE_PIZZABOX)
#	define APP_MAX_UNITS_PER_STACK	2
#else
#	define APP_MAX_UNITS_PER_STACK	1
#endif

typedef enum
{
	UID_LEFT = 0,
	UID_RIGHT = 1
} unit_id_t;

typedef enum
{
	SLOT_0 = 0,
	SLOT_1 = 1,
	SLOT_2 = 2,
	SLOT_3 = 3,
	SLOT_4 = 4,
	SLOT_5 = 5,
	SLOT_6 = 6,
	SLOT_7 = 7,
	SLOT_8 = 8,
	SLOT_9 = 9,
	SLOT_10 = 10,
	SLOT_11 = 11
} slot_id_t;

#define APP_STACK_INVALID_UNIT_ID (-1)

enum // IPC HW ID
{
	HWID_LEFT = 0,
	HWID_RIGHT = 1
};

enum
{
	UNIT_PRIO_HIGH = 100,
	UNIT_PRIO_LOW = 1,
};

typedef struct
{
	unit_id_t id;
	size_t idx;
	char name[64];
} unit_t;

typedef struct
{
	uint32_t is_online;
	unit_id_t id;
	uint8_t in_maintenance_mode;
} stack_unit_info_t;

typedef struct
{
	stack_unit_info_t unit_info[APP_MAX_UNITS_PER_STACK];
} shelf_stack_info_t;

#if !defined(BOARD_BPE_PIZZABOX)
void dump_unit_role_info(const unit_role_info_t* ri, char buf[], size_t size);
#endif
void dump_unit_role_by_idx(const unit_role_info_t* ri, size_t unit_idx, char buf[], size_t size);

void dump_unit(const unit_t* unit, char buf[], size_t size);

void dump_stack_info(const shelf_stack_info_t* si, char buf[], size_t size);

#if defined(BOARD_BPE_CTRLCARD) || defined(BOARD_BPE_LINECARD)
#define MAX_LC_BOARDS 12
#else
#define MAX_LC_BOARDS 0
#endif

#define SHOW_FLD_UNIT	9
#define SHOW_FLD_ROLE	11

typedef struct
{
	struct
	{
		char name[SHOW_FLD_UNIT];
		char role[SHOW_FLD_ROLE];
	} unit[APP_MAX_UNITS_PER_STACK];
} show_redundancy_t;

#define MAX_FMC_LC_TOPOLOGY_SIZE	(APP_MAX_UNITS_PER_STACK + MAX_LC_BOARDS)

#if !defined(BOARD_BPE_PIZZABOX)
typedef struct
{
	uint32_t mask;
	size_t master_idx;
} topology_t;

typedef struct
{
	unit_id_t unit_id;
} fw_upgrade_required_t;

typedef struct
{
	unit_id_t unit_id;
	uint32_t allow_autoupgrade;
} maintenance_mode_t;

typedef struct {
	uint32_t svc_addr[128];
} svc_info_table_t;
#endif

typedef struct
{
	pid_t pid;
	uint32_t svc_addr; // ipc_addr_t
	uint32_t is_started;
} svc_info_t;

typedef struct
{
	uint32_t svc_addr; // ipc_addr_t
	uint32_t is_busy;
} svc_task_status_t;

typedef struct
{
	struct
	{
		uint8_t is_present;
		struct timeval diff_time;
	} lc[MAX_LC_BOARDS];
	struct
	{
		uint8_t is_present;
		struct timeval diff_time;
	} fmc;
} fmc_diff_time_t;

typedef enum
{
	SLOT_STATE_NOT_PRESENT = 0,
	SLOT_STATE_PORT_DETECTED,
	SLOT_STATE_PORT_LOST,
	SLOT_STATE_SYSTEM_DETECTED,
	SLOT_STATE_SYNC_IN_PROGRESS,
	SLOT_STATE_SYNC_DONE
} slot_state_e;

typedef struct
{
	int lc_state[MAX_LC_BOARDS];
	int nbr_fmc_state;
} slot_state_t;

typedef struct
{
	int hw_type;
	int idx;
	int state;
} slot_state_notif_t;

#endif /* __SHELF_TYPES_H__ */
