#ifndef __NET_ADDR_UTILS_H__
#define __NET_ADDR_UTILS_H__

#include <netinet/in.h>

inline void get_v4_mask_addr_by_maks_bits(uint8_t mask, struct in_addr *mask_addr)
{
	int i, bytes, bits;

	bytes = mask >> 3;
	for (i = 0; i < bytes; i++)
		((uint8_t*)(&mask_addr->s_addr))[i] = 0xFF;

	if (bytes < 4)
	{
		bits = 8 - (mask & 0x7);
		((uint8_t*)(&mask_addr->s_addr))[bytes] = 0xFF - ((1 << bits) - 1);
	}

	for (i = bytes + 1; i < 4; i++)
		((uint8_t*)(&mask_addr->s_addr))[i] = 0;
}

inline void get_v4_subnet(struct in_addr *ip, struct in_addr *mask, struct in_addr *subnet)
{
	subnet->s_addr = ip->s_addr & mask->s_addr;
}

inline void get_v4_subnet_bc(struct in_addr *ip, struct in_addr *mask, struct in_addr *subnet_bc)
{
	get_v4_subnet(ip, mask, subnet_bc);
	subnet_bc->s_addr |= ~mask->s_addr;
}

inline void get_v4_host(struct in_addr *ip, struct in_addr *mask, struct in_addr *host)
{
	host->s_addr = ip->s_addr & ~mask->s_addr;
}

inline void get_v6_mask_addr_by_maks_bits(uint8_t mask, struct in6_addr *mask_addr)
{
	int i, bytes, bits;

	bytes = mask >> 3;
	for (i = 0; i < bytes; i++)
		mask_addr->s6_addr[i] = 0xFF;

	if (bytes < 16)
	{
		bits = 8 - (mask & 0x7);
		mask_addr->s6_addr[bytes] = 0xFF - ((1 << bits) - 1);
	}

	for (i = bytes + 1; i < 16; i++)
		mask_addr->s6_addr[i] = 0;
}

inline void get_v6_subnet(struct in6_addr *ip, struct in6_addr *mask, struct in6_addr *subnet)
{
	subnet->s6_addr32[0] = ip->s6_addr32[0] & mask->s6_addr32[0];
	subnet->s6_addr32[1] = ip->s6_addr32[1] & mask->s6_addr32[1];
	subnet->s6_addr32[2] = ip->s6_addr32[2] & mask->s6_addr32[2];
	subnet->s6_addr32[3] = ip->s6_addr32[3] & mask->s6_addr32[3];
}

inline void get_v6_subnet_bc(struct in6_addr *ip, struct in6_addr *mask, struct in6_addr *subnet_bc)
{
	get_v6_subnet(ip, mask, subnet_bc);
	subnet_bc->s6_addr32[0] |= ~mask->s6_addr32[0];
	subnet_bc->s6_addr32[1] |= ~mask->s6_addr32[1];
	subnet_bc->s6_addr32[2] |= ~mask->s6_addr32[2];
	subnet_bc->s6_addr32[3] |= ~mask->s6_addr32[3];
}

inline void get_v6_host(struct in6_addr *ip, struct in6_addr *mask, struct in6_addr *host)
{
	host->s6_addr32[0] = ip->s6_addr32[0] & ~mask->s6_addr32[0];
	host->s6_addr32[1] = ip->s6_addr32[1] & ~mask->s6_addr32[1];
	host->s6_addr32[2] = ip->s6_addr32[2] & ~mask->s6_addr32[2];
	host->s6_addr32[3] = ip->s6_addr32[3] & ~mask->s6_addr32[3];
}


#endif /* __NET_ADDR_UTILS_H__ */
