#ifndef __THREAD_PRIO_H__
#define __THREAD_PRIO_H__

#include <sched.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <sys/resource.h>

#define THREAD_SET_PRIO(rt, prio) \
	do { \
		struct sched_param __thread_prio_h__sched_param; \
		int __thread_prio_h__rt; \
		if (rt == 0) \
		{ \
			__thread_prio_h__rt = SCHED_OTHER; \
			__thread_prio_h__sched_param.sched_priority = 0; \
		} \
		else \
		{ \
			__thread_prio_h__rt = SCHED_RR; \
			__thread_prio_h__sched_param.sched_priority = prio; \
		} \
		pthread_setschedparam(pthread_self(), __thread_prio_h__rt, &__thread_prio_h__sched_param); \
		if (rt == 0) \
		{ \
			pid_t __thread_prio_h__tid = syscall(SYS_gettid); \
			setpriority(PRIO_PROCESS, __thread_prio_h__tid, prio); \
		} \
	} while(0)

#endif /* __THREAD_PRIO_H__ */
