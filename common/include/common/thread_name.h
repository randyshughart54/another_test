#ifndef __THREAD_NAMES_H__
#define __THREAD_NAMES_H__
#define _GNU_SOURCE

#define TNAME_SIZE 16

#define THREAD_APPEND_NAME(suffix)                                                \
	do {                                                                          \
		char t_name[TNAME_SIZE] = { 0 };                                          \
		pthread_t tid = pthread_self();                                           \
		pthread_getname_np(tid, t_name, sizeof(t_name));                          \
		memset(&t_name[TNAME_SIZE - 1] - strlen(suffix), 0, strlen(suffix) + 1);  \
		strncat(t_name, suffix, sizeof(t_name) - 1);                              \
		pthread_setname_np(tid, t_name);                                          \
	} while(0)

#endif /*__THREAD_NAMES_H__*/