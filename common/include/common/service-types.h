#ifndef SERVICE_TYPES_H
#define	SERVICE_TYPES_H

#include <stdint.h>
#include "if-types.h"

#define srv_ID_STR_SIZE       256
#define srv_MAX_COMMAND_COUNT 5
#define srv_COMMAND_STR_SIZE  512
#define srv_NICE_COMMAND_STR_SIZE (srv_COMMAND_STR_SIZE + 10)

typedef enum
{
	srv_service_start_method_INVALID,
	srv_service_start_method_RESPAWN,
	srv_service_start_method_ONCE,
} srv_service_start_method_t;

typedef struct
{
	char srv_id[srv_ID_STR_SIZE];
	char srv_command[srv_COMMAND_STR_SIZE];
	char srv_restart_commands[srv_MAX_COMMAND_COUNT][srv_COMMAND_STR_SIZE];
	char srv_pre_start_commands[srv_MAX_COMMAND_COUNT][srv_COMMAND_STR_SIZE];
	srv_service_start_method_t start_method;
	int srv_restarts_number;
	int srv_niceness;
	int srv_have_sighub_update;
	int srv_restart_commands_count;
	int srv_pre_start_commands_count;
} srv_service_process_t;

typedef int srv_service_cmd_t;


#endif	/* SERVICE_TYPES_H */
