#ifndef _MSG_DATA_TYPES_
#define _MSG_DATA_TYPES_

#include <stdint.h>
#include <common/pp-types.h>
#include <ipc/ipc.h>
#include <libnetwork/network_defs_ip.h>
#ifndef BOARD_BPE_LINECARD
#include <vrrp-manager/vrrp.h>
#endif /* BOARD_BPE_LINECARD */

#define MAC_ADDR_SIZE 6

typedef struct _msg_rc {
	int rc;
} msg_rc_t;

typedef struct _msg_rc_with_tag {
	int rc;
	ipc_msg_tag_t tag;
	ipc_addr_t addr;
} msg_rc_with_tag_t;

typedef struct _msg_rc_flags {
	int rc;
	uint32_t flags;
} msg_rc_flags_t;

typedef struct _msg_rc_lag {
	int rc;
	uint32_t lag_iface_idx;
	uint8_t lag_id;
} msg_rc_lag_t;

typedef struct _msg_rc_lag_with_operstate {
	int rc;
	uint32_t lag_iface_idx;
	uint8_t lag_id;
	uint8_t lag_is_up;
	uint8_t dsa_egress_dev;
} msg_rc_lag_with_operstate_t;

typedef struct _msg_rc_if {
	int rc;
	uint32_t iface_idx;
	uint32_t tag;
	uint32_t vrf_index;
} msg_rc_if_t;

typedef struct _msg_rc_ifs {
	uint32_t ifs_cnt;
	msg_rc_if_t ifs[0];
} msg_rc_ifs_t;

typedef struct _msg_rc_if_tun {
	int rc;
	uint32_t iface_idx;
	uint32_t iface_idx_os;
	uint32_t os_idx;
} msg_rc_if_tun_t;

typedef struct _msg_rc_if_pw {
	int rc;
	uint32_t iface_idx;
	uint32_t bundle_iface_idx;
} msg_rc_if_pw_t;

typedef struct _msg_rc_bd {
	int rc;
	size_t count;
	size_t size;
	uint8_t target;
} msg_rc_bd_t;

typedef struct _msg_rc_ft {
	int rc;
	size_t count;
	size_t size;
} msg_ft_rc_t;

typedef struct _msg_rc_pg {
	int rc;
	int status;
	float progress;
} msg_rc_pg_t;

typedef struct _msg_rc_vsi_t
{
	bd_t bd;
	uint32_t lc_vsi[0];
} msg_rc_vsi_t;

#define MSG_TRAP_COMMON_FIELDS \
	uint32_t iface_idx; \
	uint32_t vrf_idx; \
	uint8_t mac_addr[MAC_ADDR_SIZE]; \
	uint8_t mac_addr_len // Valid length of the address in bytes

typedef struct _msg_req_trap_mac {
	MSG_TRAP_COMMON_FIELDS;
#if defined(BOARD_BPE_CTRLCARD) || defined(BOARD_BPE_LINECARD)
	int from;
#endif
} msg_req_trap_mac_t;

typedef struct _msg_rc_trap_mac {
	MSG_TRAP_COMMON_FIELDS;
	int rc;
} msg_rc_trap_mac_t;

/*
 * IGMP for IPv4 and MLD for IPv6
 */
typedef enum
{
	MRT_TYPE_IPv4,
	MRT_TYPE_IPv6
} mrt_type_e;

typedef struct
{
	int rc;             // Relevant only in reply messages
	uint32_t if_index;
	uint32_t vrf_id;
	mrt_type_e type;
} msg_mrt_iface_t;

typedef struct
{
	network_ip_prefix_t dst;
	int if_index;
	int vrf_id;
	int rc;
	int from;
} msg_req_trap_mc_ip_t;

typedef struct _msg_mpls_recovery_rc {
	int rc;
	uint8_t mpls_dbs_cleaned;
} msg_mpls_recovery_rc_t;

typedef struct
{
	uint32_t if_index;
	uint32_t os_idx;
} msg_os_index_rpl_t;

typedef struct
{
	uint8_t board_type;
	uint32_t ifaces_cnt;
	iface_t ifaces[0];
} pp_adv_msg_t;

#ifndef BOARD_BPE_LINECARD
typedef struct
{
	int      rc;

	uint8_t  priority;

	uint64_t advert_rcvd;
	uint64_t advert_sent;

	uint64_t become_master;
	uint64_t release_master;

	uint64_t packet_len_err;
	uint64_t ip_ttl_err;
	uint64_t invalid_type_rcvd;
	uint64_t addr_list_err;

	uint64_t invalid_authtype;
	uint64_t authtype_mismatch;
	uint64_t auth_failure;

	uint64_t pri_zero_rcvd;
	uint64_t pri_zero_sent;

	char     role[VRRP_INSTANCE_NAME_LEN];
	char     state[VRRP_INSTANCE_NAME_LEN];
} msg_rc_vrrp_stat_t;
#endif /* BOARD_BPE_LINECARD */

#endif /* _MSG_DATA_TYPES_ */
