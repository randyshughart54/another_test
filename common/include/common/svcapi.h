#ifndef __COMMON_SERVICE_API_H_INCLUDED__
#define __COMMON_SERVICE_API_H_INCLUDED__

#include <ipc/ipc.h>
#include <common/checks.h>

typedef int (*svcapi_cb_fn)(ipc_addr_t addr);

typedef enum _svcapi_rc {
	SVCAPI_RC_OK,
	SVCAPI_RC_FAIL,
	SVCAPI_RC_BAD_ADDR
} svcapi_rc_e;

#define SVCAPI_PROT_PARAMS \
	ipc_conn_t *conn, \
	ipc_addr_t dst, \
	int timeout, \
	ipc_rpl_data_u *rpl_data

#define SVCAPI_PROT_PARAMS_MCAST \
	ipc_conn_t *conn

#define SVCAPI_FN_PARAMS \
	conn, \
	dst, \
	timeout, \
	rpl_data

#define SVCAPI_FN_PARAMS_MCAST \
	conn

#define SVCAPI_PARAMS_CHECK() \
	do { \
		if (PTR_IS_NULL(conn)) return SVCAPI_RC_FAIL; \
		if (PTR_IS_NULL(rpl_data)) return SVCAPI_RC_FAIL; \
	} while (0)

#define SVCAPI_PARAMS_CHECK_MCAST() \
	do { \
		if (PTR_IS_NULL(conn)) return SVCAPI_RC_FAIL; \
	} while (0)

#define SVCAPI_SEND_MSG(msg_ptr) \
	do { \
		rcode_e ipc_rc; \
		ipc_rc = ipc_send(conn, dst, (msg_ptr), ((timeout == IPC_TMO_NOW) ? IPC_NONBLOCK : 0), rpl_data); \
		if (ipc_rc != RCODE_OK) { \
			log_err("Couldn't send message, return code: %d", ipc_rc); \
			return SVCAPI_RC_FAIL; \
		} \
	} while (0)

#define SVCAPI_SEND_MSG_RETRY(msg_ptr, retry) \
	do { \
		int _ret_cnt = 1; \
		for (_ret_cnt = 1; _ret_cnt <= retry; _ret_cnt++) { \
			if (ipc_send(conn, dst, (msg_ptr), ((timeout == IPC_TMO_NOW) ? IPC_NONBLOCK : 0), rpl_data) != RCODE_OK) { \
				log_err("Couldn't send message, try %d more times", (retry - _ret_cnt)); \
				if (_ret_cnt < retry) continue; \
				else return SVCAPI_RC_FAIL; \
			} \
			break; \
		} \
	} while (0)

#define SVCAPI_SEND_MCAST_MSG(msg_ptr, group) \
	do { \
		rcode_e ipc_rc; \
		ipc_rc = ipc_send_mcast(conn, (group), (msg_ptr)); \
		if (ipc_rc != RCODE_OK) { \
			log_err("Couldn't send message to mcast group '%s', return code: %d", ipc_addr_mcast_group_stringize(group), ipc_rc); \
			return SVCAPI_RC_FAIL; \
		} \
	} while (0)

#define SVCAPI_SEND_LOCAL_MCAST_MSG(msg_ptr, group) \
	do { \
		rcode_e ipc_rc; \
		ipc_rc = ipc_send_local_mcast(conn, (group), (msg_ptr)); \
		if (ipc_rc != RCODE_OK) { \
			log_err("Couldn't send message to mcast group '%s', return code: %d", ipc_addr_mcast_group_stringize(group), ipc_rc); \
			return SVCAPI_RC_FAIL; \
		} \
	} while (0)

#define SVCAPI_SEND_MCAST_MSG_COMMON(msg_ptr) \
	SVCAPI_SEND_MCAST_MSG(msg_ptr, IPC_MCAST_GROUP_COMMON)

#define SVCAPI_SEND_MCAST_MSG_LC(msg_ptr) \
	SVCAPI_SEND_MCAST_MSG(msg_ptr, IPC_MCAST_GROUP_LC)

#endif
