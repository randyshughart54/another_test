#ifndef _COMMON_IPC_HUB_INCLUDED_H_
#define _COMMON_IPC_HUB_INCLUDED_H_

#include <ns/ns.h>

#define COMMON_IPC_HUB_ADDR "tcp://127.0.0.1:12352"

#define IPC_HUB_NS_NAME "stack"
#define IPC_HUB_SOCKET_GET_CB ipc_hub_socket_get

static inline int ipc_hub_socket_get(int family, int type, int protocol)
{
	return ns_socket(IPC_HUB_NS_NAME, family, type, protocol, DEFAULT_NS_NAME);
}

#endif /* _COMMON_IPC_HUB_INCLUDED_H_ */
