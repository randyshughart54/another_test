#ifndef __BOARD_TYPES_H__
#define __BOARD_TYPES_H__

inline const char * common_api_get_board_name_short()
{
    return
#if defined(BOARD_ME5200)
    "ME5200";
#elif defined(BOARD_ME5100)
    "ME5100";
#elif defined(BOARD_BPE_CTRLCARD)
    "ME5000";
#elif defined(BOARD_SIM)
    "SIM";
#else
#error "unknown board"
    "Unknown";
#endif
}

inline const char * common_api_get_board_name_full()
{
    return
#if defined(BOARD_ME5200)
    "Eltex ME5200 carrier router";
#elif defined(BOARD_ME5100)
    "Eltex ME5100 carrier router";
#elif defined(BOARD_BPE_CTRLCARD)
    "Eltex ME5000 modular carrier router";
#elif defined(BOARD_SIM)
    "SIM";
#else
#error "unknown board"
    "Unknown";
#endif
}

#endif __BOARD_TYPES_H__