#ifndef __COMMON_ERRORS_H__
#define __COMMON_ERRORS_H__

#include <log/log.h>

enum
{
	ERR_NONE = 0,
	ERR_FAIL = -1,
	ERR_BAD_PTR = -2,
	ERR_BAD_PARAM = -3,
	ERR_MEM = -4,
	ERR_NOT_FOUND = -5,
	ERR_FULL = -6,
	ERR_ALREADY_EXIST = -7,
	ERR_TIMEOUT = -8,
	ERR_AGAIN = -9,
	ERR_NOT_SUPPORTED = -10,
	ERR_WAIT = -11,
	ERR_SKIP = -12
};

#define SET_COMMON_ERROR(E) (((E) != ERR_NONE) ? log_err("FILE %s LINE %d ERRCODE %d", __FILE__, __LINE__, (E)), (E) : (E))

#endif /* __ERRORS_H__ */
