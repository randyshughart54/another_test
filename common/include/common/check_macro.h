#ifndef _CHECK_MACRO_
#define _CHECK_MACRO_

#include <unistd.h>
#include <syslog.h>

#ifndef NDEBUG
# define ASSERT(x) \
	do { \
		if (!(x)) { \
			syslog(LOG_ERR, "<%s> ASSERT in line %d, '%s'\n", __FUNCTION__, __LINE__, #x); \
			/*fflush(stdout);*/ \
			_exit(0); \
		} \
	} while (0)
#else
# define ASSERT(x) ((void)0)
#endif

#ifndef OUT_OF_RANGE
#define OUT_OF_RANGE(x, min, max) \
	(((x) < (min) || (x) > (max)) ? \
		syslog(LOG_ERR, "<%s> OUT_OF_RANGE check failed: " #x " is really out of range (%d %d %d) !!!\n", __FUNCTION__, (min), (x), (max)),1 : \
		0)
#endif

#ifndef IS_NE
#define IS_NE(x, value) \
	(((x) != (value) ) ? \
		syslog(LOG_ERR, "<%s> IS_NE check failed: " #x " (%d != %d) !!!\n", __FUNCTION__, (x), (value)),1 : \
		0)
#endif

/*
 * Formatted output
 */
#ifndef IS_NE_fmt
#define IS_NE_fmt(x, value, fmt) \
	(((x) != (value) ) ? \
		syslog(LOG_ERR, "<%s> IS_NE check failed: " #x " (" #fmt " != " #fmt ") !!!\n", __FUNCTION__, (x), (value)),1 : \
		0)
#endif

/*
 * Long int
 */
#ifndef IS_NE_l
#define IS_NE_l(x, value) \
	IS_NE_fmt(x, value, %ld)
#endif

#ifndef IS_EQ
#define IS_EQ(x, value) \
	(((x) == (value) ) ? \
		syslog(LOG_ERR, "<%s> IS_EQ check failed: " #x " (%d == %d) !!!\n", __FUNCTION__, (x), (value)),1 : \
		0)
#endif

#ifndef IS_LT
#define IS_LT(x, value) \
	(((x) < (value) ) ? \
		syslog(LOG_ERR, "<%s> IS_LT check failed: " #x " (%d < %d) !!!\n", __FUNCTION__, (x), (value)),1 : \
		0)
#endif

#ifndef IS_LE
#define IS_LE(x, value) \
	(((x) <= (value) ) ? \
		syslog(LOG_ERR, "<%s> IS_LE check failed: " #x " (%d <= %d) !!!\n", __FUNCTION__, (x), (value)),1 : \
		0)
#endif

#ifndef IS_MT
#define IS_MT(x, value) \
	(((x) > (value) ) ? \
		syslog(LOG_ERR, "<%s> IS_MT check failed: " #x " (%d > %d) !!!\n", __FUNCTION__, (x), (value)),1 : \
		0)
#endif

#ifndef IS_ME
#define IS_ME(x, value) \
	(((x) >= (value) ) ? \
		syslog(LOG_ERR, "<%s> IS_ME check failed: " #x " (%d >= %d) !!!\n", __FUNCTION__, (x), (value)),1 : \
		0)
#endif

#ifndef PTR_IS_NULL
#define PTR_IS_NULL(x) \
	(((x) == NULL) ? \
		syslog(LOG_ERR, "<%s> PTR_IS_NULL check failed: " #x " is NULL !!!\n", __FUNCTION__),1 :\
		0)
#endif

#ifndef PTR_NOT_NULL
#define PTR_NOT_NULL(x) \
	(((x) != NULL) ? \
		syslog(LOG_ERR, "<%s> PTR_NOT_NULL check failed: " #x " is not NULL !!!\n", __func__),1 :\
		0)
#endif

#ifndef PTR_IS_EQ
#define PTR_IS_EQ(x, y) \
	(((x) == (y)) ? \
		syslog(LOG_ERR, "<%s> PTR_IS_EQ check failed: " #x " == " #y " !!!\n", __FUNCTION__),1 :\
		0)
#endif

#ifndef PTR_IS_NE
#define PTR_IS_NE(x, y) \
	(((x) != (y)) ? \
		syslog(LOG_ERR, "<%s> PTR_IS_NE check failed: " #x " != " #y " !!!\n", __FUNCTION__),1 :\
		0)
#endif

#ifndef CHK_INTRC
#define CHK_INTRC(__methodNAME__, __isOK__) \
( { \
	int rc = (__methodNAME__); \
	((__isOK__) != rc) ? \
		syslog(LOG_DEBUG, "<%s> CHK_INTRC: " #__methodNAME__ " failed (%d != %d)" \
		    , __func__, rc, (__isOK__)),1 : \
		0; \
} )
#endif

#endif /* _CHECK_MACRO_ */
