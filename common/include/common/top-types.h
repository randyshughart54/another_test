#ifndef _SYSTEM_TOP_TYPES_H_INCLUDED_
#define _SYSTEM_TOP_TYPES_H_INCLUDED_

#define VRF_MAPPER_NAME_LEN 32 + 5  // + 5 is ipv4- or ipv6- prefix

typedef struct
{
	char vrf_name[VRF_MAPPER_NAME_LEN];
	unsigned int mapper_vrf_index;
} vrf_names_t;

typedef struct
{
	int vrf_count;
	vrf_names_t vrf_arr[0];
} vrf_mapper_name_msg_t;

#endif /* _SYSTEM_PP_TYPES_H_INCLUDED_ */