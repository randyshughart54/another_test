#ifndef __COMMON_INTERFACE_H_INCLUDED__
#define __COMMON_INTERFACE_H_INCLUDED__

#include <stdint.h>
#include "vrf.h"
#include "policy-types.h"
#include <libnetwork/network_ip_address.h>

#define DHCP_RELAY_NAME_LEN		256

#define HW_ADDR_LEN 6
#define IP_ADDR_LEN 16

#define MAX_SUBIFACES 8192

#define ADMIN_UP_STATE                  0x1
#define OPER_UP_STATE                   0x2
#define L3_IF                           0x4
#define HW_EXIST                        0x8
#define DSA_EGRESS_DEV_VALID            0x10
#define MCAST_IF                        0x20
#define UDLD_ERRDISABLED_STATE          0x40
#define LDP_IGP_SYNC_ISIS               0x80
#define LDP_IGP_SYNC_OSPF               0x100
#define MPLS_FORWARDING	                0x200
#define USING_IPV4                      0x400
#define USING_IPV6                      0x800
#define NO_RESOURCES_IF                 0x1000
#define IGMP_PROMISCUOUS_DISABLED_IF    0x2000

#define LOAD_INTV_MIN 15   // seconds
#define LOAD_INTV_MAX 500  // seconds

#define ARP_TMO_MIN 30    // seconds
#define ARP_TMO_MAX 76400 // seconds
#define ARP_TMO_BLOCK 5   // seconds
#define ARP_TMO_CHANGE_DST 5 // seconds

#define OUTER_TPID_DEFAULT 0x8100
#define INNER_TPID_DEFAULT 0x8100

#define INVALID_IF_INDEX 0

#define ADMIN_IS_UP(if) ((if)->flags & ADMIN_UP_STATE)
#define ADMIN_SET_UP(if) ((if)->flags |= ADMIN_UP_STATE)
#define ADMIN_SET_DOWN(if) ((if)->flags &= ~(ADMIN_UP_STATE))

#define OPER_IS_UP(if) ((if)->flags & OPER_UP_STATE)
#define OPER_SET_UP(if) ((if)->flags |= OPER_UP_STATE)
#define OPER_SET_DOWN(if) ((if)->flags &= ~(OPER_UP_STATE))

#define IS_L3_IF(if) ((if)->flags & L3_IF)
#define L3_IF_SET(if) ((if)->flags |= L3_IF)
#define L2_IF_SET(if) ((if)->flags &= ~(L3_IF))

#define IS_HW_EXIST(if) ((if)->flags & HW_EXIST)
#define HW_EXIST_SET(if) ((if)->flags |= HW_EXIST)
#define HW_EXIST_CLEAR(if) ((if)->flags &= ~(HW_EXIST))

#define IS_DSA_EGRESS_DEV_VALID(if) ((if)->flags & DSA_EGRESS_DEV_VALID)
#define DSA_EGRESS_DEV_VALID_SET(if) ((if)->flags |= DSA_EGRESS_DEV_VALID)
#define DSA_EGRESS_DEV_VALID_CLEAR(if) ((if)->flags &= ~(DSA_EGRESS_DEV_VALID))

#define IS_MCAST_IF(if) ((if)->flags & MCAST_IF)
#define MCAST_IF_SET(if) ((if)->flags |= MCAST_IF)
#define MCAST_IF_CLEAR(if) ((if)->flags &= ~(MCAST_IF))

#define IS_UDLD_ERRDISABLED(if) ((if)->flags & UDLD_ERRDISABLED_STATE)
#define UDLD_ERRDISABLE_SET(if) ((if)->flags |= UDLD_ERRDISABLED_STATE)
#define UDLD_ERRDISABLE_CLEAR(if) ((if)->flags &= ~(UDLD_ERRDISABLED_STATE))

#define IS_LDP_IGP_SYNC_ISIS_SET(if) ((if)->flags & LDP_IGP_SYNC_ISIS)
#define LDP_IGP_SYNC_ISIS_SET(if) ((if)->flags |= LDP_IGP_SYNC_ISIS)
#define LDP_IGP_SYNC_ISIS_CLEAR(if) ((if)->flags &= ~(LDP_IGP_SYNC_ISIS))

#define IS_LDP_IGP_SYNC_OSPF_SET(if) ((if)->flags & LDP_IGP_SYNC_OSPF)
#define LDP_IGP_SYNC_OSPF_SET(if) ((if)->flags |= LDP_IGP_SYNC_OSPF)
#define LDP_IGP_SYNC_OSPF_CLEAR(if) ((if)->flags &= ~(LDP_IGP_SYNC_OSPF))

#define IS_MPLS_FORWARDING_SET(if) ((if)->flags & MPLS_FORWARDING)
#define MPLS_FORWARDING_SET(if) ((if)->flags |= MPLS_FORWARDING)
#define MPLS_FORWARDING_CLEAR(if) ((if)->flags &= ~(MPLS_FORWARDING))

#define IS_USING_IPV4(if) ((if)->flags & USING_IPV4)
#define USING_IPV4_SET(if) ((if)->flags |= USING_IPV4)
#define USING_IPV4_CLEAR(if) ((if)->flags &= ~(USING_IPV4))

#define IS_USING_IPV6(if) ((if)->flags & USING_IPV6)
#define USING_IPV6_SET(if) ((if)->flags |= USING_IPV6)
#define USING_IPV6_CLEAR(if) ((if)->flags &= ~(USING_IPV6))

#define IS_NO_RESOURCES_IF(if) ((if)->flags & NO_RESOURCES_IF)
#define SET_NO_RESOURCES_IF(if) ((if)->flags |= NO_RESOURCES_IF)
#define CLEAR_NO_RESOURCES_IF(if) ((if)->flags &= ~(NO_RESOURCES_IF))

#define IS_IGMP_PROMISCUOUS_DISABLED_IF(if) ((if)->flags & IGMP_PROMISCUOUS_DISABLED_IF)
#define SET_IGMP_PROMISCUOUS_DISABLED_IF(if) ((if)->flags |= IGMP_PROMISCUOUS_DISABLED_IF)
#define CLEAR_IGMP_PROMISCUOUS_DISABLED_IF(if) ((if)->flags &= ~(IGMP_PROMISCUOUS_DISABLED_IF))


/*
 * Negotiation flags:
 *   0 bit  - Shows if negotiation is enabled
 * 1-2 bits - Negotiation flags for 10M(can be set to full-duplex, half-duplex or none)
 * 3-4 bits - Negotiation flags for 100M(can be set to full-duplex, half-duplex or none)
 *   5 bit  - Negotiation flags for 1000M(can be set to full-duplex or none)
 * 6-7 bits - Not used
 */
#define NG_FLAG_ENABLED 0x1
#define NG_ENABLE(ng_flags) ((ng_flags) |= NG_FLAG_ENABLED)
#define NG_IS_ENABLED(ng_flags) ((ng_flags) & NG_FLAG_ENABLED)
#define NG_DISABLE(ng_flags) ((ng_flags) &= ~NG_FLAG_ENABLED)

#define NG_FLAG_NONE 0x0
#define NG_FLAG_FULL_DPLX 0x1
#define NG_FLAG_HALF_DPLX 0x2

#define NG_FLAG_GET(ng_flags, ng_flags_offset, ng_flags_mask) \
	(((ng_flags) >> (ng_flags_offset)) & (ng_flags_mask))

#define NG_FLAG_CLEAR(ng_flags, ng_flags_offset, ng_flags_mask) \
	((ng_flags) &= ~((ng_flags_mask) << (ng_flags_offset)))

#define NG_FLAG_SET(ng_flags, ng_flags_offset, ng_flags_mask, ng_set_flag) \
	do { \
		NG_FLAG_CLEAR(ng_flags, ng_flags_offset, ng_flags_mask); \
		ng_flags |= (((ng_set_flag) & (ng_flags_mask)) << (ng_flags_offset)); \
	} while (0)

#define NG10_FLAGS_MASK 0x3
#define NG10_FLAGS_OFFSET 0x1
#define NG10_FLAG_GET(ng_flags) NG_FLAG_GET(ng_flags, NG10_FLAGS_OFFSET, NG10_FLAGS_MASK)
#define NG10_FLAG_SET(ng_flags, ng_set_flag) NG_FLAG_SET(ng_flags, NG10_FLAGS_OFFSET, NG10_FLAGS_MASK, ng_set_flag)

#define NG100_FLAGS_MASK 0x3
#define NG100_FLAGS_OFFSET 0x3
#define NG100_FLAG_GET(ng_flags) NG_FLAG_GET(ng_flags, NG100_FLAGS_OFFSET, NG100_FLAGS_MASK)
#define NG100_FLAG_SET(ng_flags, ng_set_flag) NG_FLAG_SET(ng_flags, NG100_FLAGS_OFFSET, NG100_FLAGS_MASK, ng_set_flag)

#define NG1000_FLAGS_MASK 0x1
#define NG1000_FLAGS_OFFSET 0x5
#define NG1000_FLAG_GET(ng_flags) NG_FLAG_GET(ng_flags, NG1000_FLAGS_OFFSET, NG1000_FLAGS_MASK)
#define NG1000_FLAG_SET(ng_flags, ng_set_flag) NG_FLAG_SET(ng_flags, NG1000_FLAGS_OFFSET, NG1000_FLAGS_MASK, ng_set_flag)


#define PW_RD_SIZE 8

#define RSVP_TUNNEL_NAME_LEN 32
#define LSP_TUNNEL_NAME_LEN 32

typedef uint8_t ng_flags_t;

typedef enum _dhcp_relay_agent {
	DHCP_RELAY_UNKNOWN,
	DHCP_RELAY_APPEND,
	DHCP_RELAY_REPLACE,
	DHCP_RELAY_FORWARD,
	DHCP_RELAY_DISCARD
} dhcp_relay_agent_e;

typedef enum _iface_type {
	IFACE_TYPE_UNKNOWN,
	IFACE_TYPE_PORT,
	IFACE_TYPE_LAG,
	IFACE_TYPE_SUB,
	IFACE_TYPE_TUNNEL,
	IFACE_TYPE_LOOPBACK,
	IFACE_TYPE_OOB,
	IFACE_TYPE_VIRTUAL
} iface_type_e;

typedef enum _iface_sub_vlan_type {
	IFACE_SUB_VLAN_TYPE_UNKNOWN,
	IFACE_SUB_VLAN_TYPE_SINGLE,
	IFACE_SUB_VLAN_TYPE_DOUBLE,
	IFACE_SUB_VLAN_TYPE_DEFAULT,
	IFACE_SUB_VLAN_TYPE_UNTAGGED
} iface_sub_vlan_type_e;

typedef enum _iface_spd {
	IFACE_SPD_UNKNOWN,
	IFACE_SPD_10M,
	IFACE_SPD_100M,
	IFACE_SPD_1G,
	IFACE_SPD_2_5G,
	IFACE_SPD_6_5G,
	IFACE_SPD_10G,
	IFACE_SPD_12_5G,
	IFACE_SPD_20G,
	IFACE_SPD_40G,
	IFACE_SPD_100G
} iface_spd_e;

typedef enum _iface_dplx
{
	IFACE_DPLX_UNKNOWN,
	IFACE_DPLX_HALF,
	IFACE_DPLX_FULL
} iface_dplx_e;

typedef enum _port_type {
	PORT_TYPE_UNKNOWN,
	PORT_TYPE_GE,        //   1 Gbit Ethernet
	PORT_TYPE_TE,        //  10 Gbit Ethernet
	PORT_TYPE_FE,        //  40 Gbit Ethernet
	PORT_TYPE_HE,         // 100 Gbit Ethernet
	PORT_TYPE_COUNT
} port_type_e;

typedef enum _iface_mediatype
{
	IFACE_MEDIA_UNKNOWN,
	IFACE_MEDIA_COPPER,
	IFACE_MEDIA_FIBER,
	IFACE_MEDIA_AC,
} iface_mediatype_e;

typedef enum _tunnel_direction {
	TUNNEL_DIRECTION_INGRESS,
	TUNNEL_DIRECTION_EGRESS,
	TUNNEL_DIRECTION_UNDIRECTION,
} tunnel_direction_e;

typedef enum _tunnel_type {
	TUNNEL_TYPE_UNKNOWN,
	TUNNEL_TYPE_MPLS,
	TUNNEL_TYPE_PW,
	TUNNEL_TYPE_PW_GROUP,
	TUNNEL_TYPE_IP,
	TUNNEL_TYPE_PIM_ENCAPS,
	TUNNEL_TYPE_PIM_DECAPS,
	TUNNEL_TYPE_L3VPN,
	TUNNEL_TYPE_MPLS_OS,
	TUNNEL_TYPE_RSVP,
} tunnel_type_e;

typedef enum _tunnel_ip_encap_type {
	TUNNEL_IP_ENCAP_UNKNOWN, //If source + dest are not specified
	TUNNEL_IP_ENCAP_IP_IPV4,
	TUNNEL_IP_ENCAP_IP_IPV6,
	TUNNEL_IP_ENCAP_GRE_IPV4,
	TUNNEL_IP_ENCAP_GRE_IPV6,
} tunnel_ip_encap_type_e;

typedef enum _virtual_type {
	VIRTUAL_TYPE_UNKNOWN,
	VIRTUAL_TYPE_VRRP,
	VIRTUAL_TYPE_OSPFV3_VL
	// TODO: new types
} virtual_type_e;

typedef struct _arp {
	uint32_t timeout;
	uint8_t  proxy_en;
	uint8_t  local_proxy_en;
	uint32_t aging_time;
	uint32_t blocking_time;
} arp_t;

typedef enum _tag_act_mode {
	TAG_ACT_MODE_INVALID = -1,
	TAG_ACT_MODE_NONE,
	TAG_ACT_MODE_POP,
	TAG_ACT_MODE_PUSH,
	TAG_ACT_MODE_REPLACE,
	TAG_ACT_MODE_EXCHANGE,
	TAG_ACT_MODE_REPLACE_POP
} tag_act_mode_e;

#define MAX_TAG_FORMATS 3

typedef struct _tag_act {
	tag_act_mode_e mode;
	union {
		uint8_t tags_num;
		struct {
			uint16_t outer_vid;
			uint32_t outer_tpid;
			uint16_t inner_vid;
			uint32_t inner_tpid;
			uint8_t  waiting;
			uint32_t action_id_used[MAX_TAG_FORMATS];
		};
	};
} tag_act_t;

typedef enum _lag_lb_mode {
	LAG_LB_MODE_INVALID = -1,
	LAG_LB_MODE_RR,
	LAG_LB_MODE_HASH
} lag_lb_mode_e;

typedef enum _lag_lb_hash {
	LAG_LB_HASH_ERROR = -1,
	LAG_LB_HASH_SRC_MAC,
	LAG_LB_HASH_DST_MAC,
	LAG_LB_HASH_SRC_IP,
	LAG_LB_HASH_DEST_IP,
	LAG_LB_HASH_MPLS_LABEL,
	LAG_LB_HASH_DEST_PORT,
	LAG_LB_HASH_SRC_PORT
} lag_lb_hash_e;

typedef enum _iface_errdisable_reason {
	IFACE_ERRDISABLE_INVALID = -3,
	IFACE_ERRDISABLE_NONE = -2,
	IFACE_ERRDISABLE_ADMIN = -1,
	IFACE_ERRDISABLE_UDLD,
	//PUT NEW REASON HERE
	IFACE_ERRDISABLE_COUNT
} iface_errdisable_reason_e ;

typedef enum _iface_igp_shortcut_type
{
	IFACE_IGP_SHORTCUT_METRIC_TYPE_NONE,
	IFACE_IGP_SHORTCUT_METRIC_TYPE_ABS,
	IFACE_IGP_SHORTCUT_METRIC_TYPE_REL,
} iface_igp_shortcut_metric_type_e;

#define RSVP_GR_OWNER 0x21020000
#define BGP_GR_OWNER 0x42010000
#define TPM_GR_OWNER 0x3a010000
#define LDP_GR_OWNER 0x23020000

#define REASON_ENUM_TO_STR(reason) \
			(reason == IFACE_ERRDISABLE_NONE) ? "none" : \
			(reason == IFACE_ERRDISABLE_ADMIN) ? "admin" : \
			(reason == IFACE_ERRDISABLE_UDLD) ? "udld" : "-----"

typedef struct _pw_route_distinguisher {
	uint8_t rd[PW_RD_SIZE];
} pw_route_distinguisher_t;

typedef struct
{
	int port;
	port_type_e type;
} log_port_t;

typedef struct _iface_desc {
	iface_type_e iface_type;
	uint32_t iface_idx;
	uint32_t vrf_idx;
	uint32_t os_idx;

	union {
		struct {
			log_port_t  log_port;
			uint8_t     dev;
			uint8_t     unit;
			uint8_t     hw_dev;
			uint32_t    hw_port;
		} port;

		struct {
			uint8_t     dev;
			uint8_t     unit;
			uint16_t    number;
			uint8_t     hw_dev;
			uint32_t    hw_port;
		} oob_port;

		struct {
			uint8_t id;
		} lag;

		struct {
			uint32_t number;
			uint32_t parent_iface_idx;
			iface_type_e parent_type;
			union
			{
				struct
				{
					log_port_t  log_port;
					uint8_t     dev;
					uint8_t     unit;
				} port;

				struct
				{
					uint8_t id;
				} lag;
			} parent;
		} sub;

		struct {
			tunnel_type_e type;
			union
			{
				struct
				{
					network_ip_address_t pw1_peer_addr;
					network_ip_address_t pw2_peer_addr;
					uint32_t pw1_vc_index;
					uint32_t pw2_vc_index;
				} pw_group;
				struct
				{
					network_ip_address_t pw_peer_addr;
					uint32_t pw_vc_index;
					uint32_t pw_group_index;
					uint16_t local_ve_id;
					uint16_t remote_ve_id;
					pw_route_distinguisher_t local_rd;
					pw_route_distinguisher_t remote_rd;
				} pw;
				struct
				{
					network_ip_prefix_t fec;
					network_ip_address_t out_next_hop_addr;
					uint32_t out_if_index;
					uint32_t out_label;
					uint16_t lsp_tun_id;       //must be zero in case of non-RSVP
					uint16_t lsp_tun_instance;
				} tunnel;
				struct
				{
					network_ip_prefix_t lsp_fec;
					network_ip_address_t pe_addr;
					uint32_t out_if_index;
					uint32_t out_label;
					int ve_id;
				} l3vpn;
				struct
				{
					char tunnel_name[RSVP_TUNNEL_NAME_LEN];
					char tunnel_lsp_name[LSP_TUNNEL_NAME_LEN];
					network_ip_address_t ingress_lsr;
					network_ip_address_t egress_lsr;
					uint16_t tun_id;
					uint16_t tun_instance;
					uint32_t lsp_xc_index;
				} rsvp;
				struct
				{
					uint32_t vrf_idx;
				} ip_tunnel;
			};
			uint32_t parent_iface_idx;
			uint32_t id;
			uint32_t owner_type;
		} tunnel;

		struct {
			uint32_t number;
		} lo;

		struct {
			uint32_t parent_if_index;
		} virt;
	};
} iface_desc_t;

#define BFD_FAST_DETECT_NONE 0x0
#define BFD_FAST_DETECT_IPV4 0x1
#define BFD_FAST_DETECT_IPV6 0x2

/*
 * @lag.dsa_egress_dev: identifies linecard that contains at least one physical port
 *   which is an active member of the LAG.
 *   @dsa_egress_dev is valid if and only if IS_DSA_EGRESS_DEV_VALID(iface_props_t) is nonzero.
 *   If there are no active members in the LAG, IS_DSA_EGRESS_DEV_VALID(iface_props_t) == 0.
 *   @dsa_egress_dev is provided by pp-manager.
 *   @dsa_egress_dev is used by if-manager for configuring the LAG interface,
 *   so that when CPU sends a packet via LAG interface, it is sent to the linecard
 *   identified by @dsa_egress_dev.
 */
typedef struct _iface_props {
	uint32_t flags;
	uint32_t cos_profile_index; /* If 0, COS profile is not configured */
	uint32_t qos_remark_profile_index; /* If 0, QOS profile is not configured */
	uint32_t bandwidth;
	char rate_limit_profile[RATE_LIMIT_PROFILE_NAME_LEN];
	char svc_plc_in[IF_SVC_PLC_NAME_LEN];
	char svc_plc_out[IF_SVC_PLC_NAME_LEN];
	char shape_profile[SHAPE_PROFILE_NAME_LEN];
	uint16_t load_interval;
	arp_t arp;
	char access_grp[IF_ACCESS_GRP_NAME_LEN];
	uint32_t l3_vsi_id;
	uint32_t vlan_port_id;
	char vrf_name[VRF_NAME_LEN];
	uint16_t lisi_hold_time; /* LGP IGP Sync ISIS hold-time */
	uint16_t liso_hold_time; /* LGP IGP Sync OSPF hold-time */
	uint32_t te_metric;
	uint32_t max_reservable_bw; //kbps
	float reserved_bw; //kbps

	union {
		struct {
			uint32_t lag_iface_idx;
			uint32_t lag_id;
			uint8_t  hwaddr[HW_ADDR_LEN];
			uint16_t mru;
			uint16_t ip_mtu;
			iface_spd_e  speed;
			iface_dplx_e duplex;
			ng_flags_t ng_flags;
			uint32_t outer_tpid_cfg;
			uint32_t inner_tpid_cfg;
			uint32_t outer_tpid;
			uint32_t inner_tpid;
			uint32_t hold_time_up_msec;
			uint32_t hold_time_down_msec;
			uint8_t  tpid_waiting;
			uint32_t vrrp_if_index;
		} port;

		struct {
			uint8_t  hwaddr[HW_ADDR_LEN];
			uint16_t mru;
			uint16_t ip_mtu;
			iface_spd_e  speed;
			iface_dplx_e duplex;
			ng_flags_t ng_flags;
		} oob_port;

		struct {
			uint8_t  hwaddr[HW_ADDR_LEN];
			uint16_t mru;
			uint16_t ip_mtu;
			iface_spd_e  speed;
			iface_dplx_e duplex;
			lag_lb_mode_e lb_mode;
			ng_flags_t ng_flags;
			uint8_t dsa_egress_dev;
			uint32_t vrrp_if_index;
			struct {
				network_ip4_address_t ipv4_src_addr;
				network_ip4_address_t ipv4_dst_addr;
				network_ip6_address_t ipv6_src_addr;
				network_ip6_address_t ipv6_dst_addr;
				uint8_t fast_detect;
				uint8_t multiplier;
				uint16_t tx_interval;
				uint16_t rx_interval;
			} bfd;
		} lag;

		struct {
			uint8_t  hwaddr[HW_ADDR_LEN];
			iface_sub_vlan_type_e type;
			uint16_t outer_vid;
			uint16_t inner_vid;
			tag_act_t rewrite_tag_action_ingress;
			tag_act_t rewrite_tag_action_egress;
			uint32_t vrrp_if_index;
		} sub;

		struct
		{
			tunnel_type_e type;
			union {
				struct {
					int pw_fec_id;
				} tunnel;

				struct {
					tunnel_ip_encap_type_e encap;
					network_ip_address_t source;
					network_ip_address_t dest;
					uint8_t tos;
					uint8_t ttl;
					uint8_t dfbit: 1; // 0 - copy, 1 - disable
					char vrf_name[VRF_NAME_LEN]; //src + dst vrf name
				} ip;
			};
		} tunnel;

		struct {
			uint16_t mru;
			uint16_t ip_mtu;
		} lo;

		struct {
			virtual_type_e type;
			union {
				struct {
					uint8_t is_master_state:1;            // Master VRRP instance
					int     garp_delay;                   // Delay to launch gratuitous ARP (sec)
					int     garp_repeat;                  // Gratuitous ARP repeat value (times)
					int     garp_refresh;                 // Next scheduled gratuitous ARP refresh (sec)
					int     garp_refresh_repeat;          // Refresh gratuitous ARP repeat value (times)
					u_char  macvlan_hw_addr[HW_ADDR_LEN]; // VRRP virtual mac
				} vrrp;
				// TODO: new types
			};
		} virt;
	};
} iface_props_t;

typedef struct _iface {
	iface_desc_t  desc;
	iface_props_t props;
} iface_t;

typedef struct {
	iface_desc_t desc;
	char relay_name[DHCP_RELAY_NAME_LEN];
} dhcp_relay_input_if_t;

typedef struct {
	network_ip_address_t address;
	char relay_name[DHCP_RELAY_NAME_LEN];
	char vrf_name[VRF_NAME_LEN];
} dhcp_relay_server_addr_t;

typedef struct {
	dhcp_relay_agent_e mode;
	char relay_name[DHCP_RELAY_NAME_LEN];
} dhcp_relay_mode_t;
/*
 *@idx_persist = 1 - if-indexes save in config file
 *@idx_persist = 0 - delete if-indexes's config file
 * and don't logging any changing
 */
typedef struct _if_idx_persist{
	uint8_t idx_persist;
} if_idx_persist_t;

typedef enum _lag_member_diff_type {
	LAG_MEMBER_DIFF_NONE,
	LAG_MEMBER_DIFF_ADD_COLL_EN_DIST_EN,
	LAG_MEMBER_DIFF_ADD_COLL_EN_DIST_DIS,
	LAG_MEMBER_DIFF_ADD_COLL_DIS_DIST_EN,
	LAG_MEMBER_DIFF_ADD_COLL_DIS_DIST_DIS,
	LAG_MEMBER_DIFF_REMOVE,
	LAG_MEMBER_DIFF_ACTIVE,
	LAG_MEMBER_DIFF_STANDBY,
	LAG_MEMBER_DIFF_COLL_ENABLE,
	LAG_MEMBER_DIFF_COLL_DISABLE,
	LAG_MEMBER_DIFF_DIST_ENABLE,
	LAG_MEMBER_DIFF_DIST_DISABLE,
	LAG_MEMBER_DIFF_BOTH_ENABLE,
	LAG_MEMBER_DIFF_BOTH_DISABLE
} lag_member_diff_type_e;

typedef struct __attribute__((__packed__)) _lag_member_diff {
	uint32_t iface_idx;
	lag_member_diff_type_e type;
#if defined(BOARD_BPE_CTRLCARD) || defined(BOARD_BPE_LINECARD)
	uint8_t module;
	uint8_t port;
	int is_aggregated;
#endif
} lag_member_diff_t;

#define LAG_MEMBERS_MAX 16

#define LAG_STATE_HDR_FIELDS \
	uint32_t iface_idx; \
	uint8_t  id; \
	uint32_t members_num

typedef struct __attribute__((__packed__)) _lag_state_hdr {
	LAG_STATE_HDR_FIELDS;
} lag_state_hdr_t;

typedef struct __attribute__((__packed__)) _lag_state {
	LAG_STATE_HDR_FIELDS;
	lag_member_diff_t members_diffs[LAG_MEMBERS_MAX];
} lag_state_t;


typedef struct _lag_lb {
	uint32_t iface_idx; /*not used for global settings*/
	uint8_t  lag_id;
	lag_lb_mode_e lb_mode;
	int lb_key;
} lag_lb_t;

typedef struct _trunk_hash {
	uint32_t iface_idx; /*not used for global settings*/
	int th_key;
} trunk_hash_t;

/*Statistics from os*/
typedef struct _count_stat_msg_spec {
	uint32_t if_index;
	uint64_t rx_packets;          /*Packets received*/
	uint64_t tx_packets;          /*Packets sent*/
	uint64_t rx_bytes;            /*Bytes received*/
	uint64_t tx_bytes;            /*Bytes sent*/
	uint64_t rx_errors;           /*Received errors*/
	uint64_t tx_errors;           /*Send errors*/
	uint64_t rx_dropped;          /*Received packets dropped*/
	uint64_t tx_dropped;          /*Packets dropped during transmit*/
	uint64_t multicast;           /*Multicast*/
	uint64_t collisions;          /*Send collisions*/
	uint64_t rx_length_errors;    /*Length errors*/
	uint64_t rx_over_errors;      /*Over errors*/
	uint64_t rx_crc_errors;       /*CRC errors*/
	uint64_t rx_frame_errors;     /*Frame errors*/
	uint64_t rx_fifo_errors;      /*Receive FIFO errors*/
	uint64_t rx_missed_errors;    /*Missed errors*/
	uint64_t tx_aborted_errors;   /*Aborted errors*/
	uint64_t tx_carrier_errors;   /*Carrier errors*/
	uint64_t tx_fifo_errors;      /*Send FIFO errors*/
	uint64_t tx_heartbeat_errors; /*Heartbeat errors*/
	uint64_t tx_window_errors;    /*Window errors*/
	uint64_t rx_compressed;       /*Compressed packets received*/
	uint64_t tx_compressed;       /*Compressed packets sent*/
	uint64_t rx_packets_rate;     /*Input Rate(packets/s) in last 300sec (average)*/
	uint64_t tx_packets_rate;     /*Output Rate(packets/s) in last 300sec (average)*/
	uint64_t rx_bits_rate;        /*Input Rate(bits!/s) in last 300sec (average)*/
	uint64_t tx_bits_rate;        /*Output Rate(bits!/s) in last 300sec (average)*/
	uint8_t  mtu;                 /*Interface MTU*/
} count_stat_msg_spec_if_t;

/*Statistics from os*/
typedef struct _sub_if_output_offset_msg_t {
	uint32_t if_index;
	uint64_t tx_packets;
	uint64_t tx_bytes;
	uint64_t tx_packets_rate;
	uint64_t tx_bits_rate;
} sub_if_output_offset_msg_t;

typedef struct _sub_if_output_offset_req_t {
	uint32_t if_index;
	uint32_t load_interval;
} sub_if_output_offset_req_t;

/*Interface status from db*/
typedef struct _if_status_msg_spec {
	uint32_t if_index;
	uint8_t hwaddr[HW_ADDR_LEN];
	uint16_t mru;
	uint16_t mtu;
	iface_spd_e  speed;
	iface_dplx_e duplex;
	iface_spd_e ability;
	iface_mediatype_e mediatype;
	struct timeval time_since_state_changed;
} status_msg_spec_if_t;

typedef enum _reserve_pw_redn_group_iface_clear_type
{
	RESERVE_PW_GROUP_IFACE_CLEAR_TYPE_BRIDGE,
	RESERVE_PW_GROUP_IFACE_CLEAR_TYPE_VFI,
	RESERVE_PW_GROUP_IFACE_CLEAR_TYPE_XCONNECT,
	RESERVE_PW_GROUP_IFACE_CLEAR_TYPE_P2P,
} reserve_pw_redn_group_iface_clear_type_e;

#define RESERVE_PW_GROUP_IFACE_CLEAR_NAME_LEN 128

typedef struct _reserve_pw_redn_group_iface_clear {
	reserve_pw_redn_group_iface_clear_type_e type;
	char subparent_name[RESERVE_PW_GROUP_IFACE_CLEAR_NAME_LEN];
	char parent_name[RESERVE_PW_GROUP_IFACE_CLEAR_NAME_LEN];
} reserve_pw_redn_group_iface_clear_t;

typedef struct _reserve_pw_redn_group_iface {
	reserve_pw_redn_group_iface_clear_t clear_data;
	network_ip_address_t pw1_peer_addr;
	network_ip_address_t pw2_peer_addr;
	uint32_t pw1_vc_index;
	uint32_t pw2_vc_index;
	uint8_t has_backup;
	uint8_t is_del_request;
} reserve_pw_redn_group_iface_t;

typedef struct _iface_errdisable_msg {
	iface_t iface;
	iface_errdisable_reason_e reason;
	uint8_t is_errdisabled;
} iface_errdisable_msg_t;

/* This struct is copy of BFDL_RX_INFO from       */
/* (n-base/delivery/code/comn/intface/bfdlextn.h) */
typedef struct _bfd_last_packet_info {
	uint8_t version;
	uint8_t diag;
	uint8_t state;
	uint8_t flags;
	uint8_t detect_multiplier;
	uint8_t length;
	uint32_t local_discriminator;
	uint32_t remote_discriminator;
	uint32_t desired_min_tx_interval;
	uint32_t required_min_rx_interval;
	uint32_t required_min_echo_rx_interval;
	time_t uptime; //  milliseconds
	uint8_t hw_sw_type; // 0 - hardware, 1 - software
	uint32_t remote_tx_interval;
	uint32_t actual_tx_interval;
	uint32_t actual_rx_interval;
} bfd_last_packet_info_t;

typedef struct _iface_mpls_info
{
	uint32_t if_index;
	uint32_t os_index;
	tunnel_type_e type;
	uint32_t owner_type;

	union
	{
		struct
		{
			network_ip_address_t pw1_peer_addr;
			network_ip_address_t pw2_peer_addr;
			uint32_t pw1_vc_index;
			uint32_t pw2_vc_index;
		} pw_group;
		struct
		{
			network_ip_address_t pw_peer_addr;
			uint32_t pw_vc_index;
			uint16_t local_ve_id;
			uint16_t remote_ve_id;
			pw_route_distinguisher_t local_rd;
			pw_route_distinguisher_t remote_rd;
		} pw;
		struct
		{
			network_ip_prefix_t fec;
			network_ip_address_t out_next_hop_addr;
			uint32_t out_label;
			uint16_t lsp_tun_id;
			uint16_t lsp_tun_instance;
		} tunnel;
		struct
		{
			network_ip_prefix_t lsp_fec;
			network_ip_address_t pe_addr;
			uint32_t out_label;
			int ve_id;
			char vrf_name[VRF_NAME_LEN];
		} l3vpn;
		struct
		{
			network_ip_address_t ingress_lsr;
			network_ip_address_t egress_lsr;
			uint16_t tun_id;
			uint16_t tun_instance;
			uint32_t lsp_xc_index;
			uint8_t is_oper_up;
		} rsvp;
	};
} iface_mpls_info_t;

typedef struct __attribute__((packed)) _iface_mpls_info_get_response
{
	uint32_t count;
	iface_mpls_info_t info[0];
} iface_mpls_info_get_response_t;

typedef struct
{
	char vrf_name[VRF_NAME_LEN];
	iface_t iface;
} ipc_msg_get_vrf_id_and_create_iface_req_t;

typedef struct
{
	uint32_t vrf_index;
	uint32_t if_index;
	uint32_t os_index;
	uint32_t rc;
} ipc_msg_get_vrf_id_and_create_iface_resp_t;

/* Interfaces params for services, that don't know if-index */
typedef struct {
	iface_type_e iface_type;
	union
	{
		struct
		{
			log_port_t log_port;
			uint8_t	 dev;
			uint8_t  unit;
		} phys;

		struct
		{
			uint8_t id;
		} lag;

		struct
		{
			uint32_t number;
			iface_type_e parent_type;
			uint32_t parent_iface_idx;
			union
			{
				struct
				{
					log_port_t	log_port;
					uint8_t		dev;
					uint8_t		unit;
				} phys;

				struct
				{
					uint8_t id;
				} lag;
			};
		} sub;
	};
} iface_id_t;

typedef struct
{
	iface_igp_shortcut_metric_type_e igp_shortcut_metric_type;
	uint32_t metric;

	uint16_t num_hops;
	network_ip_address_t nexthops[0];
} iface_rsvp_tunnel_additional_info_t;

typedef struct _iface_tunnel_props
{
	iface_t iface;
	//it must be the last member of the struct
	iface_rsvp_tunnel_additional_info_t info;
} iface_rsvp_tunnel_with_params_t;

#endif
