#ifndef __COMMON_SFP_TYPES_H_INCLUDED__
#define __COMMON_SFP_TYPES_H_INCLUDED__

#define SFP_COMMON_VENDOR_NAME_LENGTH            16
#define SFP_COMMON_SERIAL_NUMBER_LENGTH          16
#define SFP_COMMON_VENDOR_PART_NUMBER_LENGTH     16

#include <common/pp-types.h>

typedef enum _sfp_common_compliance_code
{
	SFP_COMPLIANCE_CODE_INVALID,
	SFP_COMPLIANCE_CODE_NONE,

	SFP_COMPLIANCE_CODE_10GE_SFPplus_Passive_Copper_Cable,
	SFP_COMPLIANCE_CODE_10GE_SFPplus_Active_Linear_Copper_Cable,
	SFP_COMPLIANCE_CODE_10GE_SFPplus_Active_Limiting_Copper_Cable,
	SFP_COMPLIANCE_CODE_40G_Active_Cable,
	SFP_COMPLIANCE_CODE_40GBASE_CR4,
	SFP_COMPLIANCE_CODE_40GBASE_SR4,
	SFP_COMPLIANCE_CODE_40GBASE_LR4,
	SFP_COMPLIANCE_CODE_10GBASE_ER,
	SFP_COMPLIANCE_CODE_10GBASE_LRM,
	SFP_COMPLIANCE_CODE_10GBASE_LR,
	SFP_COMPLIANCE_CODE_10GBASE_SR,
	SFP_COMPLIANCE_CODE_1000BASE_SX,
	SFP_COMPLIANCE_CODE_1000BASE_LX,
	SFP_COMPLIANCE_CODE_1000BASE_CX,
	SFP_COMPLIANCE_CODE_1000BASE_T,
	SFP_COMPLIANCE_CODE_1GE_SFP_passive_copper_cable,

	SFP_COMPLIANCE_CODE_BASE_PX,
	SFP_COMPLIANCE_CODE_BASE_BX10,
	SFP_COMPLIANCE_CODE_100BASE_FX,
	SFP_COMPLIANCE_CODE_100BASE_LX_10,
	SFP_COMPLIANCE_CODE_SOME_FIBER_1000SFP,
	SFP_COMPLIANCE_CODE_SOME_FIBER_10GSFP,

	SFP_COMPLIANCE_CODE_100G_AOC,
	SFP_COMPLIANCE_CODE_100GBASE_SR4,
	SFP_COMPLIANCE_CODE_100GBASE_LR4,
	SFP_COMPLIANCE_CODE_100GBASE_ER4,
	SFP_COMPLIANCE_CODE_100GBASE_SR10,
	SFP_COMPLIANCE_CODE_100G_CWDM4_MSA_FEC,
	SFP_COMPLIANCE_CODE_100G_PSM4_SMF,
	SFP_COMPLIANCE_CODE_100G_ACC,
	SFP_COMPLIANCE_CODE_100G_CWDM4_MSA,
	SFP_COMPLIANCE_CODE_100GBASE_CR4,
	SFP_COMPLIANCE_CODE_40GBASE_ER4,
	SFP_COMPLIANCE_CODE_4_x_10GBASE_SR,
	SFP_COMPLIANCE_CODE_40G_PSM4_SMF,
	SFP_COMPLIANCE_CODE_P1I1_2D1,
	SFP_COMPLIANCE_CODE_P1S1_2D2,
	SFP_COMPLIANCE_CODE_P1L1_2D2,
	SFP_COMPLIANCE_CODE_10GBASE_T_SFI,

	SFP_COMPLIANCE_CODE_UNKNOWN
} sfp_common_compliance_code_e;

typedef enum _sfp_common_qsfp28_module_capability
{
	SFP_MOD_CAP_40G_AOC = 0,
	SFP_MOD_CAP_40GBASE_LR4,
	SFP_MOD_CAP_40GBASE_SR4,
	SFP_MOD_CAP_40GBASE_CR4,
	SFP_MOD_CAP_10GBASE_SR,
	SFP_MOD_CAP_10GBASE_LR,
	SFP_MOD_CAP_10GBASE_LRM
} sfp_common_qsfp28_mod_cap;

typedef enum _sfp_common_alarm_flags
{
	param_normal,
	param_high_alarm,
	param_low_alarm,
	param_high_warn,
	param_low_warn,
	param_not_set
} sfp_common_alarm_flags_e;

typedef enum _calibr_common_type
{
	calibr_internal,
	calibr_external,
	calibr_none_err
} calibr_common_type_e;

typedef enum _state_flag_common
{
	state_no,
	state_yes,
	state_unknown,
	state_not_supported
} state_flag_common_e;

typedef enum _rx_pwr_common_type
{
	rx_oma,
	rx_average
} rx_pwr_common_type_e;

typedef enum _sfp_common_speed
{
	sfp_speed_10M       = 10,
	sfp_speed_100M      = 100,
	sfp_speed_1G        = 1000,
	sfp_speed_10G       = 10000,
	sfp_speed_40G       = 40000,
	sfp_speed_100G      = 100000,
	sfp_err             = -1
} sfp_common_speed_e;

typedef enum _sfp_common_connector_type
{
	sfp_connector_Unknown                       = 0x0,
	sfp_connector_SC                            = 0x1,
	sfp_connector_Fibre_Channel_Style_1_copper  = 0x2,
	sfp_connector_Fibre_Channel_Style_2_copper  = 0x3,
	sfp_connector_BNC_TNC                       = 0x4,
	sfp_connector_Fibre_Channel_coaxial         = 0x5,
	sfp_connector_Fibre_Jack                    = 0x6,
	sfp_connector_LC                            = 0x7,
	sfp_connector_MT_RJ                         = 0x8,
	sfp_connector_MU                            = 0x9,
	sfp_connector_SG                            = 0xa,
	sfp_connector_Optical_pigtail               = 0xb,
	sfp_connector_MPO_Parallel_Optic            = 0xc,
	sfp_connector_MPO_2x16                      = 0xd,
	sfp_connector_HSSDCII                       = 0x20,
	sfp_connector_Copper_pigtail                = 0x21,
	sfp_connector_RJ45                          = 0x22,
	sfp_connector_Not_separable                 = 0x23,
	sfp_connector_MXC_2x16                      = 0x24,
	sfp_connector_Unallocated                   = 0x25,
	sfp_connector_Vendor_specific               = 0x80
} sfp_common_connector_type_e;

typedef enum _sfp_common_transceiver_type
{
	sfp_type_Unknown,
	sfp_type_Fiber,
	sfp_type_Copper,
	sfp_type_Dirrect_Attach,
	sfp_type_AC_40G
} sfp_common_transceiver_type_e;

typedef enum _sfp_common_type
{
	type_transceiver_Unknown                          = 0x00,
	type_transceiver_GBIC                             = 0x01,
	type_transceiver_Motherboard_soldered             = 0x02,
	type_transceiver_SFP_SFP_plus                     = 0x03,
	type_transceiver_300pin_XBI                       = 0x04,
	type_transceiver_XENPAK                           = 0x05,
	type_transceiver_XFP                              = 0x06,
	type_transceiver_XFF                              = 0x07,
	type_transceiver_XFP_E                            = 0x08,
	type_transceiver_XPAK                             = 0x09,
	type_transceiver_X2                               = 0x0a,
	type_transceiver_DWDM_SFP_SFP_plus                = 0x0b,
	type_transceiver_QSFP                             = 0x0c,
	type_transceiver_QSFP_plus                        = 0x0d,
	type_transceiver_CXP                              = 0x0e,
	type_transceiver_Shielded_mini_multilane_HD_4x    = 0x0f,
	type_transceiver_Shielded_mini_multilane_HD_8x    = 0x10,
	type_transceiver_QSFP28                           = 0x11,
	type_transceiver_CXP2                             = 0x12,
	type_transceiver_CDFP                             = 0x13,
	type_transceiver_Shielded_mini_multilane_HD_4x_f  = 0x14,
	type_transceiver_Shielded_mini_multilane_HD_8x_f  = 0x15,
	type_transceiver_last                             = 0x16,

	type_transceiver_Reserved                         = 0x7f,
	type_transceiver_Vendor_specific                  = 0x80
} sfp_common_type_e;

typedef enum _sfp_common_length_units
{
	length_units_Unknown,
	length_units_1km,
	length_units_100m,
	length_units_10m,
	length_units_2m,
	length_units_1m,
} sfp_common_length_units_e;

typedef struct _sfp_common_diagnostic_dat
{
	/*Digital diagnostic monitoring values*/
	double temp;                                            //temp in Celsius
	double vcc;                                             //vcc in V
	double current[4];                                      //mA
	double tx_power[4];                                     //mW
	double tx_power_dBm[4];                                 //dBm
	double rx_power[4];                                     //mW
	double rx_power_dBm[4];                                 //dBm

	/*alarms*/
	sfp_common_alarm_flags_e temp_tres;
	sfp_common_alarm_flags_e vcc_tres;
	sfp_common_alarm_flags_e cur_tres[4];
	sfp_common_alarm_flags_e rx_tres[4];
	sfp_common_alarm_flags_e tx_tres[4];

	/*current state flags*/
	state_flag_common_e disabled[4];
	state_flag_common_e signal_lost[4];
	state_flag_common_e is_fault[4];

	/*supporting*/
	state_flag_common_e supports_dom;
	state_flag_common_e supports_alarms;

	/*HW types*/
	sfp_common_type_e type;
	sfp_common_transceiver_type_e transceiver_type;
	sfp_common_connector_type_e connector_type;
	sfp_common_compliance_code_e compliance_code;
	sfp_common_qsfp28_mod_cap qsfp_mod_cap;
	rx_pwr_common_type_e rxpwr_type;
	calibr_common_type_e calibr_type;

	/*HW parameters*/
	sfp_common_speed_e speed;
	uint16_t wave_length;
	uint8_t distance;                                    //nm
	sfp_common_length_units_e dist_units;

	char serial_num[SFP_COMMON_SERIAL_NUMBER_LENGTH];
	char vendor[SFP_COMMON_VENDOR_NAME_LENGTH];
	char vendor_pn[SFP_COMMON_VENDOR_PART_NUMBER_LENGTH];

	uint32_t if_index;
} sfp_common_diagnostic_dat_t;

//used only not for PIZZABOX devices
typedef struct
{
	uint8_t is_up;
	uint8_t port;
	sfp_common_transceiver_type_e type;
	port_type_e port_type;
} sfp_ipc_state_changed_t;

//used only not for PIZZABOX devices
typedef struct
{
	uint8_t port_count;
	sfp_ipc_state_changed_t status[MAX_PORTS_PER_BOARD];
} sfp_ipc_states_changed_t;

inline static const char * sfp_transceiver_type_to_string(sfp_common_transceiver_type_e type)
{
    switch (type)
    {
        case sfp_type_Unknown:
        default:
            return "Unknown";
        case sfp_type_Fiber:
            return "Fiber";
        case sfp_type_Copper:
            return "Copper";
        case sfp_type_Dirrect_Attach:
            return "Direct attach";
        case sfp_type_AC_40G:
            return "Active cable (Fiber/DAC)";
    }
    return "Unknown";
}

#endif
