#!/bin/bash


base_common=$1
file=$2

echo "last build hashes" > $file

echo "base" >> $file
git log | head -n 1 >> $file

cd bcm-sdk-cpu
echo "bcm-sdk-cpu" >> $file
git log | head -n 1 >> $file

cd $base_common/drivers
echo "drivers" >> $file
git log | head -n 1 >> $file

cd $base_common/modules/3party-services
echo "3party-services" >> $file
git log | head -n 1 >> $file

cd $base_common/modules/bcm-sdk-net
echo "bcm-sdk-net" >> $file
git log | head -n 1 >> $file

cd $base_common/modules/eltex-netconf
echo "eltex-netconf" >> $file
git log | head -n 1 >> $file

cd $base_common/modules/eltex-services
echo "eltex-services" >> $file
git log | head -n 1 >> $file

cd $base_common/modules/n-base
echo "n-base" >> $file
git log | head -n 1 >> $file
