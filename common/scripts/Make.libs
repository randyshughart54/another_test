ifeq ($(LIB_TYPE), static)
 LIB_EXT = a
else
 ifeq ($(LIB_TYPE), shared)
  LIB_EXT = so
 else
  $(error "Invalid LIB_TYPE value('$(LIB_TYPE)'), should be 'static' or 'shared'")
 endif
endif

INSTALL_PATH ?= /usr/lib

LIB_NAME = lib$(LIB_BASE).$(LIB_EXT)

BUILD_DIR = build/$(BOARD_NAME)

CFLAGS += $(GLOBAL_CFLAGS)

CFLAGS += -Werror -D$(BOARD_FAMILY) -D$(BOARD_TYPE) -D$(COMPILE_BOARD_NAME) -MP -MD
IFLAGS += -I$(INCLUDE_PATH)

ifeq ($(LIB_TYPE), shared)
CFLAGS += -fPIC
endif

SUB_OBJS_C = $(SUB_SRCS:.c=.o)
SUB_OBJS = $(SUB_OBJS_C:.S=.o)
SUB_OBJS_PATH = $(addprefix $(BUILD_DIR)/,$(SUB_OBJS))

VERSION_DIR = $(LIBS_PATH)/../version
VERSION_OBJ = $(VERSION_DIR)/$(BUILD_DIR)/version.o

all: version $(BUILD_DIR)/$(LIB_NAME) | pre-build

# If $(LINK_DEPENDENCIES) is empty, link library every `make all` invocation.
# Otherwise, link library only if $(LINK_DEPENDENCIES) changed.
ifeq ($(LINK_DEPENDENCIES),)
 .PHONY: $(BUILD_DIR)/$(LIB_NAME)
else
LINK_DEPENDENCIES_FILE = $(BUILD_DIR)/lib$(LIB_BASE).lib-d
.PHONY: force
$(LINK_DEPENDENCIES_FILE): force
	@echo '$(LINK_DEPENDENCIES)' | cmp -s - $@ || echo '$(LINK_DEPENDENCIES)' > $@
endif

$(BUILD_DIR)/$(LIB_NAME): $(VERSION_OBJ) $(SUB_OBJS_PATH) $(LINK_DEPENDENCIES) $(LINK_DEPENDENCIES_FILE) | pre-build version
ifeq ($(LIB_TYPE), shared)
	$(CC) $(CFLAGS) -shared -Wl,-soname,$(LIB_NAME) -rdynamic -o $(BUILD_DIR)/$(LIB_NAME) $(VERSION_OBJ) $(SUB_OBJS_PATH) -nostartfiles $(LDFLAGS)
else
	rm -f $(BUILD_DIR)/$(LIB_NAME)
	$(AR) -qc $(BUILD_DIR)/$(LIB_NAME) $(VERSION_OBJ) $(SUB_OBJS_PATH)
endif

-include $(SUB_OBJS_PATH:.o=.d)

$(BUILD_DIR)/%.o: %.c | $(BUILD_DIR) pre-build
	$(CC) $(CFLAGS) $(IFLAGS) -c $< -o $@

$(BUILD_DIR)/%.o: %.S | $(BUILD_DIR) pre-build
	$(CC) $(CFLAGS) $(IFLAGS) -c $< -o $@

$(BUILD_DIR):
	mkdir -p $(BUILD_DIR)

.PHONY: version
version: | pre-build
	$(MAKE) -C $(VERSION_DIR) all

.PHONY: pre-build
pre-build:
	@printf "\n[ Building '\033[1m$(LIB_BASE)\033[0m' library as \033[1m$(LIB_TYPE)\033[0m ]\n"

.PHONY: clean
clean:
	@printf "\n[ Cleaning '\033[1m$(LIB_BASE)\033[0m' library ]\n"
	-rm -Rf $(BUILD_DIR)

.PHONY: fs
fs:
	@printf "\n[ Installing '\033[1m$(LIB_BASE)\033[0m' library ]\n"
	$(ROMFSINST) $(BUILD_DIR)/$(LIB_NAME) $(INSTALL_PATH)/$(LIB_NAME)
