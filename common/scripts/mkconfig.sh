#!/bin/bash

CFG=config.mk
if [ -s "$CFG" ]; then
	echo "Error: '$CFG' already configurated. 'make unconfig' must be run first."
	exit 1
fi

ROMFSDIR=$(pwd)/.rootfs
ROMROOTFSDIR=$(pwd)/.rootfs
BOARD_PATH=$(pwd)
BOARD_NAME=$1
COMPILE_BOARD_NAME=BOARD_${BOARD_NAME^^}
BOARD_FAMILY=BOARD_BPE_FAMILY
case "$1" in
	"fmc16")
		BOARD_TYPE=BOARD_BPE_CTRLCARD
		ROMFSINST=$(pwd)/../../common/scripts/romfs-inst.sh
		OUTPUT_PATH=$(pwd)/../out/
		INCLUDE_PATH=$(pwd)/../../common/include
		LIBS_PATH=$(pwd)/../../common/libs
		SCRIPTS_PATH=$(pwd)/../../common/scripts
		NETCONF_MAIN_DIRECTORY=$(pwd)/../../common/modules/eltex-netconf
		COMMON_ROOTFS_PATH=$(pwd)/../../common/rootfs
		KERN_INCLUDE_PATH=$(pwd)/bcm-sdk-cpu/linux/include
		KERN_EVP_PATH=$(pwd)/bcm-sdk-cpu/linux/evp
		TOOLCHAIN_PATH=$(pwd)/bcm-sdk-cpu/sdk-base/buildroot/output/build/toolchains_bin
		GLOBAL_CFLAGS="-mabi=32"
		BUILDTYPE="XLP3XX"
		KERNEL="3.10.59"
		ARCH="mips"
		HOST="mips64-nlm-linux"
		CROSS_COMPILE="mips64-nlm-linux-"
		PATH=$PATH
		BUILD_CCACHE_PATH=$BUILD_CCACHE_PATH
		;;
	"lc18")
		BOARD_TYPE=BOARD_BPE_LINECARD
		ROMFSINST=$(pwd)/../../common/scripts/romfs-inst.sh
		OUTPUT_PATH=$(pwd)/../out/
		INCLUDE_PATH=$(pwd)/../../common/include
		LIBS_PATH=$(pwd)/../../common/libs
		SCRIPTS_PATH=$(pwd)/../../common/scripts
		NETCONF_MAIN_DIRECTORY=$(pwd)/../../common/modules/eltex-netconf
		COMMON_ROOTFS_PATH=$(pwd)/../../common/rootfs
		KERN_INCLUDE_PATH=$(pwd)/bcm-sdk-cpu/linux/include
		KERN_EVP_PATH=$(pwd)/bcm-sdk-cpu/linux/evp
		TOOLCHAIN_PATH=$(pwd)/bcm-sdk-cpu/sdk-base/buildroot/output/build/toolchains_bin
		GLOBAL_CFLAGS="-mabi=32"
		BUILDTYPE="XLP2XX"
		KERNEL="3.10.59"
		ARCH="mips"
		HOST="mips64-nlm-linux"
		CROSS_COMPILE="mips64-nlm-linux-"
		PATH=$PATH
		BUILD_CCACHE_PATH=$BUILD_CCACHE_PATH
		;;
	"me5100")
		BOARD_TYPE=BOARD_BPE_PIZZABOX
		ROMFSINST=$(pwd)/../common/scripts/romfs-inst.sh
		OUTPUT_PATH=$(pwd)/out/
		INCLUDE_PATH=$(pwd)/../common/include
		LIBS_PATH=$(pwd)/../common/libs
		SCRIPTS_PATH=$(pwd)/../common/scripts
		NETCONF_MAIN_DIRECTORY=$(pwd)/../common/modules/eltex-netconf
		COMMON_ROOTFS_PATH=$(pwd)/../common/rootfs
		KERN_INCLUDE_PATH=$(pwd)/bcm-sdk-cpu/linux/include
		KERN_EVP_PATH=$(pwd)/bcm-sdk-cpu/linux/evp
		TOOLCHAIN_PATH=$(pwd)/bcm-sdk-cpu/sdk-base/buildroot/output/build/toolchains_bin
		GLOBAL_CFLAGS="-mabi=32"
		BUILDTYPE="XLP3XX"
		KERNEL="3.10.59"
		ARCH="mips"
		HOST="mips64-nlm-linux"
		CROSS_COMPILE="mips64-nlm-linux-"
		PATH=$PATH
		BUILD_CCACHE_PATH=$BUILD_CCACHE_PATH
		;;
	"me5200")
		BOARD_TYPE=BOARD_BPE_PIZZABOX
		ROMFSINST=$(pwd)/../common/scripts/romfs-inst.sh
		OUTPUT_PATH=$(pwd)/out/
		INCLUDE_PATH=$(pwd)/../common/include
		LIBS_PATH=$(pwd)/../common/libs
		SCRIPTS_PATH=$(pwd)/../common/scripts
		NETCONF_MAIN_DIRECTORY=$(pwd)/../common/modules/eltex-netconf
		COMMON_ROOTFS_PATH=$(pwd)/../common/rootfs
		KERN_INCLUDE_PATH=$(pwd)/bcm-sdk-cpu/linux/include
		KERN_EVP_PATH=$(pwd)/bcm-sdk-cpu/linux/evp
		TOOLCHAIN_PATH=$(pwd)/bcm-sdk-cpu/sdk-base/buildroot/output/build/toolchains_bin
		GLOBAL_CFLAGS="-mabi=32"
		BUILDTYPE="XLP3XX"
		KERNEL="3.10.59"
		ARCH="mips"
		HOST="mips64-nlm-linux"
		CROSS_COMPILE="mips64-nlm-linux-"
		PATH=$TOOLCHAIN_PATH/mipscross/linux/bin:$PATH
		;;
	"sim")
		BOARD_TYPE=BOARD_SIM
		ROMFSINST=$(pwd)/../common/scripts/romfs-inst.sh
		OUTPUT_PATH=$(pwd)/out/
		INCLUDE_PATH=$(pwd)/../common/include
		LIBS_PATH=$(pwd)/../common/libs
		SCRIPTS_PATH=$(pwd)/../common/scripts
		NETCONF_MAIN_DIRECTORY=$(pwd)/../common/modules/eltex-netconf
		COMMON_ROOTFS_PATH=$(pwd)/../common/rootfs
		KERN_EVP_PATH=
		KERN_INCLUDE_PATH=
		TOOLCHAIN_PATH=
		GLOBAL_CFLAGS=
		BUILDTYPE="SIM"
		KERNEL="3.10.59"
		ARCH=
		HOST=
		CROSS_COMPILE=
		;;
	*)
		echo "Unknown board '$1'"
		exit 1
		;;
esac


echo "" >> "${CFG}"
echo "# Generated automatically - do not edit"            >> ${CFG}
echo "export BUILDTYPE                := ${BUILDTYPE}"          >> ${CFG}
echo "export KERNEL                   := ${KERNEL}"             >> ${CFG}
echo "export ARCH                     := ${ARCH}"               >> ${CFG}
echo "export HOST                     := ${HOST}"               >> ${CFG}
echo "export BOARD_NAME               := ${BOARD_NAME}"         >> ${CFG}
echo "export BOARD_FAMILY             := ${BOARD_FAMILY}"       >> ${CFG}
echo "export BOARD_TYPE               := ${BOARD_TYPE}"         >> ${CFG}
echo "export COMPILE_BOARD_NAME       := ${COMPILE_BOARD_NAME}" >> ${CFG}
echo "export CROSS_COMPILE            := ${CROSS_COMPILE}"      >> ${CFG}
echo "export CC                       := ${CROSS_COMPILE}gcc"   >> ${CFG}
echo "export AR                       := ${CROSS_COMPILE}ar"    >> ${CFG}
echo "export GLOBAL_CFLAGS            := ${GLOBAL_CFLAGS}"      >> ${CFG}
echo "export INCLUDE_PATH             := ${INCLUDE_PATH}"       >> ${CFG}
echo "export LIBS_PATH                := ${LIBS_PATH}"          >> ${CFG}
echo "export SCRIPTS_PATH             := ${SCRIPTS_PATH}"       >> ${CFG}
echo "export NETCONF_MAIN_DIRECTORY   := ${NETCONF_MAIN_DIRECTORY}"       >> ${CFG}
echo "export COMMON_ROOTFS_PATH       := ${COMMON_ROOTFS_PATH}" >> ${CFG}
echo "export KERN_INCLUDE_PATH        := ${KERN_INCLUDE_PATH}"  >> ${CFG}
echo "export KERN_EVP_PATH            := ${KERN_EVP_PATH}"      >> ${CFG}
echo "export BOARD_PATH               := ${BOARD_PATH}"         >> ${CFG}
echo "export ROMFSDIR                 := ${ROMFSDIR}"           >> ${CFG}
echo "export ROMROOTFSDIR             := ${ROMROOTFSDIR}"       >> ${CFG}
echo "export ROMFSINST                := ${ROMFSINST}"          >> ${CFG}
echo "export TOOLCHAIN_PATH           := ${TOOLCHAIN_PATH}"     >> ${CFG}
echo "export OUTPUT_PATH              := ${OUTPUT_PATH}"        >> ${CFG}
echo "export PATH                     := ${PATH}"               >> ${CFG}
echo "export BUILD_CCACHE_PATH        := ${BUILD_CCACHE_PATH}"  >> ${CFG}
