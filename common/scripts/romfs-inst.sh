#!/bin/bash
#
# A tool to simplify Makefiles that need to put something
# into the ROMFS
#
# Copyright (C) David McCullough, 2002,2003
#
#############################################################################

set -e

# Provide a default PATH setting to avoid potential problems...
PATH="/bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:$PATH"

usage()
{
cat << !EOF >&2
$0: [options] [src] dst
    -v          : output actions performed.
    -e env-var  : only take action if env-var is set to "y".
    -o option   : only take action if option is set to "y".
    -p perms    : chmod style permissions for dst.
    -a text     : append text to dst.
	-A pattern  : only append text if pattern doesn''t exist in file
    -l link     : dst is a link to 'link'.
    -s sym-link : dst is a sym-link to 'sym-link'.

    if "src" is not provided,  basename is run on dst to determine the
    source in the current directory.

	multiple -e and -o options are ANDed together.  To achieve an OR affect
	use a single -e/-o with 1 or more y/n/"" chars in the condition.

	if src is a directory,  everything in it is copied recursively to dst
	with special files removed (currently .svn dirs).
!EOF
	exit 1
}

#############################################################################

setperm()
{
	if [ "$perm" ]
	then
		[ "$v" ] && echo "chmod ${perm} ${1}"
		chmod ${perm} ${1}
	fi
}

#############################################################################

setowner()
{
	if [ "$user" ]
	then
		[ "$v" ] && echo "chown ${user} ${1}"
		chown ${user} ${1}
	fi
}


#############################################################################

file_copy()
{
	files=$(ls ${src} 2> /dev/null | wc -l)
	if ! [ x"$files" != x"0" ]
	then
		echo "File or directory ${src} doesn't exist!"
		exit 1
	fi

	if [ -d "${src}" ]
	then
		[ "$v" ] && echo "CopyDir ${src} ${ROMFSDIR}${dst}"
		(
			if [ ! -z "$perm" ]; then
				echo "Setting permissions on directories not supported!"
				exit 1
			fi
			cd ${src}
			V=
#			[ "$v" ] && V=v
#			find . -wholename '*/.svn' -prune -o -print0 | cpio --null -p${V}dumL ${ROMFSDIR}${dst}
			find . -wholename '*/.svn' -prune -o -print0 | cpio --null -p${V}dum ${ROMFSDIR}${dst}
			[ -d ${ROMFSDIR}${dst} ] || mkdir ${ROMFSDIR}${dst}
		)
	else
		dstdir=""
		dstfiles=""
		dst_is_dir="no"
		[ -d "${ROMFSDIR}${dst}" ] && dst_is_dir="yes"
		[ x"$files" != x"1" ] && dst_is_dir="yes"
		[ ${dst:$(expr ${#dst}-1):1} = "/" ] && dst_is_dir="yes"
		if [ $dst_is_dir = "no" ]
		then
			dstdir=$(dirname ${ROMFSDIR}${dst})
			dstfiles=${ROMFSDIR}${dst}
			rm -f ${ROMFSDIR}${dst}
		else
			dstdir=${ROMFSDIR}${dst}
			for file in ${src}
			do
				dstfile=${dstdir}/$(basename ${file})
				dstfiles="${dstfiles} ${dstfile}"
				rm -f ${dstfile}
			done
		fi

		[ "$v" ] && echo "mkdir -p ${dstdir}"
		mkdir -p ${dstdir}
		if ! [ -d "${ROMFSDIR}${dst}" ]
		then
			[ "$v" ] && echo "cp -pd ${src} ${dstfiles}"
			cp -pd ${src} ${dstfiles}
		else
			[ "$v" ] && echo "cp -pd ${src} ${dstdir}"
			cp -pd ${src} ${dstdir}
		fi
		setowner "${dstfiles}"
		setperm "${dstfiles}"
	fi
}

#############################################################################

file_append()
{
	touch ${ROMFSDIR}${dst}
	if [ -z "${pattern}" ] && grep -F "${src}" ${ROMFSDIR}${dst} > /dev/null
	then
		[ "$v" ] && echo "File entry already installed."
	elif [ "${pattern}" ] && egrep "${pattern}" ${ROMFSDIR}${dst} > /dev/null
	then
		[ "$v" ] && echo "File pattern already installed."
	else
		[ "$v" ] && echo "Installing entry into ${ROMFSDIR}${dst}."
		echo "${src}" >> ${ROMFSDIR}${dst}
	fi
	setperm ${ROMFSDIR}${dst}
}

#############################################################################

hard_link()
{
	rm -f ${ROMFSDIR}${dst}
	[ "$v" ] && echo "ln ${src} ${ROMFSDIR}${dst}"
	ln ${src} ${ROMFSDIR}${dst}
}

#############################################################################

sym_link()
{
	rm -f ${ROMFSDIR}${dst}
	[ "$v" ] && echo "ln -s ${src} ${ROMFSDIR}${dst}"
	ln -s ${src} ${ROMFSDIR}${dst}
}

#############################################################################
#
# main program entry point
#

if [ -z "$ROMFSDIR" ]
then
	echo "ROMFSDIR is not set" >&2
	usage
	exit 1
fi

v="1"
option=y
pattern=
perm=
func=file_copy
src=
dst=
user=

while getopts 've:o:A:p:a:l:s:u:' opt "$@"
do
	case "$opt" in
	v) v="1";                           ;;
	o) option="$OPTARG";                ;;
	e) eval option=\"\$$OPTARG\";       ;;
	p) perm="$OPTARG";                  ;;
	a) src="$OPTARG"; func=file_append; ;;
	A) pattern="$OPTARG";               ;;
	l) src="$OPTARG"; func=hard_link;   ;;
	s) src="$OPTARG"; func=sym_link;    ;;
	u) user="$OPTARG";                  ;;

	*)  break ;;
	esac
#
#	process option here to get an ANDing effect
#
	case "$option" in
	*[yY]*) # this gives OR effect, ie., nYn
		;;
	*)
		[ "$v" ] && echo "Condition not satisfied."
		exit 0
		;;
	esac
done

shift `expr $OPTIND - 1`

case $# in
1)
	dst="$1"
	if [ -z "$src" ]
	then
		src="`basename $dst`"
	fi
	;;
2)
	if [ ! -z "$src" ]
	then
		echo "Source file already provided" >&2
		exit 1
	fi
	src="$1"
	dst="$2"
	;;
*)
	usage
	;;
esac

if [ `dirname "$dst"` = "." ]; then
	echo "Destination must be an absolute pathname!" >&2
	exit 1
fi



$func

exit 0

#############################################################################
