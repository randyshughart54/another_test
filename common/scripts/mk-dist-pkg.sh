#!/bin/bash
#

progname=`basename $0`

BOARD_NAME=$1
BASE_DIR=$2
FS_OUT_DIR=$3
COMPAT_MODE=$4

product_name=me5k

VERSION_DIR=$BASE_DIR/common/version

FS_ARCHIVE=fs.tgz
FS_OPENSSL=fs

PROD_HDR_V1=1

PROD_HDR_V_LAST=$PROD_HDR_V1

PRODUCT_HEADER=product.header.tmp

if [ "$COMPAT_MODE" == "compat" ]; then
	PKG_SUFFIX=$product_name
else
	PKG_SUFFIX=$BOARD_NAME
fi

run()
{
	echo
	echo "$@"

	"$@"
	local RC=$?
	if [ $RC -ne 0 ]; then
		echo
		echo "$progname: \"$@\" failed"
		exit 1
	fi
}

# uimage-multi = kernel + initrd:
#
mk_kernel_uimage_sim()
{
	local KERNEL_VERSION="3.13.0-68-generic"

	local kernel=./vmlinuz-$KERNEL_VERSION
	local initrd_img=./initrd.img-$KERNEL_VERSION

	[ -f $kernel ] || {
		echo "file not found: $kernel"
		exit 1
	}

	[ -f $initrd_img ] || {
		echo "file not found: $initrd_img"
		exit 1
	}

	#TODO vmlinuz-initrd_$product_version.multi-uimage
	vmlinuz_initrd_multi_uimage=./vmlinuz-initrd.multi-uimage

	# cp /boot/$initrd_img ./initrd_img.gz
	# file ./initrd_img.gz
	# mkdir initrd
	# pushd initrd
	# gunzip -c -9 ../initrd_img.gz | cpio -i -d -H newc --no-absolute-filenames
	# ... edit/add files
	# find . | cpio -R 0:0 -o -H newc | gzip >../initrd_img
	# popd

	#TODO install to /usr/bin/
	local MKIMAGE=./mkimage

	run $MKIMAGE -A x86 -O linux -T kernel -C none -n "$kernel" -d $kernel $kernel.uimage || exit 1

	run $MKIMAGE -A x86 -O linux -T multi -C none -n "$buildtag_sp" -d $initrd_img:$kernel.uimage $vmlinuz_initrd_multi_uimage || exit 1
}

mk_fs_distro_sim()
{
	local ROOTFS_DIR=$FS_OUT_DIR/../../.rootfs/

	#TODO ugly fix:
	pushd $ROOTFS_DIR/ >/dev/null
	rm -rf ./etc/sudoers ./etc/rc.d/ ./etc/resolv.conf
	popd >/dev/null

	run tar -C $ROOTFS_DIR -czvf $FS_ARCHIVE etc/ usr/ || exit 1
}

mk_firmware_pkg_sim()
{
	echo "create distribution package: $firmware..."

	echo "$vmlinuz_initrd_multi_uimage $FS_ARCHIVE | cpio -R 0:0 -o -H newc >$firmware"
	ls $vmlinuz_initrd_multi_uimage $FS_ARCHIVE | cpio -R 0:0 -o -H newc >$firmware
}

mk_product_header_me5k()
{
	echo "create header: $PRODUCT_HEADER..."

	local timestamp=$(date +%d-%m-%Y\ %H:%M:%S)
	local FS_ARCHIVE_SUM=`cksum $FS_ARCHIVE | awk '{print \$1}'`

	echo "#$PROD_HDR_V_LAST $BOARD_NAME $buildtag $FS_ARCHIVE_SUM $timestamp" >$PRODUCT_HEADER
}

mk_firmware_pkg_me5k()
{
	echo "create distribution package: $firmware..."

	$BASE_DIR/common/libs/openssl/build/sim/install/bin/openssl enc -e -aes256 -k "eltexme5kC1sc0K1ller" -in $FS_ARCHIVE -out $FS_OPENSSL

	if [ "$COMPAT_MODE" == "compat" ]; then
		echo "ls $FS_OPENSSL | cpio -R 0:0 -o -H newc >$firmware"
		ls $FS_OPENSSL | cpio -R 0:0 -o -H newc >$firmware

		echo "cksum $firmware | awk '{print \$1}' > $firmware.cksum"
		cksum $firmware | awk '{print $1}' > $firmware.cksum
	else
		echo "ls $PRODUCT_HEADER $FS_OPENSSL | cpio -R 0:0 -o -H crc | sed '\$d' > $firmware"
		ls $PRODUCT_HEADER $FS_OPENSSL | cpio -R 0:0 -o -H crc | sed '$d' > $firmware
	fi
}

# main
#

#TODO param kernel/fs/all

[ -f $VERSION_DIR/buildtag ] || {
	echo "file not found: $VERSION_DIR/buildtag"
	exit 1
}

[ -d $FS_OUT_DIR/ ] || {
	echo "directory not found: $FS_OUT_DIR/"
	exit 1
}

buildtag=`cat $VERSION_DIR/buildtag`
buildtag_sp=`echo $buildtag | tr '.' ' '`

firmware=firmware_$buildtag.$PKG_SUFFIX

pushd $FS_OUT_DIR/ >/dev/null

if [ "$BOARD_NAME" == sim ]; then
	mk_kernel_uimage_sim
	mk_fs_distro_sim
	mk_firmware_pkg_sim
else
	if [ "$COMPAT_MODE" != "compat" ]; then
		mk_product_header_me5k
	fi
	mk_firmware_pkg_me5k
fi

popd >/dev/null

