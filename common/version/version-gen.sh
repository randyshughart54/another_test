#!/bin/sh
#

progname=`basename $0`

LINE_PREXIX="VINF:"

prepend()
{
	printf '\\n%s%s' "$LINE_PREXIX" "$1"
}

# main
#

if [ $# -ne 2 ]; then
    echo "usage: $0 buildtag-file comment..."
    exit 1
fi

buildtag="$1"
comment="$2"

BUILD_TAG=`cat $buildtag`
BUILD_TAG=`prepend $BUILD_TAG`

time=`prepend "$(env LC_TIME=\"en_US.UTF-8\" date +'%d-%b-%Y %H:%M:%S')"`
host=`hostname`
user_host=`prepend $USER\@$host`
comment=`prepend "$comment"`
#TODO multi-line comments

# Note: the prod_X* are there on purpose - this makes the core files
# to include this information, helping during debug and support.
#
cat > version.inc << EOF
const char prod_Xbuildinfo[] = "${BUILD_TAG}$time$user_host\\n";
const char prod_Xbuildcomments[] = "${comment}";
const char* const prod_build_info = prod_Xbuildinfo;
const char* const prod_build_comments = prod_Xbuildcomments;
EOF

