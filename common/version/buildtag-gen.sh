#!/bin/sh
#

# Generate build tag based on version numbers (major/minor/micro) and build revision
#

progname=`basename $0`

get_version()
{
    local rc=`grep -E '^[[:blank:]]*#define[[:blank:]]+'"$1"'[[:blank:]]+' "$2" 2>&1 | awk -F' ' '{ print $3 }' 2>&1`
    if [ $? -ne 0 ]; then
        echo "$progname: failed to extract \"$1\" value from preprocessor file \"$2\""
        exit 1
    fi

    if ! [ $rc -eq $rc 2>/dev/null ]; then
        echo "$progname: extracted value \"$rc\" is not a number"
        exit 1
    fi

    echo $rc
}

get_rev_from_file()
{
	local filename=$1
	local bump=$2

	[ -f $filename ] || {
		echo "$progname: file not found: $filename"
		exit 1
	}

	touch $filename

	if [ "x$bump" = "xyes" ]; then
		local num=`cat $filename`
		if ! [ $num -eq $num 2>/dev/null ]; then
			echo "$progname: file \"$filename\" does not contain number"
			exit 1
		fi
		rev=`awk 'BEGIN { x = 1 } { x = $1 + 1; exit } END { print x }' $filename`
		echo $rev > $filename
	else
		rev=`awk 'BEGIN { x = 1 } { x = $1; exit } END { print x }' $filename`
	fi

	echo $rev
}

# main
#

if [ $# -ne 3 ]; then
    echo "usage: $0 input-version-file input-buildrev-file output-buildtag-file"
    exit 1
fi

version_file="$1"
buildrev_file="$2"
buildtag_file="$3"

bump_build=no
#bump_build=yes

#while [ $# -gt 0 ]; do
#	case "$1" in
#	-b)
#		shift
#		bump_build=yes
#		;;
#	*)
#		echo "$progname: bad arguments" 1>&2
#		exit 1
#		;;
#	esac
#
#	shift
#done

ver_maj=`get_version 'PROD_VERSION_MAJOR' "$version_file"`
ver_minor=`get_version 'PROD_VERSION_MINOR' "$version_file"`
ver_micro=`get_version 'PROD_VERSION_MICRO' "$version_file"`

#TODO build_rev: use env var if set

build_rev=`get_rev_from_file $buildrev_file $bump_build`

echo -n "${ver_maj}."  > "$buildtag_file" || exit 1
echo -n "${ver_minor}." >> "$buildtag_file" || exit 1
echo -n "${ver_micro}." >> "$buildtag_file" || exit 1
echo -n "${build_rev}" >> "$buildtag_file" || exit 1

