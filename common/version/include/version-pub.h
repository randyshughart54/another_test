#pragma once

// Major, minor and micro (patch) versions of the product:
//
#define PROD_VERSION_MAJOR	2
#define PROD_VERSION_MINOR	2
#define PROD_VERSION_MICRO	0

// Build number, incremented by the build system each time a new build is run:
//
#ifndef PROD_BUILD_NUMBER
#define PROD_BUILD_NUMBER	0
#endif

#define _TO_STR2(s) #s
#define _TO_STR(s) _TO_STR2(s)

#define PROD_VERSION_STR(major, minor, micro, build) \
	_TO_STR(major) "." _TO_STR(minor) "." _TO_STR(micro) "." _TO_STR(build)

// Strings include symbolic build tag, date and place of the build:
//
extern const char* const prod_build_info;
extern const char* const prod_build_comments;
