#include <syslog.h>
#include <errno.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <pthread.h>

#include <common/checks.h>

#include <iv.h>
#include <iv_list.h>
#include <ivykis-tools/avl.h>
#include <mem-monitor/mem-monitor.h>

typedef struct _mem {
	struct iv_avl_node node;
	size_t size;
	void *ptr;
} mem_t;

static int compare_mem(const struct iv_avl_node *_a, const struct iv_avl_node *_b);


static size_t total_alloc_size = 0;
static struct iv_avl_tree mem_alloc_db;
static pthread_rwlock_t mem_lock = PTHREAD_RWLOCK_INITIALIZER;


void mem_mon_init(void)
{
	INIT_IV_AVL_TREE(&mem_alloc_db, compare_mem);
}

void mem_mon_add_ptr(const char *caller_name, uint32_t line, const char *ptr_name, void *ptr, size_t size)
{
	if (PTR_IS_NULL(caller_name)) return;
	if (PTR_IS_NULL(ptr_name)) return;
	if (PTR_IS_NULL(ptr)) return;

	mem_t *mem;
	mem = malloc(sizeof(mem_t));
	if (PTR_IS_NULL(mem)) {
		/*
		 * TODO: Add libsyslog sypport
		 */
		syslog(LOG_ERR, ">>>>>  MEM(+):  Failed to allocate memory: %s(%d), caller: '%s'(line: %u)", strerror(errno), errno, caller_name, line);
		return;
	}

	mem->ptr = ptr;
	mem->size = size;

	pthread_rwlock_wrlock(&mem_lock);

	iv_avl_tree_insert(&mem_alloc_db, &mem->node);
	total_alloc_size += (size + sizeof(mem_t));

	syslog(LOG_DEBUG, ">>>>>  MEM(+):  Caller '%s'(line: %u) allocated new pointer '%s': %p, size %zu(+%zu) bytes, total allocated: %zu bytes",
	                  caller_name, line, ptr_name, ptr, size, sizeof(mem_t), total_alloc_size);

	pthread_rwlock_unlock(&mem_lock);
}

void mem_mon_del_ptr(const char *caller_name, uint32_t line, const char *ptr_name, void *ptr)
{
	if (PTR_IS_NULL(caller_name)) return;
	if (PTR_IS_NULL(ptr_name)) return;
	if (PTR_IS_NULL(ptr)) return;

	mem_t mem_tmp;
	const struct iv_avl_node *found_node;

	mem_tmp.ptr = ptr;

	pthread_rwlock_wrlock(&mem_lock);

	found_node = iv_tools_avl_find(&mem_alloc_db, &mem_tmp.node);
	if (found_node == NULL)
	{
		pthread_rwlock_unlock(&mem_lock);
		syslog(LOG_DEBUG, ">>>>>  MEM(-):  Failed to find allocated pointer '%s'(%p), caller: '%s'(line: %u)", ptr_name, ptr, caller_name, line);
		return;
	}

	mem_t *found_mem;
	found_mem = iv_container_of(found_node, mem_t, node);
	total_alloc_size -= (found_mem->size + sizeof(mem_t));

	syslog(LOG_DEBUG, ">>>>>  MEM(-):  Caller '%s'(line: %u) deallocated pointer '%s': %p, size %zu(+%zu) bytes, total allocated: %zu bytes",
	                  caller_name, line, ptr_name, ptr, found_mem->size, sizeof(mem_t), total_alloc_size);

	iv_avl_tree_delete(&mem_alloc_db, &found_mem->node);
	free(found_mem);

	pthread_rwlock_unlock(&mem_lock);
}

size_t mem_mon_get_total_alloc_size(void)
{
	size_t tmp;

	pthread_rwlock_rdlock(&mem_lock);

	tmp = total_alloc_size;

	pthread_rwlock_unlock(&mem_lock);

	return tmp;
}

inline void * mem_mon_malloc(const char *caller_name, uint32_t line, const char *ptr_name, size_t size)
{
	if (PTR_IS_NULL(caller_name)) return NULL;
	if (PTR_IS_NULL(ptr_name)) return NULL;

	void *ptr;
	ptr = malloc(size);
	if (PTR_IS_NULL(ptr)) {
		syslog(LOG_ERR, ">>>>>  MEM(+):  Failed to allocate memory: %s(%d), caller: '%s'(line: %u)", strerror(errno), errno, caller_name, line);
		return NULL;
	}

#if defined(MEMORY_MONITOR_EN)
	mem_mon_add_ptr(caller_name, line, ptr_name, ptr, size);
#endif

	return ptr;
}

inline void * mem_mon_calloc(const char *caller_name, uint32_t line, const char *ptr_name, size_t elem_num, size_t elem_size)
{
	if (PTR_IS_NULL(caller_name)) return NULL;
	if (PTR_IS_NULL(ptr_name)) return NULL;

	void *ptr;
	ptr = calloc(elem_num, elem_size);
	if (PTR_IS_NULL(ptr)) {
		syslog(LOG_ERR, ">>>>>  MEM(+):  Failed to allocate memory: %s(%d), caller: '%s'(line: %u)", strerror(errno), errno, caller_name, line);
		return NULL;
	}

#if defined(MEMORY_MONITOR_EN)
	mem_mon_add_ptr(caller_name, line, ptr_name, ptr, elem_num * elem_size);
#endif

	return ptr;
}

inline void * mem_mon_realloc(const char *caller_name, uint32_t line, const char *ptr_name, void *ptr, size_t size)
{
	if (PTR_IS_NULL(caller_name)) return NULL;
	if (PTR_IS_NULL(ptr_name)) return NULL;

#if defined(MEMORY_MONITOR_EN)
	if (ptr)
		mem_mon_del_ptr(caller_name, line, ptr_name, ptr);
#endif

	ptr = realloc(ptr, size);
	if (PTR_IS_NULL(ptr)) {
		syslog(LOG_ERR, ">>>>>  MEM(+):  Failed to reallocate memory: %s(%d), caller: '%s'(line: %u)", strerror(errno), errno, caller_name, line);
		return NULL;
	}

#if defined(MEMORY_MONITOR_EN)
	mem_mon_add_ptr(caller_name, line, ptr_name, ptr, size);
#endif

	return ptr;
}

inline void mem_mon_free(const char *caller_name, uint32_t line, const char *ptr_name, void *ptr)
{
	if (PTR_IS_NULL(caller_name)) return;
	if (PTR_IS_NULL(ptr_name)) return;
	if (PTR_IS_NULL(ptr)) return;

#if defined(MEMORY_MONITOR_EN)
	mem_mon_del_ptr(caller_name, line, ptr_name, ptr);
#endif

	free(ptr);
}

static int compare_mem(const struct iv_avl_node *_a, const struct iv_avl_node *_b)
{
	const mem_t *a = iv_container_of(_a, mem_t, node);
	const mem_t *b = iv_container_of(_b, mem_t, node);

	if (a->ptr > b->ptr)
		return 1;

	if (a->ptr < b->ptr)
		return -1;

	return 0;
}
