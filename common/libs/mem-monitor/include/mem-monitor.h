#ifndef MEMORY_MONITOR_H_INCLUDED
#define MEMORY_MONITOR_H_INCLUDED

#include <stdint.h>

/*
 * Define this symbol to gather
 * information about allocated
 * and freed memory, otherwise
 * library will work like
 * standard allocation functions
 */
//#define MEMORY_MONITOR_EN

void mem_mon_init(void);
void mem_mon_add_ptr(const char *caller_name, uint32_t line, const char *ptr_name, void *ptr, size_t size);
void mem_mon_del_ptr(const char *caller_name, uint32_t line, const char *ptr_name, void *ptr);
size_t mem_mon_get_total_alloc_size(void);
inline void * mem_mon_malloc(const char *caller_name, uint32_t line, const char *ptr_name, size_t size);
inline void * mem_mon_calloc(const char *caller_name, uint32_t line, const char *ptr_name, size_t elem_num, size_t elem_size);
inline void * mem_mon_realloc(const char *caller_name, uint32_t line, const char *ptr_name, void *ptr, size_t size);
inline void mem_mon_free(const char *caller_name, uint32_t line, const char *ptr_name, void *ptr);

#if defined(MEMORY_MONITOR_EN)

# define MEM_MON_ADD_PTR(__ptr__, __size__) \
	mem_mon_add_ptr(__func__, __LINE__, #__ptr__, __ptr__, __size__)

# define MEM_MON_DEL_PTR(__ptr__) \
	mem_mon_del_ptr(__func__, __LINE__, #__ptr__, __ptr__)

#else

# define MEM_MON_ADD_PTR(__ptr__, __size__)
# define MEM_MON_DEL_PTR(__ptr__)

#endif

#define MEM_MON_MALLOC(__ptr__, __size__) \
	__ptr__ = mem_mon_malloc(__func__, __LINE__, #__ptr__, __size__)

#define MEM_MON_CALLOC(__ptr__, __elem_num__, __elem_size__) \
	__ptr__ = mem_mon_calloc(__func__, __LINE__, #__ptr__, __elem_num__, __elem_size__)

#define MEM_MON_REALLOC(__ptr__, __size__) \
	__ptr__ = mem_mon_realloc(__func__, __LINE__, #__ptr__, __ptr__, __size__)

#define MEM_MON_FREE(__ptr__) \
	mem_mon_free(__func__, __LINE__, #__ptr__, __ptr__)

#endif
