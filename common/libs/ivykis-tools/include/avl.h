#ifndef __IV_TOOLS_AVL_H__
#define __IV_TOOLS_AVL_H__

#include <stddef.h>
#include <iv_avl.h>

typedef void (*avl_tour_cb_fn)(const struct iv_avl_node *an, int depth, void *cookie);
typedef int (*avl_clear_cb_fn)(struct iv_avl_node *an);
typedef int (*avl_clear_cb_param_fn)(struct iv_avl_node *an, void *cookie);
typedef enum {
	AVL_DEL_CHECK, // select for deleting from tree
	AVL_DEL_FREE,  // delete from tree
} avl_del_step_e;
typedef int (*avl_del_partially_cb_fn)(const struct iv_avl_node *an, avl_del_step_e step, void *cookie);

int iv_tools_avl_check_tour(const struct iv_avl_tree *tree, const struct iv_avl_node *an, const struct iv_avl_node *parent,
                            const struct iv_avl_node *min, const struct iv_avl_node *max, int *cnt, int depth,
                            avl_tour_cb_fn callback_fn, void *cookie);
const struct iv_avl_node *iv_tools_avl_find(const struct iv_avl_tree *tree, const struct iv_avl_node *an);
const struct iv_avl_node *iv_tools_avl_find_ge(const struct iv_avl_tree *tree, const struct iv_avl_node *an);
int iv_tools_clear_avl(struct iv_avl_tree *tree, avl_clear_cb_fn callback_fn);
int iv_tools_clear_avl_param(struct iv_avl_tree *tree, avl_clear_cb_param_fn callback_fn, void *cookie);
const struct iv_avl_node *iv_tools_avl_get_next(const struct iv_avl_tree *tree, const struct iv_avl_node *an);
void iv_tools_del_avl_partially(struct iv_avl_tree *tree, avl_del_partially_cb_fn callback_fn, void *cookie);

#define iv_tools_avl_tree_for_each_in_range(an, tree, begin, end) \
	for ((an) = iv_tools_avl_find_ge(tree, begin); \
	     (an) && (tree)->compare(an, end) <= 0; \
	     (an) = iv_avl_tree_next((struct iv_avl_node *)(an)))

#define iv_tools_avl_tree_for_each_in_range_safe(an, an2, tree, begin, end) \
	for ((an) = iv_tools_avl_find_ge(tree, begin), (an2) = iv_avl_tree_next_safe((struct iv_avl_node *)(an)); \
	     (an) && (tree)->compare(an, end) <= 0; \
	     (an) = (an2), (an2) = iv_avl_tree_next_safe((struct iv_avl_node *)(an2)))

#endif /* __IV_TOOLS_AVL_H__ */
