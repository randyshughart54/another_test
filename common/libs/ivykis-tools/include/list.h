#ifndef __IV_TOOLS_LIST_H__
#define __IV_TOOLS_LIST_H__

#include <stddef.h>
#include <iv_list.h>

const struct iv_list_head* iv_tools_list_last_get(struct iv_list_head *head);

#endif /* __IV_TOOLS_LIST_H__ */