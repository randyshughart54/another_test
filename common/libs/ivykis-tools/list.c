#include <ivykis-tools/list.h>

const struct iv_list_head* iv_tools_list_last_get(struct iv_list_head *head)
{
	return head->prev;
}