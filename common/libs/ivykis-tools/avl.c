#include <stdio.h>
#include <stdlib.h>
#include <iv_avl.h>
#include <iv_list.h>
#include <common/errors.h>
#include <syslog.h>

#include <ivykis-tools/avl.h>

int iv_tools_avl_check_tour(const struct iv_avl_tree *tree, const struct iv_avl_node *an, const struct iv_avl_node *parent,
                            const struct iv_avl_node *min, const struct iv_avl_node *max, int *cnt, int depth,
                            avl_tour_cb_fn callback_fn, void *cookie)
{
	int i;
	int hl;
	int hr;
	int my;

	if (an == NULL) // root checkiung
		return 0;

	if (an->parent != parent) {
		syslog(LOG_ERR, "parent mismatch: %p, %p versus %p",
		      an, an->parent, parent);
	}

	if (cnt)
		(*cnt)++;

	if (min != NULL || max != NULL) {
		int err;

		err = 0;
		if (min != NULL && tree->compare(min, an) >= 0)
			err++;
		if (max != NULL && tree->compare(an, max) >= 0)
			err++;

		if (err){
			syslog(LOG_ERR, "violated %p < %p < %p", min, an, max);
			if (tree->list != NULL)
				tree->list();
		}
	}

	hl = 0;
	if (an->left != NULL)
		hl = iv_tools_avl_check_tour(tree, an->left, an, min, an, cnt, depth + 1, callback_fn, cookie);

	if (callback_fn != NULL)
		callback_fn(an, depth, cookie);

	hr = 0;
	if (an->right != NULL)
		hr = iv_tools_avl_check_tour(tree, an->right, an, an, max, cnt, depth + 1, callback_fn, cookie);

	if (abs(hl - hr) > 1)
		syslog(LOG_ERR, "balance mismatch: %d vs %d", hl, hr);

	my = 1 + ((hl > hr) ? hl : hr);
	if (an->height != my)
		syslog(LOG_ERR, "height mismatch: %d vs %d/%d", an->height, hl, hr);


	return my;
}

const struct iv_avl_node *iv_tools_avl_find(const struct iv_avl_tree *tree, const struct iv_avl_node *an)
{
	int cmp;
	const struct iv_avl_node *node;

	node = tree->root;

	while (node != NULL)
	{
		cmp = tree->compare(node, an);
		if (cmp == 0)
			break;
		if (cmp < 0)
			node = node->right;
		else
			node = node->left;
	}

	return node;
}

const struct iv_avl_node *iv_tools_avl_find_ge(const struct iv_avl_tree *tree, const struct iv_avl_node *an)
{
	int cmp;
	const struct iv_avl_node *node, *result;

	node = tree->root;
	result = NULL;

	while (node != NULL)
	{
		cmp = tree->compare(node, an);
		if (cmp == 0)
		{
			result = node;
			break;
		}
		else if (cmp < 0)
		{
			node = node->right;
		}
		else
		{
			result = node;
			node = node->left;
		}
	}

	return result;
}

int iv_tools_clear_avl(struct iv_avl_tree *tree, avl_clear_cb_fn callback_fn)
{
	struct iv_avl_node *node, *node2;

	if (tree->root != NULL)
	{
		node = tree->root;

		iv_avl_tree_for_each_safe(node, node2, tree)
		{
			if (callback_fn != NULL)
			{
				int ret = callback_fn(node);
				if (ret != ERR_NONE)
					return ret;
			}
		}
	}

	return ERR_NONE;
}

int iv_tools_clear_avl_param(struct iv_avl_tree *tree, avl_clear_cb_param_fn callback_fn, void *cookie)
{
	struct iv_avl_node *node, *node2;

	node = tree->root;

	if (tree->root != NULL)
	{
		iv_avl_tree_for_each_safe(node, node2, tree)
		{
			if (callback_fn != NULL)
			{
				int ret = callback_fn(node, cookie);
				if (ret != ERR_NONE)
					return ret;
			}
		}
	}

	return ERR_NONE;
}

const struct iv_avl_node *iv_tools_avl_get_next(const struct iv_avl_tree *tree, const struct iv_avl_node *an)
{
	int cmp;
	const struct iv_avl_node *node, *result;

	node = tree->root;
	result = NULL;

	while (node != NULL)
	{
		cmp = tree->compare(node, an);
		if (cmp <= 0)
		{
			node = node->right;
		}
		else
		{
			result = node;
			node = node->left;
		}
	}

	return result;
}

struct iv_tools_del_avl_list
{
	struct iv_list_head list;
	const struct iv_avl_node *an;
};

struct iv_tools_del_avl_cookie
{
	void *cookie;
	avl_del_partially_cb_fn callback_fn;
	struct iv_list_head head;
};

static void iv_tools_del_avl_partially_cb(const struct iv_avl_node *an, int depth, void *cookie)
{
	struct iv_tools_del_avl_cookie *int_cookie = (struct iv_tools_del_avl_cookie*)cookie;
	if (int_cookie->callback_fn(an, AVL_DEL_CHECK, int_cookie->cookie))
	{
		struct iv_tools_del_avl_list *list;
		list = (struct iv_tools_del_avl_list*)malloc(sizeof(struct iv_tools_del_avl_list));
		if (list)
		{
			list->an = an;
			iv_list_add_tail(&list->list, &int_cookie->head);
		}
		else
			syslog(LOG_ERR, "malloc failed");
	}
}

void iv_tools_del_avl_partially(struct iv_avl_tree *tree, avl_del_partially_cb_fn callback_fn, void *cookie)
{
	struct iv_list_head *list1, *list2;
	struct iv_tools_del_avl_cookie int_cookie;
	int_cookie.callback_fn = callback_fn;
	int_cookie.cookie = cookie;
	INIT_IV_LIST_HEAD(&int_cookie.head);

	iv_tools_avl_check_tour(tree,
	                        tree->root,
	                        NULL, NULL, NULL, NULL, 0,
							iv_tools_del_avl_partially_cb, &int_cookie);

	iv_list_for_each_safe(list1, list2, &int_cookie.head)
	{
		struct iv_tools_del_avl_list *avl_node_list = (struct iv_tools_del_avl_list*)list1;
		callback_fn(avl_node_list->an, AVL_DEL_FREE, cookie);
		free(avl_node_list);
	}
}
