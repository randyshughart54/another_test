#ifndef _LOG_H_INCLUDED_
#define _LOG_H_INCLUDED_

#include <stdarg.h>
#include <syslog.h>

#define log_err(fmt...) log_print(__func__, LOG_ERR, fmt)
#define log_warn(fmt...) log_print(__func__, LOG_WARNING, fmt)
#define log_info(fmt...) log_print(__func__, LOG_INFO, fmt)
#define log_dbg(fmt...) log_print(__func__, LOG_DEBUG, fmt)

void log_print(const char *func, int lvl, const char *fmt, ...) __attribute__ ((format (printf, 3, 4)));

#endif
