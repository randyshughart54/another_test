#include <log/log.h>
#include <common/errors.h>

#include <stdio.h>
#include <string.h>
#include <syslog.h>
#include <time.h>
#include <sys/time.h>
#include <assert.h>
#include <errno.h>

#define LOG_MSG_BUF_SIZE 2048

static int loglvl_is_valid(int lvl)
{
	static const int lvls[] = {
		LOG_EMERG,
		LOG_ALERT,
		LOG_CRIT,
		LOG_ERR,
		LOG_WARNING,
		LOG_NOTICE,
		LOG_INFO,
		LOG_DEBUG
	};
	const int lvls_cnt = sizeof(lvls) / sizeof(int);

	int lvl_idx;

	for (lvl_idx = 0; lvl_idx < lvls_cnt; lvl_idx++)
		if (lvl == lvls[lvl_idx])
			return 1;

	return 0;
}

void log_print(const char *func, int lvl, const char *fmt, ...)
{
	assert(fmt != NULL);

	va_list args;

	va_start(args, fmt);

	char buf[LOG_MSG_BUF_SIZE];

	int printed = vsnprintf(buf, sizeof(buf), fmt, args);

	assert(printed > 0 && "vsnprintf");

	va_end(args);

	if (!loglvl_is_valid(lvl))
		lvl = LOG_EMERG;

	tzset();
	syslog(lvl, "%s: %s", func, buf);
}
