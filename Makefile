.PHONY: all
all: me5000 me5100 me5200

.PHONY: me5000
me5000:
	$(MAKE) -C $@

.PHONY: me5100
me5100:
	$(MAKE) -C $@

.PHONY: me5200
me5200:
	$(MAKE) -C $@

.PHONY: sim
sim:
	$(MAKE) -C $@

.PHONY: me5000-config
me5000-config:
	$(MAKE) -C me5000 config

.PHONY: me5100-config
me5100-config:
	$(MAKE) -C me5100 config

.PHONY: me5200-config
me5200-config:
	$(MAKE) -C me5200 config

.PHONY: me5000-config-rebuild-lib
me5000-config-rebuild-lib:
	$(MAKE) -C me5000 config-rebuild-lib

.PHONY: me5100-config-rebuild-lib
me5100-config-rebuild-lib:
	$(MAKE) -C me5100 config-rebuild-lib

.PHONY: me5200-config-rebuild-lib
me5200-config-rebuild-lib:
	$(MAKE) -C me5200 config-rebuild-lib

.PHONY: sim-config
sim-config:
	$(MAKE) -C sim config
